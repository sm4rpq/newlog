<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" indent="yes"/>

    <!-- This template matches the root element of the input file -->
	<xsl:template match="/">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

			<fo:layout-master-set>
				<fo:simple-page-master master-name="content"
				  page-height="210mm"
				  page-width="297mm"
				  margin-top="20mm"
				  margin-bottom="10mm"
				  margin-left="5mm"
				  margin-right="5mm">
					<fo:region-body margin-bottom="3.5mm"/>
					<fo:region-after extent="3mm"/>
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-name="content" master-reference="content">
				<fo:static-content flow-name="xsl-region-after">
					<fo:block font-size="6pt"><xsl:value-of select="current-dateTime()"/></fo:block>
				</fo:static-content>
				<fo:flow flow-name="xsl-region-body" margin-bottom="5mm">

			<fo:block>
				<fo:table border="solid" font-size="8pt" table-layout="fixed" width="287mm">			
					<fo:table-column column-width="7mm"/>  <!--QSO Nr-->
					<fo:table-column column-width="17mm"/> <!--Datum-->
					<fo:table-column column-width="10mm"/> <!--Tid start-->
					<fo:table-column column-width="10mm"/> <!--Tid slut-->
					<fo:table-column column-width="15mm"/> <!--Frekvens-->
					<fo:table-column column-width="11mm"/> <!--Trafiks�tt-->
					<fo:table-column column-width="24mm"/> <!--Signal-->
					<fo:table-column column-width="27mm"/> <!--Namn-->
					<fo:table-column column-width="40mm"/> <!--QTH-->
					<fo:table-column column-width="57mm"/> <!--STN // Ant // Eff-->
				 	<fo:table-column column-width="8mm"/>  <!--RST given -->
				 	<fo:table-column column-width="7mm"/>  <!--Eff ut -->
				 	<fo:table-column column-width="8mm"/>  <!--RST f�dd -->
				 	<fo:table-column column-width="5mm"/>  <!--QSL S -->
				 	<fo:table-column column-width="5mm"/>  <!--QSL M -->
				 	<fo:table-column column-width="8mm"/>  <!--V�gutbredn -->
					<fo:table-column column-width="8mm"/>  <!--US State-->
					<fo:table-column column-width="8mm"/>  <!--IOTA-->
					<fo:table-column column-width="10mm"/>  <!--Sat-->
					<fo:table-column column-width="4mm"/>  <!--SatMode-->
					<fo:table-header>
						<fo:table-row border="solid">
							<fo:table-cell>
								<fo:block font-size="6pt">x</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Datum</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Start</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Slut</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">QRG</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Mod</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Signal</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Namn</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">QTH</fo:block>
								<fo:block font-size="6pt">Lokator</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Noteringar</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">RST</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Effekt</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">RST</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">QS</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">QM</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Prop</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">State</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Iota</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">Sat</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-size="6pt">SM</fo:block>
							</fo:table-cell>

						</fo:table-row>
					</fo:table-header>
					<fo:table-body>
<!--						<xsl:apply-templates select="/Log/Record[OwnCall='SM3RPP' and FreqUp='50']"/>-->
						<xsl:apply-templates select="/Log/Record"/>
					</fo:table-body>
				</fo:table>
			</fo:block>
				</fo:flow>
			</fo:page-sequence>


		</fo:root>
	</xsl:template>

	<xsl:template match="Record">
		<fo:table-row border="solid">
			<xsl:if test="Complete='NO'">
				<xsl:attribute name="font-style">italic</xsl:attribute>
			</xsl:if>
			<fo:table-cell><fo:block></fo:block></fo:table-cell>
			<fo:table-cell>
				<fo:block>
					<xsl:value-of select="DateOn"/>
				</fo:block>
				<xsl:if test="Complete='NO'">
					<fo:block>
						Ej komplett
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="TimeOn"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="DateOff"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block>
						<xsl:value-of select="FreqUp"/>
					</fo:block>
					<xsl:if test="(FreqDn!='0') and (FreqDn!=FreqUp)">
					<fo:block>
						<xsl:value-of select="FreqDn"/>
					</fo:block>
					</xsl:if>
			</fo:table-cell>
			<!-- Mode HELL is used as a note of not knowning since VQLOG does not accept entering blank -->
			<!-- logprog2 does not accept blank either but uses UNKNOWN -->
			<fo:table-cell><fo:block><xsl:if test="(Mode!='HELL') and (Mode!='UNKNOWN')"><xsl:value-of select="Mode"/></xsl:if></fo:block></fo:table-cell>
			<fo:table-cell>
				<fo:block><xsl:value-of select="Call"/><xsl:if test="IsSilentKey='T'"> #</xsl:if></fo:block>
				
				<xsl:if test="Manager!=''">
					<fo:block> Via 
						<xsl:value-of select="Manager"/>
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="Name"/></fo:block></fo:table-cell>
			<fo:table-cell>
				<xsl:if test="Place='' and Locator=''">
					<fo:block/>
				</xsl:if>
				<xsl:if test="Place!=''">
					<fo:block>
						<xsl:value-of select="Place"/>
					</fo:block>
				</xsl:if>
				<xsl:if test="Locator!=''">
					<fo:block>
						<xsl:value-of select="Locator"/>
					</fo:block>
				</xsl:if>
			</fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="Notes"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="RSTs"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="Pwr"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="RSTr"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="QSLSent"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="QSLRcvd"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="Propagation"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="USState"/></fo:block></fo:table-cell>
			<fo:table-cell><fo:block><xsl:value-of select="IOTA"/></fo:block></fo:table-cell>
			<xsl:if test="Propagation='SAT'">
				<fo:table-cell><fo:block><xsl:value-of select="Sat"/></fo:block></fo:table-cell>
				<fo:table-cell><fo:block><xsl:value-of select="SatMode"/></fo:block></fo:table-cell>
			</xsl:if>
		</fo:table-row>
	</xsl:template>

</xsl:stylesheet>
