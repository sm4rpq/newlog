unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, StdCtrls;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ADOConnection1: TADOConnection;
    ADOTable1: TADOQuery;
    ADOTable1FechaHora: TDateTimeField;
    ADOTable1Estacion: TStringField;
    ADOTable1SuRST: TStringField;
    ADOTable1MiRST: TStringField;
    ADOTable1Frecuencia: TFloatField;
    ADOTable1FrecDown: TFloatField;
    ADOTable1Modo: TStringField;
    ADOTable1SWL: TBooleanField;
    ADOTable1QSLE: TStringField;
    ADOTable1QSLR: TStringField;
    ADOTable1TipoPropa: TStringField;
    ADOTable1Locator: TStringField;
    ADOTable1Nombre: TStringField;
    ADOTable1Poblacion: TStringField;
    ADOTable1Observacio: TStringField;
    ADOTable1Completado: TStringField;
    ADOTable1IndUsado: TStringField;
    ADOTable1MiLocator: TStringField;
    ADOTable1HoraFin: TDateTimeField;
    ADOTable1Random: TBooleanField;
    ADOTable1SoloIndicativo: TStringField;
    ADOTable1CodDXCC: TSmallintField;
    ADOTable1Continente: TStringField;
    ADOTable1ZonaCQ: TSmallintField;
    ADOTable1ZonaITU: TSmallintField;
    ADOTable1OutPwr: TStringField;
    ADOTable1Rig: TStringField;
    ADOTable1QSLManager: TStringField;
    ADOTable1Diploma1: TStringField;
    ADOTable1Diploma2: TStringField;
    ADOTable1Diploma3: TStringField;
    ADOTable1Diploma4: TStringField;
    ADOTable1Diploma5: TStringField;
    ADOTable1Diploma6: TStringField;
    ADOTable1Diploma7: TStringField;
    ADOTable1Diploma8: TStringField;
    ADOTable1Diploma9: TStringField;
    ADOTable1Diploma10: TStringField;
    ADOTable1Azimuth: TSmallintField;
    ADOTable1Elevacion: TSmallintField;
    ADOTable1Satellite: TStringField;
    ADOTable1SatMode: TStringField;
    ADOTable1Pings: TSmallintField;
    ADOTable1Bursts: TSmallintField;
    ADOTable1MaxSecs: TFloatField;
    ADOTable1RndMeteors: TBooleanField;
    ADOTable1Shower: TBooleanField;
    ADOTable1ShowerName: TStringField;
    ADOTable1SameThan: TStringField;
    ADOTable1ForceInit: TBooleanField;
    ADOTable1Diploma11: TStringField;
    ADOTable1Diploma12: TStringField;
    ADOTable1Diploma13: TStringField;
    ADOTable1Diploma14: TStringField;
    ADOTable1Diploma15: TStringField;
    ADOTable1Diploma16: TStringField;
    ADOTable1Diploma17: TStringField;
    ADOTable1Diploma18: TStringField;
    ADOTable1Diploma19: TStringField;
    ADOTable1Diploma20: TStringField;
    ADOTable1Diploma21: TStringField;
    ADOTable1Diploma22: TStringField;
    ADOTable1Diploma23: TStringField;
    ADOTable1Diploma24: TStringField;
    ADOTable1Diploma25: TStringField;
    ADOTable1Diploma26: TStringField;
    ADOTable1Diploma27: TStringField;
    ADOTable1Diploma28: TStringField;
    ADOTable1Diploma29: TStringField;
    ADOTable1Diploma30: TStringField;
    ADOTable1NroRegistro: TAutoIncField;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);

var
  A : TextFile;
  B : string;

  procedure Fileit(Element : string; Content : string);

  var
    i : integer;
    Str : string;


  begin
    for i:=1 to length(Content) do begin
      case Content[i] of
      '&' : Str:=Str+'&amp;';
      '<' : Str:=Str+'&lt;';
      '>' : Str:=Str+'&gt;';
      else
        str:=Str+Content[i];
      end;
    end;
    WriteLn(A,'<'+Element+'>',Str,'</'+Element+'>');
  end;

begin
  AssignFile(A,'diverse.xml');
  Rewrite(A);
  WriteLn(A,'<?xml version="1.0" encoding="ISO-8859-1" ?>');
  WriteLn(A,'<Log>');
  ADOTable1.Open;
  while not(ADOTable1.Eof) do begin
    WriteLn(A,'<Record>');
    Fileit('DateOn',FormatDateTime('yyyy-mm-dd',ADOTable1FechaHora.AsDateTime));
    Fileit('TimeOn',FormatDateTime('hh:nn',ADOTable1FechaHora.AsDateTime));
    B:=ADOTable1HoraFin.AsString;
    if not(ADOTable1HoraFin.IsNull) and (length(B)>10) then
      Fileit('DateOff',FormatDateTime('hh:nn',ADOTable1HoraFin.AsDateTime))
    else
      Fileit('DateOff','');
    Fileit('Call',ADOTable1Estacion.AsString);
    Fileit('RSTs',ADOTable1SuRST.AsString);
    Fileit('RSTr',ADOTable1MiRST.AsString);
    Fileit('FreqUp',ADOTable1Frecuencia.AsString);
    Fileit('FreqDn',ADOTable1FrecDown.AsString);
    Fileit('Mode',ADOTable1Modo.AsString);
    Fileit('QSLSent',ADOTable1QSLE.AsString);
    Fileit('QSLRcvd',ADOTable1QSLR.AsString);
    Fileit('Propagation',ADOTable1TipoPropa.AsString);
    Fileit('Locator',ADOTable1Locator.AsString);
    Fileit('Name',ADOTable1Nombre.AsString);
    Fileit('Place',ADOTable1Poblacion.AsString);
    Fileit('Notes',ADOTable1Observacio.AsString);
    Fileit('Complete',ADOTable1Completado.AsString);
    Fileit('OwnCall',ADOTable1IndUsado.AsString);
    Fileit('OwnLoc',ADOTable1MiLocator.AsString);
    Fileit('Random',ADOTable1Random.AsString);
    Fileit('RootCall',ADOTable1SoloIndicativo.AsString);
//    Fileit('DxccCode',ADOTable1CodDXCC.AsString);
    Fileit('Pwr',ADOTable1OutPwr.AsString);
    Fileit('Rig',ADOTable1Rig.AsString);
    Fileit('Manager',ADOTable1QSLManager.AsString);
    Fileit('USState',ADOTable1Diploma1.AsString);
    Fileit('IOTA',ADOTable1Diploma2.AsString);
    Fileit('Sat',ADOTable1Satellite.AsString);
    Fileit('SatMode',ADOTable1SatMode.AsString);
    Fileit('MaxSecs',ADOTable1MaxSecs.AsString);
    Fileit('RandomMeteors',ADOTable1RndMeteors.AsString);
    WriteLn(A,'</Record>');
    ADOTable1.Next;
  end;
  ADOTable1.Close;
  WriteLn(A,'</Log>');
  CloseFile(A);
end;

end.
