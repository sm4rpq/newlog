object Form1: TForm1
  Left = 2075
  Top = 103
  Width = 261
  Height = 185
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 16
    Top = 16
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object ADOConnection1: TADOConnection
    ConnectionString = 
      'Provider=MSDASQL.1;Persist Security Info=False;Data Source=vqdiv' +
      'erse;'
    DefaultDatabase = 'D:\delphprj\newlog\vqtoxml\diverse.mdb'
    Provider = 'MSDASQL.1'
    Left = 16
    Top = 56
  end
  object ADOTable1: TADOQuery
    Connection = ADOConnection1
    Parameters = <>
    SQL.Strings = (
      'select * from log order by FechaHora')
    Left = 96
    Top = 56
    object ADOTable1FechaHora: TDateTimeField
      FieldName = 'FechaHora'
    end
    object ADOTable1Estacion: TStringField
      FieldName = 'Estacion'
      Size = 15
    end
    object ADOTable1SuRST: TStringField
      FieldName = 'SuRST'
      Size = 8
    end
    object ADOTable1MiRST: TStringField
      FieldName = 'MiRST'
      Size = 8
    end
    object ADOTable1Frecuencia: TFloatField
      FieldName = 'Frecuencia'
    end
    object ADOTable1FrecDown: TFloatField
      FieldName = 'FrecDown'
    end
    object ADOTable1Modo: TStringField
      FieldName = 'Modo'
      Size = 10
    end
    object ADOTable1SWL: TBooleanField
      FieldName = 'SWL'
    end
    object ADOTable1QSLE: TStringField
      FieldName = 'QSLE'
      Size = 1
    end
    object ADOTable1QSLR: TStringField
      FieldName = 'QSLR'
      Size = 1
    end
    object ADOTable1TipoPropa: TStringField
      FieldName = 'TipoPropa'
      Size = 3
    end
    object ADOTable1Locator: TStringField
      FieldName = 'Locator'
      Size = 6
    end
    object ADOTable1Nombre: TStringField
      FieldName = 'Nombre'
      Size = 32
    end
    object ADOTable1Poblacion: TStringField
      FieldName = 'Poblacion'
      Size = 32
    end
    object ADOTable1Observacio: TStringField
      FieldName = 'Observacio'
      Size = 70
    end
    object ADOTable1Completado: TStringField
      FieldName = 'Completado'
      Size = 3
    end
    object ADOTable1IndUsado: TStringField
      FieldName = 'IndUsado'
      Size = 15
    end
    object ADOTable1MiLocator: TStringField
      FieldName = 'MiLocator'
      Size = 6
    end
    object ADOTable1HoraFin: TDateTimeField
      FieldName = 'HoraFin'
    end
    object ADOTable1Random: TBooleanField
      FieldName = 'Random'
    end
    object ADOTable1SoloIndicativo: TStringField
      FieldName = 'SoloIndicativo'
      Size = 15
    end
    object ADOTable1CodDXCC: TSmallintField
      FieldName = 'CodDXCC'
    end
    object ADOTable1Continente: TStringField
      FieldName = 'Continente'
      Size = 2
    end
    object ADOTable1ZonaCQ: TSmallintField
      FieldName = 'ZonaCQ'
    end
    object ADOTable1ZonaITU: TSmallintField
      FieldName = 'ZonaITU'
    end
    object ADOTable1OutPwr: TStringField
      FieldName = 'OutPwr'
      Size = 10
    end
    object ADOTable1Rig: TStringField
      FieldName = 'Rig'
      Size = 32
    end
    object ADOTable1QSLManager: TStringField
      FieldName = 'QSLManager'
      Size = 15
    end
    object ADOTable1Diploma1: TStringField
      FieldName = 'Diploma1'
      Size = 10
    end
    object ADOTable1Diploma2: TStringField
      FieldName = 'Diploma2'
      Size = 10
    end
    object ADOTable1Diploma3: TStringField
      FieldName = 'Diploma3'
      Size = 10
    end
    object ADOTable1Diploma4: TStringField
      FieldName = 'Diploma4'
      Size = 10
    end
    object ADOTable1Diploma5: TStringField
      FieldName = 'Diploma5'
      Size = 10
    end
    object ADOTable1Diploma6: TStringField
      FieldName = 'Diploma6'
      Size = 10
    end
    object ADOTable1Diploma7: TStringField
      FieldName = 'Diploma7'
      Size = 10
    end
    object ADOTable1Diploma8: TStringField
      FieldName = 'Diploma8'
      Size = 10
    end
    object ADOTable1Diploma9: TStringField
      FieldName = 'Diploma9'
      Size = 10
    end
    object ADOTable1Diploma10: TStringField
      FieldName = 'Diploma10'
      Size = 10
    end
    object ADOTable1Azimuth: TSmallintField
      FieldName = 'Azimuth'
    end
    object ADOTable1Elevacion: TSmallintField
      FieldName = 'Elevacion'
    end
    object ADOTable1Satellite: TStringField
      FieldName = 'Satellite'
      Size = 15
    end
    object ADOTable1SatMode: TStringField
      FieldName = 'SatMode'
      Size = 10
    end
    object ADOTable1Pings: TSmallintField
      FieldName = 'Pings'
    end
    object ADOTable1Bursts: TSmallintField
      FieldName = 'Bursts'
    end
    object ADOTable1MaxSecs: TFloatField
      FieldName = 'MaxSecs'
    end
    object ADOTable1RndMeteors: TBooleanField
      FieldName = 'RndMeteors'
    end
    object ADOTable1Shower: TBooleanField
      FieldName = 'Shower'
    end
    object ADOTable1ShowerName: TStringField
      FieldName = 'ShowerName'
      Size = 32
    end
    object ADOTable1SameThan: TStringField
      FieldName = 'SameThan'
      Size = 15
    end
    object ADOTable1ForceInit: TBooleanField
      FieldName = 'ForceInit'
    end
    object ADOTable1Diploma11: TStringField
      FieldName = 'Diploma11'
      Size = 10
    end
    object ADOTable1Diploma12: TStringField
      FieldName = 'Diploma12'
      Size = 10
    end
    object ADOTable1Diploma13: TStringField
      FieldName = 'Diploma13'
      Size = 10
    end
    object ADOTable1Diploma14: TStringField
      FieldName = 'Diploma14'
      Size = 10
    end
    object ADOTable1Diploma15: TStringField
      FieldName = 'Diploma15'
      Size = 10
    end
    object ADOTable1Diploma16: TStringField
      FieldName = 'Diploma16'
      Size = 10
    end
    object ADOTable1Diploma17: TStringField
      FieldName = 'Diploma17'
      Size = 10
    end
    object ADOTable1Diploma18: TStringField
      FieldName = 'Diploma18'
      Size = 10
    end
    object ADOTable1Diploma19: TStringField
      FieldName = 'Diploma19'
      Size = 10
    end
    object ADOTable1Diploma20: TStringField
      FieldName = 'Diploma20'
      Size = 10
    end
    object ADOTable1Diploma21: TStringField
      FieldName = 'Diploma21'
      Size = 10
    end
    object ADOTable1Diploma22: TStringField
      FieldName = 'Diploma22'
      Size = 10
    end
    object ADOTable1Diploma23: TStringField
      FieldName = 'Diploma23'
      Size = 10
    end
    object ADOTable1Diploma24: TStringField
      FieldName = 'Diploma24'
      Size = 10
    end
    object ADOTable1Diploma25: TStringField
      FieldName = 'Diploma25'
      Size = 10
    end
    object ADOTable1Diploma26: TStringField
      FieldName = 'Diploma26'
      Size = 10
    end
    object ADOTable1Diploma27: TStringField
      FieldName = 'Diploma27'
      Size = 10
    end
    object ADOTable1Diploma28: TStringField
      FieldName = 'Diploma28'
      Size = 10
    end
    object ADOTable1Diploma29: TStringField
      FieldName = 'Diploma29'
      Size = 10
    end
    object ADOTable1Diploma30: TStringField
      FieldName = 'Diploma30'
      Size = 10
    end
    object ADOTable1NroRegistro: TAutoIncField
      FieldName = 'NroRegistro'
      ReadOnly = True
    end
  end
end
