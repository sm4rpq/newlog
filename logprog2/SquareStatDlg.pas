unit SquareStatDlg;

{
  Purpose:
    Dialogue for displaying square statistics.
  Status:
    Add map alternative
    Show QSL Status
  Date:
    2021-03-28
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, IdAntiFreezeBase, IdAntiFreeze,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP,
  IdIntercept, IdSSLIntercept, IdSSLOpenSSL;

type
  TdlgStatSquare = class(TForm)
    btnClose: TButton;
    rgCallsigns: TRadioGroup;
    rgPlace: TRadioGroup;
    rgFrequency: TRadioGroup;
    rgPropagation: TRadioGroup;
    PageControl1: TPageControl;
    Karta: TTabSheet;
    Textrapport: TTabSheet;
    redStat: TRichEdit;
    Image1: TImage;
    procedure recalcStat(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    LatestField : string;
    SquaresWorked : array[0..99] of boolean;
    procedure AddSquare(Square : string);
    procedure Finish;
  public
    { Public declarations }
  end;

var
  dlgStatSquare: TdlgStatSquare;

implementation

uses ExportData;

{$R *.dfm}

procedure TdlgStatSquare.recalcStat(Sender: TObject);

var
  Count : integer;

begin
  with dmDataExport.qrySqrsQGIS do begin
    SQL.Clear;
    SQL.Add('select ai.name,count(*) From');
    SQL.Add(' awarditem ai,qso q,receivedawardcredit rac,usedlocation ul');
    SQL.Add('where');
    SQL.Add(' rac.ASOQSO_ID=q.QSO_ID and');
    SQL.Add(' rac.ASOAWARDITEM_ID=ai.AWARDITEM_ID and');
    SQL.Add(' ai.REFAWARD_ID=1 and'); //TODO: Separate select on VUCC
    SQL.Add(' ul.USEDLOCATION_ID=q.REFUSEDLOCATION_ID');

    case rgCallsigns.ItemIndex of
      0 : begin //All - no condition needed
          end;
      1 : begin //SM4RPP
            SQL.Add('and q.REFOPCALL=''SM4RPP''');
          end;
      2 : begin
            SQL.Add('and q.REFOPCALL=''SM4RPQ''');
          end;
    end;
    case rgPlace.ItemIndex of
      0 : ;// No condition
      1 : begin //Hemma
            SQL.Add('and ul.locator<''JP''');
          end;
      2 : begin //Stugan
            SQL.Add('and ul.locator>''JP74''');
          end;
    end;
    case rgFrequency.ItemIndex of
      0 : ; //Alla band
      1 : //Kortv�g
          SQL.Add('and q.Frequency_rx<30.0');
      2 : //50MHz
          SQL.Add('and q.Frequency_rx>=50.0 and q.Frequency_rx<=54.0');
      3 : //144MHz
          SQL.Add('and q.Frequency_rx>=144.0 and q.Frequency_rx<=146.0');
      4 : //432MHz
          SQL.Add('and q.Frequency_rx>=432.0 and q.Frequency_rx<=432.0');
      5 : //1296MHz
          SQL.Add('and q.Frequency_rx>=1240.0 and q.Frequency_rx<=1300.0');
    end;
    case rgPropagation.ItemIndex of
      0 : ; //Allt
      1 : // Ej repeater
          SQL.Add('and (q.refpropagationname not in (''RPT'',''SAT'') or q.refpropagationname is null)');
      2 : // Aurora
          SQL.Add('and (q.refpropagationname=''AUR'' or q.refpropagationname=''AUE'')');
      3 : // Satellit
          SQL.Add('and q.refpropagationname=''SAT''');
      4 : // Meteorscatter
          SQL.Add('and q.refpropagationname=''MS''');
    end;
    SQL.Add('and (complete<>''F'' or complete is null)');
    SQL.Add('group by ai.name order by ai.name');

    Open;
    redStat.Clear;
    Count:=0;
    while not EOF do begin
      AddSquare(FieldByName('Name').AsString);
      inc(Count);
      Next;
    end;
    Close;
    Finish;
  end;
  LatestField:='';
  redStat.Lines.Insert(0,'Totalt antal rutor: '+IntToStr(Count));
end;

procedure TdlgStatSquare.FormShow(Sender: TObject);
begin
  recalcStat(Sender);
end;

procedure TdlgStatSquare.AddSquare(Square: string);

var
  Field : string;
  Number : integer;

begin
  if Length(Square)<>4 then
    exit;
  Field:=Copy(Square,1,2);
  if (LatestField<>'') and (Field<>LatestField) then
    Finish;
  LatestField:=Field;
  Number:=StrToInt(Copy(Square,3,4));
  SquaresWorked[Number]:=true;
end;

procedure TdlgStatSquare.Finish;

var
  d1,d2 : integer;
  i : integer;
  Line : string;
  Count : integer;

begin
  redStat.Lines.Add('');
  redStat.Lines.Add(LatestField);
  Line:='';
  Count:=0;
  redStat.Lines.Add('-------------------------------');
  for d2:=9 downto 0 do begin
    for d1:=0 to 9 do begin
      i:=10*d1+d2;

      if SquaresWorked[i] then begin
        Line:=Line+'|'+Format('%2.2d',[i]);
        inc(Count);
      end else
        Line:=Line+'|  ';
      SquaresWorked[i]:=false;
      if d1=9 then begin
        redStat.Lines.Add(Line+'|');
        redStat.Lines.Add('-------------------------------');
        Line:='';
      end;
    end;
  end;
  redStat.Lines.Add('Antal rutor i '+LatestField+': '+IntToStr(Count));
end;

end.
