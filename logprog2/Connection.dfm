object dmConnection: TdmConnection
  OldCreateOrder = False
  Left = 2802
  Top = 214
  Height = 268
  Width = 263
  object IBDatabase1: TIBDatabase
    Connected = True
    DatabaseName = 'newlog'
    Params.Strings = (
      'user_name=sysdba'
      'password=masterkey'
      'lc_ctype=WIN1252')
    LoginPrompt = False
    IdleTimer = 0
    SQLDialect = 3
    TraceFlags = []
    Left = 32
    Top = 8
  end
  object IBTransaction1: TIBTransaction
    Active = False
    DefaultDatabase = IBDatabase1
    AutoStopAction = saNone
    Left = 112
    Top = 8
  end
  object procGetUniqueId: TIBStoredProc
    Database = IBDatabase1
    Transaction = IBTransaction1
    StoredProcName = 'GET_UNIQUE_ID'
    Left = 40
    Top = 72
    object procGetUniqueIdUNIQUE_ID: TIntegerField
      FieldName = 'UNIQUE_ID'
      Origin = 'GET_UNIQUE_ID.UNIQUE_ID'
    end
  end
  object qryIdDxcc: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * '
      'from dxcc_entity de inner join'
      'callprefix cp on cp.REFDXCC_ID=de.DXCC_ID'
      
        ' where de.valid_from<:qsodate1 and (de.valid_to>:qsodate2 or de.' +
        'valid_to is null) and prefix = :prefix')
    Left = 128
    Top = 72
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'qsodate1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'qsodate2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'prefix'
        ParamType = ptUnknown
      end>
  end
  object qryAwardId: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from award where name=:name')
    Left = 40
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'name'
        ParamType = ptUnknown
      end>
  end
  object qryQSOList: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'SELECT'
      
        '  q.starttime,q.frequency_tx,q.refmode,q.call,q.rpt_s,q.rpt_r,q.' +
        'locator as locatorq,q.complete,'
      '  c.name,c.nick,c.issilentkey,q.qth,'
      '  q.comment,q.refdxcc_id,coalesce(de.svnamn,de.name),'
      '  q.refcall,q.paperlogref,'
      '  q.sat_mode,q.isswl,'
      '  q.refpropagationname,q.endtime,'
      '  ul.locator as locatorul,ul.heightamsl,sat.name as satname'
      'FROM qso q'
      
        'INNER JOIN contactedstation c on c.basecall=q.refbasecall and c.' +
        'instance=q.refinstance'
      
        'INNER JOIN UsedLocation ul on ul.usedlocation_id=q.refusedlocati' +
        'on_id'
      'LEFT OUTER JOIN dxcc_entity de on de.dxcc_id=q.refdxcc_id'
      
        'LEFT OUTER JOIN satellite sat on sat.satellite_id=q.refsatellite' +
        '_id'
      'where q.call like :workedcall and q.locator like :workedloc'
      'ORDER BY'
      '  q.starttime;')
    Left = 120
    Top = 136
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'workedcall'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'workedloc'
        ParamType = ptUnknown
      end>
  end
end
