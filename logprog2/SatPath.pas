unit SatPath;

{
  Purpose:
    Calculates path from a groundstation to a satellite.
    Math based on plan13 in qrptracker on github. That is in turn based on
    G3RUHs old plan13 basic program.
  Status:
    Does not shine with confidence. In fact calculations for older data
     seems incorrect. Needs investigation (results vs actual QSOs show El<<0)
  Date:
    2021-04-25
}

interface

uses
  groundstation,spaceobject,vector;

type
  TSatPath = class(TObject)
  private
    FAzimuth            : double;
    FElevation          : double;

    FStation            : TGroundStation;
    FSat                : TSpaceObject;

    FrxFrequency        : double;
    FtxFrequency        : double;
    FRxDoppler          : double;
    FTxDoppler          : double;
    dopplerFactor       : double;
    FRange              : double;

    FAOS                : TDateTime;
    FLOS                : TDateTime;

    procedure SetRxFrequency(const Value: double);
    procedure SetTxFrequency(const Value: double);

  public
    procedure setStation(aStation : TGroundStation);
    procedure setSat(aSat : TSpaceObject);

    procedure rangevec();
    procedure pass();

    procedure calculate();

    property TxFrequency : double read FTxFrequency write SetTxFrequency;
    property RxFrequency : double read FRxFrequency write SetRxFrequency;
    property RxDoppler   : double read FRxDoppler;
    property TxDoppler   : double read FTxDoppler;
    property AOS         : TDateTime read FAOS;
    property LOS         : TDateTime read FLOS;

    property Azimuth : double read FAzimuth;
    property Elevation : double read FElevation;
    property Range : double read FRange;
  end;

implementation

uses
  Geoconstants, Math, DateUtils;


procedure TSatPath.setStation(aStation : TGroundStation);
begin
  FStation := aStation;
end;

procedure TSatPath.setSat(aSat : TSpaceObject);
begin
  FSat := aSat;
end;


(*
 * Compute and manipulate range/velocity/antenna vectors
 *)
procedure TSatPath.rangevec();

var
  R : T3DVector;
  U,E,N : double;
  RR : double;

begin
  (* Range vector = sat vector - observer vector *)
  R:=Subtract(FSat.S,FStation.O);

  (* Distance to satellite *)
  FRange:=Magnitude(R);

  Normalize(R);

  U := DotProduct(R,FStation.U);
  E := DotProduct(R,FStation.E);
  N := DotProduct(R,FStation.N);

  FAzimuth := arctan2(E, N);
  if FAzimuth<0 then
    FAzimuth:=FAzimuth+2*Pi;
  FElevation := arcsin(U);

  (* Solve antenna vector along unit range vector, -r.a = cos(SQ) *)
  (*    SQ = deg(acos(-(Ax * Rx + Ay * Ry + Az * Rz)));
  *)

  (* Resolve Sat-Obs velocity vector along unit range vector. *)
  RR := DotProduct(Subtract(FSat.V,FStation.VO),R); (* Range rate, m/sec/sec *)

  dopplerFactor := RR / 299792.458;
  FRxDoppler:=FRxFrequency * dopplerFactor;
  FTxDoppler:=FTxFrequency * dopplerFactor;
end;

procedure TSatPath.pass();
begin
  //Recalculate if:
  //(current time>FLOS) or forced
end;

procedure TSatPath.calculate();
begin
  FSat.satvec();
  rangevec();
end;

procedure TSatPath.SetRxFrequency(const Value: double);
begin
  FRxFrequency := Value;
end;

procedure TSatPath.SetTxFrequency(const Value: double);
begin
  FTxFrequency := Value;
end;

end.
