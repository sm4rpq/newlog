unit QSLRulesDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls;

type
  TdlgQSLRules = class(TForm)
    btnProcess: TButton;
    cbQSLService: TComboBox;
    Label1: TLabel;
    TreeView1: TTreeView;
    Notebook1: TNotebook;
    procedure FormShow(Sender: TObject);
    procedure btnProcessClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgQSLRules: TdlgQSLRules;

implementation

{$R *.dfm}

procedure TdlgQSLRules.FormShow(Sender: TObject);
begin
  cbQSLService.Items.Clear;
  //TODO:
end;

procedure TdlgQSLRules.btnProcessClick(Sender: TObject);
begin
  //Loop over selected QSOs
  //  If matching criteria
  //    Get or Create ConfirmationServiceInteraction
  //    Get previous Sent Confirmation (if any)
  //    Insert QSOConfirmation with number++
  //    Insert SentConfirmation with status
  //  end if
  //End Loop
end;

end.
