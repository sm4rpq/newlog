unit AdifExport;

{
  Purpose:
    Provide possibility to export logbook as ADIF file.
  Status:
    - May no longer be compatible with VQLOG V2
    - Need to add more/better selection of what to export
    - Only contest_id missing for Adif1 now (of the fields available)
    - Progress bar not working if subset selected
  Date:
    2023-01-29
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ComCtrls;

type
  TdlgExportADIF = class(TForm)
    btnExport: TButton;
    pbExport: TProgressBar;
    btnClose: TButton;
    edtExportFile: TEdit;
    spdExportFile: TSpeedButton;
    odFilename: TOpenDialog;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    cbAdifVersion: TComboBox;
    cbCalls: TComboBox;
    cbLocators: TComboBox;
    Label4: TLabel;
    cbPurposes: TComboBox;
    Label5: TLabel;
    chkUpdate: TCheckBox;
    procedure spdExportFileClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
    procedure cbPurposesChange(Sender: TObject);
  private
    { Private declarations }
    ADIF : TextFile;
    AdifExportVersion : integer;
    AdifVer : string;

    function Freq2Band(Frequency : double) : string;

    procedure ExportQSO(const QSO_Id : integer; const Timestamp : TDateTime);
    procedure AddDate(var Row : string;
                      Id : string;
                      Value : TDateTime);
    procedure AddTime(var Row : string;
                      Id : string;
                      Value : TDateTime);
    procedure AddString(var Row : string;
                        Id : string;
                        Value : string);
    procedure AddNumeric(var Row : string;
                         Id : string;
                         Value : string);
  public
    { Public declarations }
  end;

var
  dlgExportADIF: TdlgExportADIF;

implementation

{$R *.dfm}

uses
  Settings, ExportData, IBQuery, IBSQL, Connection;


procedure TdlgExportADIF.AddDate(var Row : string;
                         Id : string;
                         Value : TDateTime);
begin
  Row:=Row+'<'+Id+':8:d>'+FormatDateTime('yyyymmdd',Value);
end;

procedure TdlgExportADIF.AddTime(var Row : string;
                         Id : string;
                         Value : TDateTime);
begin
  Row:=Row+'<'+Id+':4:t>'+FormatDateTime('hhmm',Value);
end;

procedure TdlgExportADIF.AddString(var Row : string;
                           Id : string;
                           Value : string);
begin
  Value:=Trim(Value);
  if Value<>'' then
    Row:=Row+'<'+Id+':'+IntToStr(Length(Value))+'>'+Value;
end;

procedure TdlgExportADIF.AddNumeric(var Row : string;
                           Id : string;
                           Value : string);
begin
  Value:=Trim(Value);
  if Value<>'' then
    Row:=Row+'<'+Id+':'+IntToStr(Length(Value))+'>'+Value;
end;

procedure TdlgExportADIF.ExportQSO(const QSO_Id : integer; const Timestamp : TDateTime);

var
  ADIFRow    : string;
  Band       : string;
  AwardField : string;
  AwardItem  : string;
  QSL        : string;
  QSLField   : string;
  QSLDField  : string;
  Loc        : string;
  Include    : boolean;
  Update     : boolean;

begin
  Include:=true;
  Update:=false;
  if cbPurposes.ItemIndex>0 then begin
    //Query for export "permit"
    with dmDataExport.qryOnlineLogInteraction do begin
      params.ParamByName('qsoid').AsInteger:=QSO_Id;
      params.ParamByName('logserviceid').AsInteger:=cbPurposes.ItemIndex;
      Open;
      if not(EOF) then begin
        //If a record exists, it should only be included if the status
        //is 'M' as in modified.
        //'Y' means it has already been uploaded => should be ignored
        //'N' means do no uploads =< should be ignored.
        Include:=Fields.FieldByName('refindicator').AsString='M';
        Update:=Include;
      end;
      Close;
    end;
  end;

  if Include then begin
    with dmDataExport.qryLog.Fields do begin
      ADIFRow:='';
      AddDate(ADIFRow,'qso_date',FieldByName('STARTTIME').AsDateTime);
      AddTime(ADIFRow,'time_on',FieldByName('STARTTIME').AsDateTime);
      AddString(ADIFRow,'freq',FieldByName('FREQUENCY_TX').AsString);
      Band:=Freq2Band(FieldByName('FREQUENCY_TX').AsFloat);
      if Band<>'' then
        AddString(ADIFRow,'band',Band);

      if (AdifExportVersion>200) and (FieldByName('Frequency_rx').AsFloat<>FieldByName('Frequency_tx').AsFloat) then begin
        AddString(ADIFRow,'freq_rx',FieldByName('FREQUENCY_RX').AsString);
        Band:=Freq2Band(FieldByName('FREQUENCY_RX').AsFloat);
        if Band<>'' then
          AddString(ADIFRow,'band_rx',Band);
      end;

      if (AdifExportVersion<304) then
        AddString(ADIFRow,'mode',FieldByName('REFMODE').AsString)
      else begin
        AddString(ADIFRow,'mode',FieldByName('adif_mode').AsString);
        AddString(ADIFRow,'submode',FieldByName('adif_submode').AsString);
      end;
      AddString(ADIFRow,'tx_pwr',FieldByName('Power').AsString);
      AddString(ADIFRow,'call',FieldByName('CALL').AsString);
      AddString(ADIFRow,'rst_sent',FieldByName('RPT_S').AsString);
      AddNumeric(ADIFRow,'stx',FieldByName('Serial_S').AsString);
      AddString(ADIFRow,'rst_rcvd',FieldByName('RPT_R').AsString);
      AddNumeric(ADIFRow,'srx',FieldByName('Serial_R').AsString);
      AddString(ADIFRow,'sat_mode',FieldByName('SAT_MODE').AsString);
      if not FieldByName('SATNAME').IsNull then begin
        AddString(ADIFRow,'sat_name',FieldByName('SATNAME').AsString);
      end;
      loc:=FieldByName('LOCATOR').AsString;
      AddString(ADIFRow,'gridsquare',copy(Loc,1,8));
      if (AdifExportVersion>=314) and (length(loc)>8) then
        AddString(ADIFRow,'gridsquare_ext',copy(Loc,9,2));
      AddString(ADIFRow,'name',FieldByName('CSNAME').AsString);
      AddString(ADIFRow,'qth',FieldByName('QTH').AsString);
      //AddString(ADIFRow,'',FieldByName('O_ANT').AsString);
      AddString(ADIFRow,'rx_pwr',FieldByName('O_PWR').AsString);
      AddString(ADIFRow,'comment',FieldByName('COMMENT').AsString);
      AddString(ADIFRow,'notes',FieldByName('Note').AsString);
      AddNumeric(ADIFRow,'dxcc',FieldByName('REFDXCC_Id').AsString);
      if not(FieldByName('ENDTIME').IsNull) then begin
        AddTime(ADIFRow,'time_off',FieldByName('ENDTIME').AsDateTime);
        if AdifExportVersion>=223 then //Check version
          AddDate(ADIFrow,'qso_date_off',FieldByName('ENDTIME').AsDateTime);
      end;
      if AdifExportVersion>200 then
        if FieldByName('COMPLETE').AsString='F' then
          AddString(ADIFRow,'qso_complete','N');
      AddString(ADIFRow,'qsl_via',FieldByName('CSMGR').AsString);
      AddString(ADIFRow,'prop_mode',FieldByName('REFPROPAGATIONNAME').AsString);

      AddString(ADIFRow,'operator',FieldByName('REFCALL').AsString);
      if AdifExportVersion>200 then begin
        loc:=FieldByName('Locator1').AsString;
        AddString(ADIFRow,'my_gridsquare',Copy(Loc,1,8));
        if (AdifExportVersion>=314) and (length(loc)>8) then
          AddString(ADIFRow,'my_gridsquare',Copy(Loc,9,2));
        AddString(ADIFRow,'my_name',FieldByName('UCNAME').AsString);
      end;
    end; //with

    with dmDataExport.qryReceivedQSL do begin
      ParamByName('QSO_Id').AsInteger:=QSO_id;
      Open;
      while not(EOF) do begin
        QSL:=FieldByName('REFQSL_RCVD').AsString;
        QSLField:=FieldByName('QSLRCVDFIELD').AsString;
        QSLDField:=FieldByName('QSLRDATEFIELD').AsString;
        AddString(ADIFRow,QSLField,QSL);
        if QSL='Y' then
          AddDate(ADIFRow,QSLDField,FieldByName('REGISTEREDDATE').AsDateTime);
        Next;
      end;
      Close;
    end;

    with dmDataExport.qrySentQSL do begin
      ParamByName('QSO_Id').AsInteger:=QSO_id;
      Open;
      while not(EOF) do begin
        QSL:=FieldByName('REFQSL_SENT').AsString;
        QSLField:=FieldByName('QSLSENTFIELD').AsString;
        QSLDField:=FieldByName('QSLSDATEFIELD').AsString;
        AddString(ADIFRow,QSLField,QSL);
        if QSL='Y' then begin
          AddDate(ADIFRow,QSLDField,FieldByName('REGISTEREDDATE').AsDateTime);
          AddString(ADIFROw,'qslmsg',FieldByName('QSLMESSAGE').AsString);
        end;
        Next;
      end;
      Close;
    end;

    with dmDataExport.qryQSOAwardItems do begin
      ParamByName('QSO_Id').AsInteger:=QSO_id;
      Open;
      while not(EOF) do begin
        if not(FieldByName('ADIF_Field').IsNull) then begin
          if (AdifExportVersion<200) and (FieldByName('REFDXCC_Id').AsInteger=1) then
            AwardField:='VE_PROV'
          else
            AwardField:=FieldByName('ADIF_Field').AsString;
          AwardItem:=FieldByName('Name').AsString;
          AddString(ADIFRow,AwardField,AwardItem);
        end;
        Next;
      end;
      Close;
    end; //with

    if (cbPurposes.ItemIndex=0) then begin
      //Loop over any OnlineLogInteraction records and add suitable fields and their data.
      with dmDataExport.qryOnlineLogInteractions do begin
        Params.ParamByName('qsoid').AsInteger:=QSO_Id;
        Open;
        while not(Eof) do begin
          AddString(ADIFRow,FieldByName('UploadDateField').AsString,FormatDateTime('yyyymmdd',FieldByName('UploadDate').AsDateTime));
          AddString(ADIFRow,FieldByName('UploadStatusField').AsString,FieldByName('RefIndicator').AsString);
          Next;
        end;
        Close;
      end;
    end else if (chkUpdate.Checked) then begin
      if Update then begin
        with dmDataExport.qryUpdateOLI do begin
          paramByName('qso_id').AsInteger:=qso_id;
          paramByName('logservice_id').AsInteger:=cbPurposes.ItemIndex;
          ParamByName('uploaddate').AsDateTime:=Timestamp;
          ParamByName('refindicator').AsString:='Y';
          ExecQuery;
        end;
      end else begin
        with dmDataExport.qryInsertOLI do begin
          ParamByName('qso_id').AsInteger:=qso_id;
          ParamByName('logservice_id').AsInteger:=cbPurposes.ItemIndex;
          ParamByName('uploaddate').AsDateTime:=Timestamp;
          ParamByName('refindicator').AsString:='Y';
          ExecQuery;
        end;
      end;
    end;

    ADIFRow:=ADIFRow+'<eor>';
    WriteLn(ADIF,ADIFRow);
  end;
end;

procedure TdlgExportADIF.spdExportFileClick(Sender: TObject);
begin
  if odFilename.Execute then begin
    edtExportFile.Text:=odFilename.FileName;
  end;
end;

procedure TdlgExportADIF.FormShow(Sender: TObject);
begin
  edtExportFile.Text:=dmSettings.AdifExportPath;

  with dmDataExport.qryUsedCalls do begin
    cbCalls.Clear;
    cbCalls.Items.Add('- All -');
    Open;
    while not eof do begin
      cbCalls.Items.Add(Fields.FieldByName('CALL').AsString);
      Next;
    end;
    Close;
    cbCalls.ItemIndex:=0;
  end;

  with dmDataExport.qryUsedLocs do begin
    cbLocators.Clear;
    cbLocators.Items.Add('- All -');
    Open;
    while not eof do begin
      cbLocators.Items.Add(Fields.FieldByName('LOCATOR').AsString);
      Next;
    end;
    Close;
    cbLocators.ItemIndex:=0;
  end;

  with dmDataExport.qryLogServices do begin
    cbPurposes.Clear;
    cbPurposes.Items.Add('Normal export');
    Open;
    while not eof do begin
      cbPurposes.Items.AddObject('Export to '+Fields.FieldByName('NAME').AsString,TObject(Fields.FieldByName('Logservice_Id').AsInteger));
      Next;
    end;
    Close;
    cbPurposes.ItemIndex:=0;
    chkUpdate.Enabled:=false;
    chkUpdate.Checked:=false;
  end;

end;

procedure TdlgExportADIF.btnExportClick(Sender: TObject);

var
  Header : string;
  Timestamp : TDateTime;

begin
  btnClose.Enabled:=false;
  btnExport.Enabled:=false;
  Application.ProcessMessages;

  with dmDataExport.qryQSOCount do begin
    open;
    pbExport.Max:=Fields[0].AsInteger;
    pbExport.Position:=0;
    Close;
  end;

  case cbAdifVersion.ItemIndex of
    0 : begin
          AdifExportVersion:=100;
          AdifVer:='1.0.0';
        end;
    1 : begin
          AdifExportVersion:=214;
          AdifVer:='2.1.4';
        end;
    2 : begin
          AdifExportVersion:=227;
          AdifVer:='2.2.7';
        end;
    3 : begin
          AdifExportVersion:=301;
          AdifVer:='3.0.1';
        end;
    4 : begin
          AdifExportVersion:=314;
          AdifVer:='3.1.4';
        end;
  else
    // Can't happen.
    AdifExportVersion:=0;
    AdifVer:='';
  end;

  (* Set up query *)
  with dmDataExport.qryLog.SQL do begin
    Clear;
    Add('select q.*,u.locator,sat.name as satname,cs.name as csname,cs.nick as csnick,cs.manager as csmgr,cs.issilentkey, uc.name as ucname, om.adif_mode, om.adif_submode');
    Add(' from qso q');
    Add(' inner join usedlocation u on u.usedlocation_id=q.refusedlocation_id');
    Add(' inner join CONTACTEDSTATION cs on cs.basecall=q.REFBASECALL and cs.INSTANCE=q.REFINSTANCE');
    Add(' inner join USEDCALL uc on uc.CALL=q.REFCALL');
    Add(' inner join OperatingMode om on om.Mode=q.refmode');
    Add(' left outer join');
    Add('satellite sat on sat.satellite_id=q.refsatellite_id');
    if (cbCalls.ItemIndex>0) or (cbLocators.ItemIndex>0) then
      Add('where');
    if (cbCalls.ItemIndex>0) then
      Add('uc.call=:usedcall');
    if (cbLocators.ItemIndex>0) then begin
      if cbCalls.ItemIndex>0 then
        Add('and');
      Add('u.locator=:usedloc');
    end;
    Add(' order by q.starttime');
  end;

  if (cbCalls.ItemIndex>0) then
    dmDataExport.qryLog.ParamByName('usedcall').AsString:=cbCalls.Text;
  if (cbLocators.ItemIndex>0) then
    dmDataExport.qryLog.ParamByName('usedloc').AsString:=cbLocators.Text;


  if AdifExportVersion>0 then begin
    AssignFile(ADIF,edtExportFile.Text);
    Rewrite(ADIF);
    Timestamp:=now;

    Header:='ADIF export from Logprog2 ';
    if AdifExportVersion>200 then begin
      AddString(Header,'ProgramId','logprog2');
      AddString(Header,'ProgramVersion','0.0');
    end;
    if AdifExportVersion>=218 then begin
      AddString(Header,'adif_ver',AdifVer)
    end;
    Header:=Header+'<eoh>';
    WriteLn(ADIF,Header);

    //The actual export
    dmDataExport.qryLog.Open;
    while not(dmDataExport.qryLog.Eof) do begin
      if AdifExportVersion>0 then
        ExportQSO(dmDataExport.qryLog.FieldByName('QSO_Id').AsInteger,Timestamp);
      dmDataExport.qryLog.Next;
      pbExport.StepBy(1);
    end;
    dmDataExport.qryLog.Close;
    if chkUpdate.Checked then
      dmConnection.IBTransaction1.Commit;

    CloseFile(ADIF);

    dmSettings.AdifExportPath:=edtExportFile.Text;
  end;
  btnExport.Enabled:=true;
  btnClose.Enabled:=true;
end;

function TdlgExportADIF.Freq2Band(Frequency: double): string;
begin
  if (Frequency>=0.1357) and (Frequency<=0.1378) then
    Result:='2190m'
  else if (Frequency>=0.472) and (Frequency<=0.479) then
    Result:='630m'
  else if (Frequency>=0.501) and (Frequency<0.504) then
    Result:='560m'
  else if (Frequency>=1.8) and (Frequency<=2.0) then
    Result:='160m'
  else if (Frequency>=3.5) and (Frequency<=4.0) then
    Result:='80m'
  else if (Frequency>=5.06) and (Frequency<=5.45) then
    Result:='60m'
  else if (Frequency>=7.0) and (Frequency<=7.3) then
    Result:='40m'
  else if (Frequency>=10.1) and (Frequency<=10.15) then
    Result:='30m'
  else if (Frequency>=14.0) and (Frequency<=14.35) then
    Result:='20m'
  else if (Frequency>=18.068) and (Frequency<=18.168) then
    Result:='17m'
  else if (Frequency>=21.0) and (Frequency<=21.45) then
    Result:='15m'
  else if (Frequency>=24.890) and (Frequency<=24.99) then
    Result:='12m'
  else if (Frequency>=28.0) and (Frequency<=29.7) then
    Result:='10m'
  else if (Frequency>=40.0) and (Frequency<=45.0) then
    Result:='8m'
  else if (Frequency>=50.0) and (Frequency<=54.0) then
    Result:='6m'
  else if (Frequency>=54.000001) and (Frequency<=69.9) then
    Result:='5m'
  else if (Frequency>=70.0) and (Frequency<=71.0) then
    Result:='4m'
  else if (Frequency>=144.0) and (Frequency<=148.0) then
    Result:='2m'
  else if (Frequency>=222.0) and (Frequency<=225.0) then
    Result:='1.25m'
  else if (Frequency>=420.0) and (Frequency<=450.0) then
    Result:='70cm'
  else if (Frequency>=902.0) and (Frequency<=928.0) then
    Result:='33cm'
  else if (Frequency>=1240.0) and (Frequency<=1300.0) then
    Result:='23cm'
  else if (Frequency>=2300.0) and (Frequency<=2450.0) then
    Result:='13cm'
  else if (Frequency>=3300.0) and (Frequency<=3500.0) then
    Result:='9cm'
  else if (Frequency>=5650.0) and (Frequency<=5925.0) then
    Result:='6cm'
  else if (Frequency>=10000.0) and (Frequency<=10500.0) then
    Result:='3cm'
  else if (Frequency>=24000.0) and (Frequency<=24250.0) then
    Result:='1.25cm'
  else if (Frequency>=47000.0) and (Frequency<=47200.0) then
    Result:='6mm'
  else if (Frequency>=75500.0) and (Frequency<=81000.0) then
    Result:='4mm'
  else if (Frequency>=119980.0) and (Frequency<=120020.0) then
    Result:='2.5mm'
  else if (Frequency>=142000.0) and (Frequency<=149000.0) then
    Result:='2mm'
  else if (Frequency>=241000.0) and (Frequency<=250000.0) then
    Result:='1mm'
  else
    Result:='';
end;

procedure TdlgExportADIF.cbPurposesChange(Sender: TObject);
begin
  chkUpdate.Enabled:=cbPurposes.ItemIndex<>0;
end;

end.
