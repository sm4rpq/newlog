object dlgQSLRules: TdlgQSLRules
  Left = 2477
  Top = 247
  Width = 863
  Height = 512
  Caption = 'QSL-regler'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 53
    Height = 13
    Caption = 'QSL-Tj'#228'nst'
  end
  object btnProcess: TButton
    Left = 8
    Top = 448
    Width = 75
    Height = 25
    Caption = 'Process'
    TabOrder = 0
    OnClick = btnProcessClick
  end
  object cbQSLService: TComboBox
    Left = 8
    Top = 24
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Text = 'cbQSLService'
  end
  object TreeView1: TTreeView
    Left = 8
    Top = 56
    Width = 241
    Height = 385
    Indent = 19
    TabOrder = 2
  end
  object Notebook1: TNotebook
    Left = 264
    Top = 56
    Width = 585
    Height = 385
    PageIndex = 2
    TabOrder = 3
    object TPage
      Left = 0
      Top = 0
      Caption = 'Default'
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'A'
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'B'
    end
  end
end
