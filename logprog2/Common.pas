unit Common;

{
  Purpose:
    Things potentially used from many places.
  Status:
    Kind of stable
  Date:
    2023-01-14
}

interface

const
  ConfigKey = 'Software\SM4RPQ\LogProg2';
  CSVImportPath = 'CSVImportPath';
  TLImportPath = 'TLImportPath';
  XMLImportPath = 'XMLImportPath';
  QGISSquareListPath = 'QGISSquareListPath';
  LogXmlExportPath = 'LogXmlExportPath';
  AdifExportPath = 'AdifExportPath';
  
implementation

end.
 