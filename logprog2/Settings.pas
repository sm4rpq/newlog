unit Settings;

{
  Purpose:
    Provides access to settings stored in the registry. These settings
    are things that is remembered per user from one session to another.
  Status:
    Likely stable
  Date:
    2023-01-14
}

interface

uses
  SysUtils, Classes, Registry;

type
  TdmSettings = class(TDataModule)
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
    Reg : TRegistry;
    function GetCSVImportPath: string;
    procedure SetCSVImportPath(const Value: string);
    procedure SetTLImportPath(const Value: string);
    function GetTLImportPath: string;
    function GetXMLImportPath: string;
    procedure SetXMLImportPath(const Value: string);
    function GetQGISSquareListPath: string;
    procedure SetQGISSquareListPath(const Value: string);
    function GetLogXmlExportPath: string;
    procedure SetLogXmlExportPath(const Value: string);
    function GetAdifExportPath: string;
    procedure SetAdifExportPath(const Value: string);
  public
    { Public declarations }
    property CSVImportPath : string read GetCSVImportPath write SetCSVImportPath;
    property QGISSquareListPath : string read GetQGISSquareListPath write SetQGISSquareListPath;
    property LogXmlExportPath : string read GetLogXmlExportPath write SetLogXmlExportPath;
    property TLImportPath : string read GetTLImportPath write SetTLImportPath;
    property XMLImportPath : string read GetXMLImportPath write SetXMLImportPath;
    property AdifExportPath : string read GetAdifExportPath write SetAdifExportPath;
  end;

var
  dmSettings: TdmSettings;

implementation

uses
  Windows,
  Common;

{$R *.dfm}

procedure TdmSettings.DataModuleCreate(Sender: TObject);
begin
  Reg:=TRegistry.Create;
  Reg.RootKey:=HKEY_CURRENT_USER;
  Reg.OpenKey(ConfigKey,True);
end;

procedure TdmSettings.DataModuleDestroy(Sender: TObject);
begin
  Reg.Free;
end;

function TdmSettings.GetAdifExportPath: string;
begin
  Result:=Reg.ReadString(Common.AdifExportPath);
end;

function TdmSettings.GetCSVImportPath: string;
begin
  Result:=Reg.ReadString(Common.CSVImportPath);
end;

function TdmSettings.GetLogXmlExportPath: string;
begin
  Result:=Reg.ReadString(Common.LogXmlExportPath);
end;

function TdmSettings.GetQGISSquareListPath: string;
begin
  Result:=Reg.ReadString(Common.QGISSquareListPath);
end;

function TdmSettings.GetTLImportPath: string;
begin
  Result:=Reg.ReadString(Common.TLImportPath);
end;

function TdmSettings.GetXMLImportPath: string;
begin
  Result:=Reg.ReadString(Common.XMLImportPath);
end;

procedure TdmSettings.SetAdifExportPath(const Value: string);
begin
  Reg.WriteString(Common.AdifExportPath,Value);
end;

procedure TdmSettings.SetCSVImportPath(const Value: string);
begin
  Reg.WriteString(Common.CSVImportPath,Value);
end;

procedure TdmSettings.SetLogXmlExportPath(const Value: string);
begin
  Reg.WriteString(Common.LogXmlExportPath,Value);
end;

procedure TdmSettings.SetQGISSquareListPath(const Value: string);
begin
  Reg.WriteString(Common.QGISSquareListPath,Value);
end;

procedure TdmSettings.SetTLImportPath(const Value: string);
begin
  Reg.WriteString(Common.TLImportPath,Value);
end;

procedure TdmSettings.SetXMLImportPath(const Value: string);
begin
  Reg.WriteString(Common.XMLImportPath,Value);
end;

end.
