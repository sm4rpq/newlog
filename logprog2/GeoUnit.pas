unit GeoUnit;

{
  Purpose:
    Geograophic coordinate handling routines
  Status:
    TGeoCoord
     needs work on translation chains
    TGeoCircle
     needs work on ellipsoid distance vs flat
  Date:
    2021-02-28
}

interface

uses
  Classes,Vector;

type
  TSetAs = (gcSphere,gcCartesian,gcLoc,gcText);
  TGeoCoord = class(TObject)
  private
    CurrentExact : TSetAs;
    CurrentValid : set of TSetAs;

    //Spherical coordinates
    FLat : double;
    FLon : double;
    FHeight : double; //Above ellipsoid! No geoid madness

    //Cartesian coordinates
    FCartesian : T3DVector; //Axis from south to north. South-pole = -R North pole=+R

    //String coordinate (locator)
    FLocator : string;

    //String coordinate (textual)
    FText : string;

    //Conversion functions
    function RadiiAtLat(GeocLat : double) : double;
    procedure SphericalToCartesian;
    procedure CartesianToSpherical;
    procedure GeodeticToLocator;
    procedure LocatorToGeodetic;

    //Access stuff
    function GetLocator: string;
    procedure SetLocator(const Value: string);
    function GetText: string;
    procedure SetText(const Value: string);
    function GetCartesian: T3DVector;
    procedure SetCartesian(const Value: T3DVector);

  public
    property Locator : string read GetLocator write SetLocator;
    property Text : string read GetText write SetText;
    procedure GetGeodetic(var Lat,Lon : double);
    property Height : double read FHeight write FHeight;
    procedure SetGeodetic(Lat,Lon : double);
    property Cartesian : T3DVector read GetCartesian write SetCartesian;
  end;
  TGeoCircle = record
    CenterC : T3DVector;
    CenterG : record
                Lon : double;
                Lat : double;
              end;
    RadiusC : double;
    RadiusG : double;
  end;
  TGeoCoordList = array of TGeoCoord;

  function get2ptCircle(P1, P2: T3DVector): TGeoCircle;
  function get3ptCircle(P1, P2, P3: T3DVector): TGeoCircle;
  function trivial(const Points : TGeoCoordList): TGeoCircle;
  function Welzl(P, R : TGeoCoordList) : TGeoCircle;


implementation

uses
  Math,Misc;

const
  a = 6378137.0;
  a2= a*a;
  b = 6356752.3142451;
  b2 = b*b;

{ TGeoCoord }

procedure TGeoCoord.CartesianToSpherical;

var
  R : double;
  RadLat : double;
  RadLon : double;

begin
  R:=Magnitude(FCartesian);
  RadLat:=arcsin(FCartesian.Z/R);
  RadLon:=arctan2(FCartesian.Y,FCartesian.X);

  //TODO: Geodetic to geocentric

  FLat:=Misc.RadToDeg(RadLat);
  FLon:=Misc.RadToDeg(RadLon);
  FHeight:=R-RadiiAtLat(RadLat);
end;

procedure TGeoCoord.GeodeticToLocator;

const
  E=1.0E-9;

var
  A,B : double;
  C,D : integer;
  Level : integer;

begin
  A:=FLon+E+180.0;
  B:=FLat+E+90.0;
  FLocator:='';
  Level:=4;

  A:=A/20.0; B:=B/10.0; C:=trunc(A); D:=trunc(B);
  FLocator:=chr(65+C)+chr(65+D);
  dec(Level);

  while (Level>0) do begin
    A:=(A-C)*10.0; B:=(B-D)*10.0; C:=trunc(A); D:=trunc(B);
    FLocator:=FLocator+chr(48+C)+chr(48+D);
    dec(Level);

    if (Level>0) then begin
      A:=(A-C)*24.0; B:=(B-D)*24.0; C:=trunc(A); D:=trunc(B);
      FLocator:=FLocator+chr(C+65)+chr(D+65);
      dec(Level);
    end;
  end;
  include(CurrentValid,gcLoc);
end;

function TGeoCoord.GetCartesian : T3DVector;
begin
  if not (gcCartesian in CurrentValid) then begin
    if not(gcSphere in CurrentValid) and (gcLoc in CurrentValid) then begin
      LocatorToGeodetic;
      SphericalToCartesian;
    end else if gcSphere in CurrentValid then
      SphericalToCartesian;
  end;
  Result:=FCartesian;
end;

procedure TGeoCoord.GetGeodetic(var Lat, Lon: double);
begin
  if not (gcSphere in CurrentValid) then begin
    case CurrentExact of
      gcSphere : ; //Error
      gcCartesian : CartesianToSpherical;
      gcText : ;
      gcLoc : LocatorToGeodetic;
    end;
  end;
  Lat:=FLat;
  Lon:=FLon;
end;

function TGeoCoord.GetLocator: string;
begin
  if not(gcLoc in CurrentValid) then begin
    if not(gcSphere in CurrentValid) and (gcCartesian in CurrentValid) then begin
      CartesianToSpherical;
      GeodeticToLocator;
    end else if gcSphere in CurrentValid then
      GeodeticToLocator;
  end;
  Result:=FLocator
end;

function TGeoCoord.GetText: string;
begin
  if not(gcText in CurrentValid) then begin
    case CurrentExact of
      gcSphere : ;
      gcCartesian : ;
      gcText : ; //Error
      gcLoc : ;
    end;
  end;
  Result:=FText
end;

procedure TGeoCoord.LocatorToGeodetic;

var
  i     : integer;
  Char  : array[1..10] of integer;

begin
  for i:=1 to Length(FLocator) do
    if i in [1..2,5..6,9..10] then
      Char[i]:=ord(FLocator[i])-65
    else
      Char[i]:=ord(FLocator[i])-48;

  FLon:=(Char[1]*20.0)-180.0; //Each field-letter is 20 degrees longitude starting at -180
  FLat:=(Char[2]*10.0)-90.0;  //Each field-letter is 10 degress latitude starting at -90

  if Length(FLocator)>2 then begin

    FLon:=FLon+Char[3]*2.0; //Each square-digits is 2 degrees longitude starting at field corner
    FLat:=FLat+Char[4]*1.0; //Each square-digits is 1 degree latitude starting at field corner

    if Length(FLocator)>4 then begin

      FLon:=FLon+Char[5]*2.0/24.0; //Each locator-letter is 2/24 degrees
      FLat:=FLat+Char[6]*1.0/24.0; //Each locator-letter is 1/24 degree

      if Length(FLocator)>6 then begin

        FLon:=FLon+Char[7]*2.0/240.0; //Each locator-digit is 2/240 degrees
        FLat:=FLat+Char[8]*1.0/240.0; //Each locator-digit is 1/240 degree

        if Length(FLocator)>8 then begin

          FLon:=FLon+Char[7]*2.0/5760.0; //Each locator-letter is 2/5760 degrees
          FLat:=FLat+Char[8]*1.0/5760.0; //Each locator-letter is 1/5760 degree

          FLon:=FLon+1.0/5760.0;
          FLat:=FLat+0.5/5760.0;

        end else begin

          FLon:=FLon+1.0/240.0;
          FLat:=FLat+0.5/240.0;

        end;

      end else begin

        FLon:=FLon+1.0/24.0; //Add half a subsquare to get to the middle
        FLat:=FLat+0.5/24.0; //Add half a subsquare to get to the middle

      end;

    end else begin

      FLon:=FLon+2.0/2; //Add half a square to get to the middle
      FLat:=FLat+1.0/2; //Add half a square to get to the middle

    end;

  end else begin
    FLon:=FLon+20.0/2.0; //Add half a field to get to the middle
    FLat:=FLat+10.0/2.0;  //Add half a field to get to the middle
  end;

  include(CurrentValid,gcSphere);
end;

function TGeoCoord.RadiiAtLat(GeocLat: double): double;

var
  t : double;
  n : double;

begin
  t:= power(a2*cos(GeocLat),2) + power(b2*sin(GeocLat),2);
  n:= power(a*cos(GeocLat),2) + power(b*sin(GeocLat),2);
  result:=sqrt(t/n);
end;

procedure TGeoCoord.SetCartesian(const Value : T3DVector);
begin
  FCartesian:=Value;
  CurrentExact:=gcCartesian;
  CurrentValid:=[gcCartesian];
end;

procedure TGeoCoord.SetGeodetic(Lat, Lon: double);
begin
  FLat:=Lat;
  FLon:=Lon;
  CurrentExact:=gcSphere;
  CurrentValid:=[gcSphere];
end;

procedure TGeoCoord.SetLocator(const Value: string);
begin
  FLocator := Value;
  CurrentExact:=gcLoc;
  CurrentValid:=[gcLoc];
end;

procedure TGeoCoord.SetText(const Value: string);
begin
  FText:=Value;
  CurrentExact:=gcText;
  CurrentValid:=[gcText];
end;

procedure TGeoCoord.SphericalToCartesian;

var
  R : double;
  RadLat : double;
  RadLon : double;

begin
  //TODO: Geodetic to geocentric
  RadLat:=Misc.DegToRad(FLat);
  RadLon:=Misc.DegToRad(FLon);

  R:=RadiiAtLat(RadLat)+FHeight;

  FCartesian.X:=R*cos(RadLat)*cos(RadLon);
  FCartesian.Y:=R*cos(RadLat)*sin(RadLon);
  FCartesian.Z:=R*sin(RadLat);
end;

function get2ptCircle(P1, P2: T3DVector): TGeoCircle;

var
  Temp1 : T3DVector;

begin
  Temp1:=Subtract(P1,P2);
  Result.RadiusC:=Magnitude(Temp1)/2.0;
  Result.CenterC:=Add(Divide(Temp1,2),P2);
end;

function get3ptCircle(P1, P2, P3: T3DVector): TGeoCircle;

var
  a,b : T3DVector;

  r : double;

  temp1 : T3DVector;
  temp2 : T3DVector;
  temp3 : double;

begin
  A:=Subtract(P1,P3);
  B:=Subtract(P2,P3);

  R:=Magnitude(A)*Magnitude(B)*Magnitude(Subtract(A,B));
  R:=R/(2*Magnitude(CrossProduct(A,B)));

  Temp1:=Multiply(B,DotProduct(A,A));   //  ||a||^2 * b
  Temp2:=Multiply(A,DotProduct(B,B));   //  ||b||^2 * a

  Temp1:=Subtract(Temp1,Temp2);
  Temp2:=CrossProduct(A,B);
  Temp1:=CrossProduct(Temp1,Temp2);

  Temp3:=2*DotProduct(Temp2,Temp2);

  Temp1:=Add(Divide(Temp1,Temp3),P3);

  Result.RadiusC:=R;
  Result.CenterC:=Temp1;
end;

function InsideCircle(P : T3DVector; C : TGeoCircle) : boolean;

var
  RSqr : double;
  CVec : T3DVector;

begin
  RSqr:=C.RadiusC*C.RadiusC;
  CVec:=Subtract(C.CenterC,P);
  result:=RSqr>DotProduct(CVec,CVec);
end;

function trivial(const Points : TGeoCoordList): TGeoCircle;

var
  C : TGeoCircle;
  P1,P2,P3 : T3DVector;

begin
  if Length(Points)=0 then begin
    P1.X:=0.0;
    P1.Y:=0.0;
    P1.Z:=0.0;
    C.CenterC:=P1;
    C.RadiusC:=0.0;
  end else if Length(Points)=1 then begin
    C.CenterC:=Points[0].Cartesian;
    C.RadiusC:=0.0;
  end else begin
    P1:=TGeoCoord(Points[0]).Cartesian;
    P2:=TGeoCoord(Points[1]).Cartesian;
    if Length(Points)>1 then begin
      //P1 -- P2
      C:=get2ptCircle(P1,P2);
    end;
    if Length(Points)>2 then begin
      P3:=TGeoCoord(Points[2]).Cartesian;
      //P2 -- P3
      if not(InsideCircle(P3,C)) then begin
        C:=get2ptCircle(P2,P3);

        //P1 -- P3
        if not(InsideCircle(P1,C)) then begin
          C:=get2ptCircle(P1,P3);

          //P1-P2-P3
          if not(InsideCircle(P2,C)) then begin
            C:=get3ptCircle(P1,P2,P3);

          end;
        end;
      end;
    end;
  end;
  Result:=C;
end;

function Welzl(P, R : TGeoCoordList) : TGeoCircle;

var
  D : TGeoCircle;
  a : TGeoCoord;
  ItemP : integer;
  SubP : TGeoCoordList;
  SubR : TGeoCoordList;
  i : integer;


begin
  if (Length(P)=0) or (Length(R)=3) then
    Result:=trivial(R)
  else begin
    //choose p in P (randomly and uniformly)
    ItemP:=RandomRange(0,Length(P)-1);

    //Remove the point from the set P
    SetLength(SubP,Length(P));
    for i:=0 to length(P)-1 do
      SubP[i]:=P[i];
    A:=SubP[ItemP];
    SubP[ItemP]:=SubP[length(SubP)-1]; //Replace with last point
    SubP[Length(SubP)-1]:=nil;
    setlength(SubP,Length(SubP)-1); //Shorten array

    //D:=welzl(P-{p},R)
    D:=Welzl(SubP,R);

    //if p in in D then
    if InsideCircle(A.Cartesian,D) then
      //return D
      result:=D
    else begin
      //return welzl(P-{p},R U {p})

      //add a to R
      SetLength(SubR,Length(R));
      for i:=0 to length(R)-1 do
        SubR[i]:=R[i];
      SetLength(SubR,length(SubR)+1);
      SubR[Length(SubR)-1]:=A;

      result:=welzl(SubP,SubR);
    end;
  end;
end;

end.
