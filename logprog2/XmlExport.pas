unit XmlExport;

{
  Purpose:
    Provide possibility to export logbook as XML file.
  Status:
    - Should use a proper XML component for writing
    - Add selection of what to export
    - Some fields removed as they are not in the QSO table. The query must
       cross over to other tables.
    - More data on QSLs and as sub-elements. Current implementation for compatibility only.
    - More data on award items as sub-elements. Current implementation for compatibility only.
  Date:
    2021-03-28
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls, ComCtrls;

type
  TdlgExportXML = class(TForm)
    btnExport: TButton;
    ProgressBar1: TProgressBar;
    btnClose: TButton;
    edtExportFile: TEdit;
    spdExportFile: TSpeedButton;
    odFilename: TOpenDialog;
    procedure spdExportFileClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
  private
    { Private declarations }
    XMLFile : TextFile;

    procedure Fileit(Element : string; Content : string);
    procedure ExportQSLR(QSO_Id : integer);
    procedure ExportQSLS(QSO_Id : integer);
    procedure ExportAwards(QSO_Id : integer);
    procedure ExportQSO;
  public
    { Public declarations }
  end;

var
  dlgExportXML: TdlgExportXML;

implementation

{$R *.dfm}

uses
  Settings, ExportData;

procedure TdlgExportXML.spdExportFileClick(Sender: TObject);
begin
  if odFilename.Execute then begin
    edtExportFile.Text:=odFilename.FileName;
  end;
end;

procedure TdlgExportXML.FormShow(Sender: TObject);
begin
  edtExportFile.Text:=dmSettings.LogXmlExportPath;
end;

procedure TdlgExportXML.Fileit(Element : string; Content : string);

var
  i : integer;
  Str : string;


begin
  for i:=1 to length(Content) do begin
    case Content[i] of
    '&' : Str:=Str+'&amp;';
    '<' : Str:=Str+'&lt;';
    '>' : Str:=Str+'&gt;';
    else
      str:=Str+Content[i];
    end;
  end;
  if Str<>'' then
    WriteLn(XMLFile,'<'+Element+'>',Str,'</'+Element+'>')
  else
    WriteLn(XMLFile,'<'+Element+'/>');
end;

procedure TdlgExportXML.ExportQSO;

var
  B : string;
  
begin
  with dmDataExport.qryLog.Fields do begin
    WriteLn(XMLFile,'<Record>');
    Fileit('DateOn',FormatDateTime('yyyy-mm-dd',FieldByName('STARTTIME').AsDateTime));
    Fileit('TimeOn',FormatDateTime('hh:nn',FieldByName('STARTTIME').AsDateTime));
    B:=FieldByName('ENDTIME').AsString;
    if not(FieldByName('ENDTIME').IsNull) and (length(B)>10) then
      Fileit('DateOff',FormatDateTime('hh:nn',FieldByName('ENDTIME').AsDateTime))
    else
      Fileit('DateOff','');
    Fileit('Call',FieldByName('CALL').AsString);
    FileIt('IsSilentKey',FieldByName('ISSILENTKEY').AsString);
    Fileit('RSTs',FieldByName('RPT_S').AsString);
    Fileit('RSTr',FieldByName('RPT_R').AsString);
    Fileit('FreqUp',FieldByName('FREQUENCY_TX').AsString);
    Fileit('FreqDn',FieldByName('FREQUENCY_RX').AsString);
    Fileit('Mode',FieldByName('REFMODE').AsString);
//    Fileit('QSLSent','');//FieldByName('QSL_S').AsString);
    ExportQSLS(FieldByName('QSO_Id').AsInteger);
//    Fileit('QSLRcvd',''); //FieldByName('QSL_R').AsString);
    ExportQSLR(FieldByName('QSO_Id').AsInteger);
    Fileit('Propagation',FieldByName('REFPROPAGATIONNAME').AsString);
    Fileit('Locator',FieldByName('LOCATOR').AsString);
    if FieldByName('CSNICK').AsString<>'' then
      FileIt('Name',FieldByName('CSNICK').AsString+' ('+FieldByName('CSNAME').AsString+')')
    else
      Fileit('Name',FieldByName('CSNAME').AsString);
    Fileit('Place',FieldByName('QTH').AsString);
    Fileit('Notes',FieldByName('COMMENT').AsString);
    Fileit('Complete',FieldByName('COMPLETE').AsString);
    Fileit('OwnCall',FieldByName('REFCALL').AsString);
    Fileit('OwnLoc',FieldByName('LOCATOR1').AsString);
    Fileit('Random',FieldByName('RANDOM').AsString);
    Fileit('RootCall',FieldByName('REFBASECALL').AsString);
    Fileit('DxccCode',FieldByName('REFDXCC_ID').AsString);
    Fileit('Pwr','');//FieldByName('PWR').AsString);
//    Fileit('Rig',ADOTable1Rig.AsString);
    Fileit('Manager',FieldByName('CSMGR').AsString);
    ExportAwards(FieldByName('QSO_Id').AsInteger);
    Fileit('Sat',FieldByName('SATNAME').AsString);
    Fileit('SatMode',FieldByName('SAT_MODE').AsString);
    WriteLn(XMLFile,'</Record>');
  end;
end;

procedure TdlgExportXML.btnExportClick(Sender: TObject);
begin
  btnClose.Enabled:=false;
  Application.ProcessMessages;

  AssignFile(XMLFile,edtExportFile.Text);
  Rewrite(XMLFile);
  WriteLn(XMLFile,'<?xml version="1.0" encoding="ISO-8859-1" ?>');
  WriteLn(XMLFile,'<Log>');

  //The actual export
  dmDataExport.qryLog.Open;
  while not(dmDataExport.qryLog.Eof) do begin
    ExportQSO;
    dmDataExport.qryLog.Next;
  end;
  dmDataExport.qryLog.Close;

  WriteLn(XMLFile,'</Log>');
  CloseFile(XMLFile);


  dmSettings.LogXmlExportPath:=edtExportFile.Text;

  btnClose.Enabled:=true;
end;

procedure TdlgExportXML.ExportQSLR(QSO_Id : integer);

var
  QSLString : string;

begin
  with dmDataExport.qryReceivedQSL do begin
    ParamByName('QSO_Id').AsInteger:=QSO_Id;
    Open;
    if not(EOF) then begin
      //Open element
      QSLString:='';
      while not(EOF) do begin
        QSLString:=QSLString+Copy(FieldByName('Name').AsString,1,1);
//        FileIt('QSL_Rcvd',FieldByName('Name').AsString);
        Next;
      end;
      FileIt('QSLRcvd',QSLString);
      //Close element
    end;
    Close
  end;
end;

procedure TdlgExportXML.ExportQSLS(QSO_Id: integer);

var
  QSLString : string;

begin
  with dmDataExport.qrySentQSL do begin
    ParamByName('QSO_Id').AsInteger:=QSO_Id;
    Open;
    if not(EOF) then begin
      //Open element
      QSLString:='';
      while not(EOF) do begin
        QSLString:=QSLString+Copy(FieldByName('Name').AsString,1,1);
//        FileIt('QSL_Rcvd',FieldByName('Name').AsString);
        Next;
      end;
      FileIt('QSLSent',QSLString);
      //Close element
    end;
    Close;
  end;
end;

procedure TdlgExportXML.ExportAwards(QSO_Id: integer);
begin
  with dmDataExport.qryQSOAwardItems do begin
    ParamByName('QSO_Id').AsInteger:=QSO_Id;
    OPen;
    while not(EOF) do begin
      FileIt(FieldByName('ADIF_FIELD').AsString,FieldByName('Name').AsString);
      Next;
    end;
    Close;
  end;
end;

end.
