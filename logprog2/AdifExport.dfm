object dlgExportADIF: TdlgExportADIF
  Left = 2311
  Top = 275
  Width = 649
  Height = 331
  Caption = 'Export log in ADIF format'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object spdExportFile: TSpeedButton
    Left = 608
    Top = 168
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = spdExportFileClick
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 64
    Height = 13
    Caption = 'ADIF-version:'
  end
  object Label2: TLabel
    Left = 8
    Top = 56
    Width = 39
    Height = 13
    Caption = 'Callsign:'
  end
  object Label3: TLabel
    Left = 168
    Top = 56
    Width = 39
    Height = 13
    Caption = 'Locator:'
  end
  object Label4: TLabel
    Left = 8
    Top = 152
    Width = 54
    Height = 13
    Caption = 'Export path'
  end
  object Label5: TLabel
    Left = 8
    Top = 104
    Width = 39
    Height = 13
    Caption = 'Purpose'
  end
  object btnExport: TButton
    Left = 8
    Top = 208
    Width = 75
    Height = 25
    Caption = 'Export'
    TabOrder = 4
    OnClick = btnExportClick
  end
  object pbExport: TProgressBar
    Left = 8
    Top = 240
    Width = 625
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 5
  end
  object btnClose: TButton
    Left = 560
    Top = 272
    Width = 75
    Height = 25
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 6
  end
  object edtExportFile: TEdit
    Left = 8
    Top = 168
    Width = 593
    Height = 21
    TabOrder = 3
  end
  object cbAdifVersion: TComboBox
    Left = 8
    Top = 24
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      '1.0'
      '2.1.4'
      '2.2.7'
      '3.0.2'
      '3.1.4')
  end
  object cbCalls: TComboBox
    Left = 8
    Top = 72
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 1
  end
  object cbLocators: TComboBox
    Left = 168
    Top = 72
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
  end
  object cbPurposes: TComboBox
    Left = 8
    Top = 120
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 7
    Text = 'cbPurposes'
    OnChange = cbPurposesChange
  end
  object chkUpdate: TCheckBox
    Left = 168
    Top = 120
    Width = 153
    Height = 17
    Caption = 'Update status after export'
    TabOrder = 8
  end
  object odFilename: TOpenDialog
    Left = 592
    Top = 8
  end
end
