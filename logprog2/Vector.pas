unit Vector;

{
  Purpose:
    Minimal vector library for us poor programmers who cant afford a new version of Delphi
  Status:
    Likely stable
  Date:
    2021-03-28
}

interface

type
  T3DVector = record
    case boolean of
      false : (All : array[0..2] of double);
      true  : (X : double;
               Y : double;
               Z : double;
              );
  end;

  function Divide(vec : T3DVector; Divisor : double) : T3DVector;
  function Multiply(vec : T3DVector; Multiplicand : double) : T3DVector;
  function Add(vec1,vec2 : T3DVector) : T3DVector;
  function Subtract(vec1,vec2 : T3DVector) : T3DVector;
  function Magnitude(vec : T3DVector) : double;
  procedure Normalize(var vec : T3DVector);
  function DotProduct(vec1,vec2 : T3DVector) : double;
  function CrossProduct(vec1,Vec2 : T3DVector) : T3DVector;

implementation

uses
  Math;

function Divide(vec : T3DVector; Divisor : double) : T3DVector;
begin
  Result.X:=vec.X/Divisor;
  Result.Y:=Vec.Y/Divisor;
  Result.Z:=Vec.Z/Divisor;
end;

function Multiply(vec : T3DVector; Multiplicand : double) : T3DVector;
begin
  Result.X:=vec.X*Multiplicand;
  Result.Y:=vec.Y*Multiplicand;
  Result.Z:=vec.Z*Multiplicand;
end;

function Add(vec1,vec2 : T3DVector) : T3DVector;
begin
  Result.X:=vec1.X+Vec2.X;
  Result.Y:=vec1.Y+Vec2.Y;
  Result.Z:=vec1.Z+Vec2.Z;
end;

function Subtract(vec1,vec2 : T3DVector) : T3DVector;
begin
  Result.X:=vec1.X-Vec2.X;
  Result.Y:=vec1.Y-Vec2.Y;
  Result.Z:=vec1.Z-Vec2.Z;
end;

function Magnitude(vec : T3DVector) : double;
begin
  Result:=Sqrt(vec.X*vec.X+Vec.Y*Vec.Y+Vec.Z*Vec.Z);
end;

procedure Normalize(var vec : T3DVector);
begin
  vec:=Divide(vec,Magnitude(vec));
end;

function DotProduct(vec1,vec2 : T3DVector) : double;
begin
  Result:=vec1.X*Vec2.X+Vec1.Y*Vec2.Y+Vec1.Z*Vec2.Z;
end;

function CrossProduct(vec1,Vec2 : T3DVector) : T3DVector;
begin
  Result.X:=Vec1.Y*Vec2.Z-Vec1.Z*Vec2.Y;
  Result.Y:=Vec1.Z*Vec2.X-Vec1.X*Vec2.Z;
  Result.Z:=Vec1.X*Vec2.Y-Vec1.Y*Vec2.X;
end;

end.
