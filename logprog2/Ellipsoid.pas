unit Ellipsoid;

interface

type
  TEllipsoid = class(TObject)
  protected
    Semi_Major_Axis : double;
    Inverse_Flattening : double;
    First_Eccentricity_Squared : double;
  public
    function M(sin_geod_lat : double) : double;
    function N(sin_geod_lat : double) : double;
  end;

implementation

{ TEllipsoid }

function TEllipsoid.M(sin_geod_lat: double): double;
begin
  result:=0;
end;

function TEllipsoid.N(sin_geod_lat: double): double;
begin
  result:=0;
end;

end.
 