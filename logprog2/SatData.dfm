object dmSatData: TdmSatData
  OldCreateOrder = False
  Left = 2381
  Top = 320
  Height = 313
  Width = 371
  object tblSats: TIBTable
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'SATELLITE'
    Left = 96
    Top = 16
  end
  object tblElements: TIBTable
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    TableName = 'ELEMENTSET'
    Left = 96
    Top = 80
  end
  object qryFindSat: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from satellite where satellite_id=:satellite_id')
    Left = 24
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'satellite_id'
        ParamType = ptUnknown
      end>
  end
  object qryFindElement: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select * from elementset where REFSATELLITE_ID=:satellite_id and' +
        ' ELEMENTSET_ID=:elementset_id')
    Left = 24
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'satellite_id'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'elementset_id'
        ParamType = ptUnknown
      end>
  end
  object qryGetElement: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from elementset where'
      ' refsatellite_id=:satellite_id and '
      '(epoch_year*1000+epoch_day)<=:epoch'
      'order by (epoch_year*1000+epoch_day) desc')
    Left = 24
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'satellite_id'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'epoch'
        ParamType = ptUnknown
      end>
  end
  object qryActiveSats: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from satellite where opstart<:thedate and'
      '(opend>:thedate or opend is null) order by satellite_id')
    Left = 168
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'thedate'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'thedate'
        ParamType = ptUnknown
      end>
  end
end
