object dlgConstellation: TdlgConstellation
  Left = 1445
  Top = 173
  Width = 470
  Height = 651
  Caption = 'Satellite Constellation'
  Color = clBtnFace
  Constraints.MinHeight = 651
  Constraints.MinWidth = 470
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefaultPosOnly
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pbConstellation: TPaintBox
    Left = 0
    Top = 0
    Width = 462
    Height = 400
    Align = alClient
    PopupMenu = PopupMenu1
    OnPaint = pbConstellationPaint
  end
  object Splitter1: TSplitter
    Left = 0
    Top = 400
    Width = 462
    Height = 4
    Cursor = crVSplit
    Align = alBottom
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 605
    Width = 462
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 100
      end
      item
        Width = 100
      end
      item
        Width = 50
      end>
    PopupMenu = PopupMenu1
    SimplePanel = False
  end
  object OvcTable1: TOvcTable
    Left = 0
    Top = 404
    Width = 462
    Height = 160
    Align = alBottom
    GridPenSet.NormalGrid.NormalColor = clBtnShadow
    GridPenSet.NormalGrid.Style = psDot
    GridPenSet.NormalGrid.Effect = geBoth
    GridPenSet.LockedGrid.NormalColor = clBtnShadow
    GridPenSet.LockedGrid.Style = psSolid
    GridPenSet.LockedGrid.Effect = ge3D
    GridPenSet.CellWhenFocused.NormalColor = clBlack
    GridPenSet.CellWhenFocused.Style = psSolid
    GridPenSet.CellWhenFocused.Effect = geBoth
    GridPenSet.CellWhenUnfocused.NormalColor = clBlack
    GridPenSet.CellWhenUnfocused.Style = psDash
    GridPenSet.CellWhenUnfocused.Effect = geBoth
    LockedRowsCell = OvcTCColHead1
    TabOrder = 1
    OnGetCellData = OvcTable1GetCellData
    OnGetCellAttributes = OvcTable1GetCellAttributes
    CellData = (
      'dlgConstellation.OvcTCColHead1'
      'dlgConstellation.OvcTCSimpleField1')
    RowData = (
      20)
    ColData = (
      50
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      44
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      44
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      50
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      50
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      10
      False
      False
      44
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      44
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      50
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      50
      False
      True
      'dlgConstellation.OvcTCSimpleField1'
      150
      False
      False)
  end
  object Panel1: TPanel
    Left = 0
    Top = 564
    Width = 462
    Height = 41
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 34
      Height = 13
      Caption = 'Timmar'
    end
    object Label2: TLabel
      Left = 96
      Top = 16
      Width = 35
      Height = 13
      Caption = 'Minuter'
    end
    object speHourOfs: TSpinEdit
      Left = 48
      Top = 8
      Width = 41
      Height = 22
      MaxValue = 26
      MinValue = -26
      TabOrder = 0
      Value = 0
    end
    object speMinOfs: TSpinEdit
      Left = 136
      Top = 8
      Width = 41
      Height = 22
      MaxValue = 15
      MinValue = -15
      TabOrder = 1
      Value = 0
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 16
    object itmRealTime: TMenuItem
      Caption = 'Real-Time'
      OnClick = itmRealTimeClick
    end
    object itmQSOTime: TMenuItem
      Caption = 'QSO-Time'
      OnClick = itmQSOTimeClick
    end
    object itmSetOtherLoc: TMenuItem
      Caption = 'Set "Other" Locator'
      OnClick = itmSetOtherLocClick
    end
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 320
    Top = 16
  end
  object OvcTCColHead1: TOvcTCColHead
    Headings.Strings = (
      'Satellite'
      'Az'
      'El'
      'AOS'
      'LOS'
      ''
      'Az'
      'El'
      'AOS'
      'LOS'
      '')
    ShowLetters = False
    Table = OvcTable1
    Left = 200
    Top = 32
  end
  object OvcTCSimpleField1: TOvcTCSimpleField
    Adjust = otaTopRight
    CaretOvr.Shape = csBlock
    EFColors.Disabled.BackColor = clWindow
    EFColors.Disabled.TextColor = clGrayText
    EFColors.Error.BackColor = clRed
    EFColors.Error.TextColor = clBlack
    EFColors.Highlight.BackColor = clHighlight
    EFColors.Highlight.TextColor = clHighlightText
    Table = OvcTable1
    Left = 248
    Top = 48
    RangeHigh = {00000000000000000000}
    RangeLow = {00000000000000000000}
  end
end
