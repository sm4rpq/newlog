object dlgExportSquareList: TdlgExportSquareList
  Left = 2236
  Top = 344
  Width = 1009
  Height = 617
  Caption = 'Export Square list for QGIS'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object spdExportFile: TSpeedButton
    Left = 728
    Top = 472
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = spdExportFileClick
  end
  object Label1: TLabel
    Left = 520
    Top = 280
    Width = 123
    Height = 13
    Caption = 'Valda ryms inom: nnnn km'
  end
  object Label2: TLabel
    Left = 8
    Top = 312
    Width = 44
    Height = 13
    Caption = 'Trafiks'#228'tt'
  end
  object Label3: TLabel
    Left = 264
    Top = 312
    Width = 69
    Height = 13
    Caption = 'V'#229'gutbredning'
  end
  object btnRun: TButton
    Left = 359
    Top = 528
    Width = 75
    Height = 25
    Caption = 'Run'
    TabOrder = 0
    OnClick = btnRunClick
  end
  object pbProgress: TProgressBar
    Left = 8
    Top = 504
    Width = 713
    Height = 16
    Min = 0
    Max = 25
    TabOrder = 1
  end
  object btnClose: TButton
    Left = 679
    Top = 560
    Width = 75
    Height = 25
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 2
  end
  object edtExportFile: TEdit
    Left = 8
    Top = 472
    Width = 713
    Height = 21
    TabOrder = 3
    Text = 'loc50.csv'
  end
  object lvUsedCalls: TListView
    Left = 8
    Top = 40
    Width = 233
    Height = 225
    Checkboxes = True
    Columns = <
      item
        Caption = 'Anv.Signal'
        Width = 100
      end
      item
      end>
    TabOrder = 4
    ViewStyle = vsReport
  end
  object lvOperatorCalls: TListView
    Left = 264
    Top = 40
    Width = 233
    Height = 225
    Checkboxes = True
    Columns = <
      item
        Caption = 'Operat'#246'r'
        Width = 100
      end
      item
      end>
    TabOrder = 5
    ViewStyle = vsReport
  end
  object lvUsedLocations: TListView
    Left = 520
    Top = 40
    Width = 233
    Height = 225
    Checkboxes = True
    Columns = <
      item
        Caption = 'Lokator'
        Width = 100
      end
      item
        Caption = 'Plats'
      end>
    TabOrder = 6
    ViewStyle = vsReport
    OnMouseDown = lvUsedLocationsMouseDown
  end
  object cbxOperatingCondition: TComboBox
    Left = 8
    Top = 8
    Width = 745
    Height = 21
    ItemHeight = 13
    TabOrder = 7
    OnSelect = cbxOperatingConditionSelect
  end
  object lvBand: TListView
    Left = 760
    Top = 40
    Width = 233
    Height = 225
    Checkboxes = True
    Columns = <
      item
        Caption = 'Band'
        Width = -1
        WidthType = (
          -1)
      end>
    Items.Data = {
      7F0000000500000000000000FFFFFFFFFFFFFFFF000000000000000002444300
      000000FFFFFFFFFFFFFFFF000000000000000002353000000000FFFFFFFFFFFF
      FFFF00000000000000000331343400000000FFFFFFFFFFFFFFFF000000000000
      00000334333200000000FFFFFFFFFFFFFFFF00000000000000000431323936}
    TabOrder = 8
    ViewStyle = vsReport
  end
  object btnAllBands: TButton
    Left = 760
    Top = 272
    Width = 233
    Height = 25
    Caption = 'V'#228'lj alla'
    TabOrder = 9
    OnClick = btnAllBandsClick
  end
  object ComboBox1: TComboBox
    Left = 760
    Top = 8
    Width = 233
    Height = 21
    ItemHeight = 13
    TabOrder = 10
    Text = 'ComboBox1'
  end
  object cbxMode: TComboBox
    Left = 8
    Top = 328
    Width = 233
    Height = 21
    ItemHeight = 13
    TabOrder = 11
    Text = 'cbxMode'
  end
  object cbxProp: TComboBox
    Left = 264
    Top = 328
    Width = 233
    Height = 21
    ItemHeight = 13
    TabOrder = 12
    Text = 'cbxProp'
  end
  object btnAllUsedCalls: TButton
    Left = 8
    Top = 272
    Width = 233
    Height = 25
    Caption = 'V'#228'lj alla'
    TabOrder = 13
    OnClick = btnAllUsedCallsClick
  end
  object odFilename: TOpenDialog
    DefaultExt = '.txt'
    Left = 48
    Top = 528
  end
end
