unit OtherStatDlg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls;

type
  TdlgStatOther = class(TForm)
    rgCallsigns: TRadioGroup;
    rgPlace: TRadioGroup;
    rgFrequency: TRadioGroup;
    rgPropagation: TRadioGroup;
    redStat: TRichEdit;
    btnClose: TButton;
    cbxAward: TComboBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgStatOther: TdlgStatOther;

implementation

uses
  ExportData;

{$R *.dfm}

procedure TdlgStatOther.FormShow(Sender: TObject);
begin
  cbxAward.Clear;
  with dmDataExport.qryAwards do begin
    Open;
    while not(EOF) do begin
      if UpperCase(FieldByName('Name').AsString)<>'VUCC' then
        cbxAward.AddItem(FieldByName('Name').AsString,TObject(FieldByName('Award_Id').AsInteger));
      Next;
    end;
    Close
  end;
end;

end.
