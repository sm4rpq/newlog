program logprog;

{
  Purpose:
  Status:
  Date:
}

uses
  Forms,
  main in 'main.pas' {frmMain},
  CSVImportDlg in 'CSVImportDlg.pas' {dlgCSVImport},
  Connection in 'Connection.pas' {dmConnection: TDataModule},
  ExportSquareListDlg in 'ExportSquareListDlg.pas' {dlgExportSquareList},
  ExportData in 'ExportData.pas' {dmDataExport: TDataModule},
  XmlExport in 'XmlExport.pas' {dlgExportXML},
  AdifExport in 'AdifExport.pas' {dlgExportADIF},
  CSVFile in 'CSVFile.pas',
  ImportData in 'ImportData.pas' {dmDataImport: TDataModule},
  Mystrlist in '..\base_data\mystrlist.pas',
  TaclogImportDlg in 'TaclogImportDlg.pas' {dlgTaclogImport},
  tacfile in '..\tac2adif\tacfile.pas',
  Misc in '..\tac2adif\MISC.PAS',
  GeoUnit in 'GeoUnit.pas',
  Vector in 'Vector.pas',
  XMLImportDlg in 'XMLImportDlg.pas' {dlgXMLImport},
  SquareStatDlg in 'SquareStatDlg.pas' {dlgStatSquare},
  DXCCStatDlg in 'DXCCStatDlg.pas' {dlgStatDXCC},
  MainFilterDlg in 'MainFilterDlg.pas' {dlgMainFilter},
  OtherStatDlg in 'OtherStatDlg.pas' {dlgStatOther},
  TLEImportDlg in 'TLEImportDlg.pas' {dlgTLEImport},
  spaceobject in 'spaceobject.pas',
  geoconstants in 'GeoConstants.pas',
  SatData in 'SatData.pas' {dmSatData: TDataModule},
  groundstation in 'groundstation.pas',
  SatPath in 'SatPath.pas',
  ConstellationDlg in 'ConstellationDlg.pas' {dlgConstellation},
  Ellipsoid in 'Ellipsoid.pas',
  WGS84 in 'WGS84.pas',
  Common in 'Common.pas',
  Settings in 'Settings.pas' {dmSettings: TDataModule},
  ExtraControls in 'ExtraControls.pas',
  QSLRulesDlg in 'QSLRulesDlg.pas' {dlgQSLRules},
  QSLRules in 'QSLRules.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TdlgCSVImport, dlgCSVImport);
  Application.CreateForm(TdmConnection, dmConnection);
  Application.CreateForm(TdlgExportSquareList, dlgExportSquareList);
  Application.CreateForm(TdmDataExport, dmDataExport);
  Application.CreateForm(TdlgExportXML, dlgExportXML);
  Application.CreateForm(TdlgExportADIF, dlgExportADIF);
  Application.CreateForm(TdmDataImport, dmDataImport);
  Application.CreateForm(TdlgTaclogImport, dlgTaclogImport);
  Application.CreateForm(TdlgXMLImport, dlgXMLImport);
  Application.CreateForm(TdlgStatSquare, dlgStatSquare);
  Application.CreateForm(TdlgStatDXCC, dlgStatDXCC);
  Application.CreateForm(TdlgMainFilter, dlgMainFilter);
  Application.CreateForm(TdlgStatOther, dlgStatOther);
  Application.CreateForm(TdlgTLEImport, dlgTLEImport);
  Application.CreateForm(TdmSatData, dmSatData);
  Application.CreateForm(TdlgConstellation, dlgConstellation);
  Application.CreateForm(TdmSettings, dmSettings);
  Application.CreateForm(TdlgQSLRules, dlgQSLRules);
  Application.Run;
end.
