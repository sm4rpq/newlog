object dlgStatOther: TdlgStatOther
  Left = 1923
  Top = 108
  Width = 841
  Height = 617
  Caption = 'Statistik f'#246'r andra diplom'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object rgCallsigns: TRadioGroup
    Left = 8
    Top = 8
    Width = 185
    Height = 121
    Caption = 'Signaler'
    ItemIndex = 0
    Items.Strings = (
      'B'#229'da signalerna'
      'SM4RPP'
      'SM4RPQ')
    TabOrder = 0
  end
  object rgPlace: TRadioGroup
    Left = 200
    Top = 8
    Width = 185
    Height = 121
    Caption = 'Platser'
    ItemIndex = 1
    Items.Strings = (
      'Alla platser'
      'Hemma'
      'Stugan')
    TabOrder = 1
  end
  object rgFrequency: TRadioGroup
    Left = 392
    Top = 8
    Width = 185
    Height = 121
    Caption = 'Frekvenser'
    ItemIndex = 2
    Items.Strings = (
      'Alla band'
      'Kortv'#229'g'
      '6m'
      '2m'
      '70cm'
      '23cm')
    TabOrder = 2
  end
  object rgPropagation: TRadioGroup
    Left = 584
    Top = 8
    Width = 185
    Height = 121
    Caption = 'V'#229'gutbredning'
    ItemIndex = 1
    Items.Strings = (
      'Allt'
      'Ej repeater eller satellit'
      'Aurora'
      'Satellit'
      'Meteorscatter')
    TabOrder = 3
  end
  object redStat: TRichEdit
    Left = 8
    Top = 160
    Width = 817
    Height = 385
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object btnClose: TButton
    Left = 752
    Top = 552
    Width = 75
    Height = 25
    Caption = 'St'#228'ng'
    ModalResult = 1
    TabOrder = 5
  end
  object cbxAward: TComboBox
    Left = 8
    Top = 136
    Width = 761
    Height = 21
    ItemHeight = 13
    TabOrder = 6
    Text = 'cbxAward'
  end
end
