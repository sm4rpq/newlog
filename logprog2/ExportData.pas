unit ExportData;

{
  Purpose:
    Datamodule with components for accessing logbook during exports
  Status:
    Work in progress
  Date:
    2021-03-28
}

interface

uses
  SysUtils, Classes, Connection, DB, IBCustomDataSet, IBQuery, IBSQL;

type
  TdmDataExport = class(TDataModule)
    qryLog: TIBQuery;
    qryUsedCalls: TIBQuery;
    qryUsedLocs: TIBQuery;
    qryOperators: TIBQuery;
    qryOperatingCondition: TIBQuery;
    qrySqrsQGIS: TIBQuery;
    qryRefModes: TIBQuery;
    qryRefProp: TIBQuery;
    qryReceivedQSL: TIBQuery;
    qrySentQSL: TIBQuery;
    qryQSOAwardItems: TIBQuery;
    qryQSOCount: TIBQuery;
    qryAwards: TIBQuery;
    qryLogServices: TIBQuery;
    qryOnlineLogInteraction: TIBQuery;
    qryOnlineLogInteractions: TIBQuery;
    qryInsertOLI: TIBSQL;
    qryUpdateOLI: TIBSQL;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmDataExport: TdmDataExport;

implementation

{$R *.dfm}

end.
