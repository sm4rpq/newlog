object dlgTaclogImport: TdlgTaclogImport
  Left = 2545
  Top = 162
  BorderStyle = bsDialog
  Caption = 'TACLOG-Import'
  ClientHeight = 214
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 158
    Height = 13
    Caption = 'Path to search for taclog .dat-files'
  end
  object SpeedButton1: TSpeedButton
    Left = 512
    Top = 24
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = SpeedButton1Click
  end
  object Label1: TLabel
    Left = 248
    Top = 88
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object edtImportPath: TEdit
    Left = 8
    Top = 24
    Width = 497
    Height = 21
    TabOrder = 0
    Text = 'D:\d\LOG\TACLOG'
  end
  object btnExecute: TButton
    Left = 8
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Execute'
    TabOrder = 1
    OnClick = btnExecuteClick
  end
  object OKBtn: TButton
    Left = 455
    Top = 180
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object ProgressBar1: TProgressBar
    Left = 192
    Top = 112
    Width = 150
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 3
  end
end
