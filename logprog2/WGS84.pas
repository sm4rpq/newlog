unit WGS84;

interface

uses
  Ellipsoid;

const
  a = 6378137.0;
  rf = 298.257223563;


type
  TWGS84 = class(TEllipsoid)
  private
    constructor Create;
    destructor Destroy; virtual;
  public
  end;

implementation

uses
  GeoUnit;

{ TWGS84 }

constructor TWGS84.Create;
begin
  Semi_Major_Axis:=a;
  Inverse_Flattening:=rf;
  First_Eccentricity_Squared:=0.0;
end;

destructor TWGS84.Destroy;
begin

end;

end.
