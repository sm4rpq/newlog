unit Connection;

{
  Purpose:
    Provides the overall database connection for the logging program.
    Also provides access to the generators for ids and the transaction
    object
  Status:
    Likely stable
  Date:
    2021-01-29

  Constraints:
    230 = Qso->Operatingmode
    231 = Qso->Contest
    233 = Qso->ContactedStation
    238 = Qso->UsedCall
    247 = QSOConfirmation->ConfirmationServiceInteraction
}

interface

uses
  SysUtils, Classes, IBDatabase, DB, IBCustomDataSet, IBStoredProc, IBQuery;

type
  TdmConnection = class(TDataModule)
    IBDatabase1: TIBDatabase;
    IBTransaction1: TIBTransaction;
    procGetUniqueId: TIBStoredProc;
    procGetUniqueIdUNIQUE_ID: TIntegerField;
    qryIdDxcc: TIBQuery;
    qryAwardId: TIBQuery;
    qryQSOList: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
    function Get_Unique_Id : integer;
    function Get_Dxcc_Id(Call : string; date : tdatetime) : integer;
    function Get_Award_Id(Award : String) : integer;
  end;

var
  dmConnection: TdmConnection;

implementation

{$R *.dfm}


{ TdmConnection }

function TdmConnection.Get_Award_Id(Award: String): integer;
begin
  Result:=0;
  with qryAwardId do begin
    ParamByName('Name').AsString:=Award;
    open;
    if not(eof) then begin
      Result:=FieldByName('Award_Id').AsInteger;
    end;
    Close;
  end;
end;

function TdmConnection.Get_Dxcc_Id(Call: string; date: tdatetime): integer;

var
  i : integer;
  d : integer;
  n : integer;

begin
  d:=0;
  n:=0;
  for i:=length(Call) downto 1 do begin
    if (n=0) then
      with qryIdDxcc do begin
        ParamByName('qsodate1').AsDate:=Date;
        ParamByName('qsodate2').AsDate:=Date;
        ParamByName('prefix').AsString:=Copy(Call,1,i);
        Open;
        if not(EOF) then begin
          d:=FieldByName('dxcc_id').AsInteger;
          inc(n);
        end;
        Close;
      end;
  end;
  if n=1 then
    result:=d
  else
    result:=0;
end;

function TdmConnection.Get_Unique_Id: integer;
begin
  procGetUniqueId.ExecProc;
  result:=procGetUniqueId.ParamByName('UNIQUE_ID').AsInteger;
end;

end.
