unit QSLRules;

interface

uses
  ComCtrls, IBQuery;

type
  TQSLRuleBase = class(TTreeNode)
  private
    function GetSQLWhere: string; virtual; abstract;
  public
    property SQLWhere : string read GetSQLWhere;
    function Matches(AQuery : TIBQuery) : boolean; virtual; abstract;
  end;

  TNewDXCCRule = class(TQSLRuleBase)
  private
    function GetSQLWhere : string;
  public
    function Matches(AQuery : TIBQuery) : boolean;
  end;

  TNewAwardItemRule = class(TQSLRuleBase)
  private
    function GetSQLWhere : string;
  public
    function Matches(AQuery : TIBQuery) : boolean;
  end;

implementation

{ TNewDXCCRule }

function TNewDXCCRule.GetSQLWhere: string;
begin

end;

function TNewDXCCRule.Matches(AQuery: TIBQuery): boolean;
begin

end;

{ TNewAwardItemRule }

function TNewAwardItemRule.GetSQLWhere: string;
begin

end;

function TNewAwardItemRule.Matches(AQuery: TIBQuery): boolean;
begin

end;

end.
