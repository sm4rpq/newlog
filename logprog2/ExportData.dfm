object dmDataExport: TdmDataExport
  OldCreateOrder = False
  Left = 2983
  Top = 534
  Height = 352
  Width = 383
  object qryLog: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select q.*,u.locator,sat.name as satname,cs.name as csname,cs.ni' +
        'ck as csnick,cs.manager as csmgr,cs.issilentkey, uc.name as ucna' +
        'me from qso q'
      
        ' inner join usedlocation u on u.usedlocation_id=q.refusedlocatio' +
        'n_id'
      
        ' inner join CONTACTEDSTATION cs on cs.basecall=q.REFBASECALL and' +
        ' cs.INSTANCE=q.REFINSTANCE'
      ' inner join USEDCALL uc on uc.CALL=q.REFCALL'
      ' left outer join'
      'satellite sat on sat.satellite_id=q.refsatellite_id'
      ' order by q.starttime')
    Left = 24
    Top = 8
  end
  object qryUsedCalls: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from usedcall order by call')
    Left = 24
    Top = 112
  end
  object qryUsedLocs: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from usedlocation order by usedlocation_id')
    Left = 144
    Top = 112
  end
  object qryOperators: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from operator order by opcall')
    Left = 88
    Top = 112
  end
  object qryOperatingCondition: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from operatingcondition')
    Left = 24
    Top = 64
  end
  object qrySqrsQGIS: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    Left = 24
    Top = 168
  end
  object qryRefModes: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select distinct refmode from qso where refmode is not null')
    Left = 208
    Top = 112
  end
  object qryRefProp: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select distinct refpropagationname from qso where refpropagation' +
        'name is not null')
    Left = 272
    Top = 112
  end
  object qryReceivedQSL: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from '
      ' QSOCONFIRMATION qc,'
      ' CONFIRMATIONSERVICE cs,'
      ' RECEIVEDCONFIRMATION rc'
      'where'
      '  qc.ASOCONFIRMATIONSERVICE_ID=cs.CONFIRMATIONSERVICE_ID and'
      '  rc.GENCONFIRMATION_ID=qc.CONFIRMATION_ID and'
      '  qc.ASOQSO_ID=:qso_id'
      'order by qc.ASOCONFIRMATIONSERVICE_ID,qc.REGISTEREDDATE')
    Left = 80
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'qso_id'
        ParamType = ptUnknown
      end>
  end
  object qrySentQSL: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from '
      ' QSOCONFIRMATION qc,'
      ' CONFIRMATIONSERVICE cs,'
      ' SENTCONFIRMATION sc'
      'where'
      '  qc.ASOCONFIRMATIONSERVICE_ID=cs.CONFIRMATIONSERVICE_ID and'
      '  sc.GENCONFIRMATION_ID=qc.CONFIRMATION_ID and'
      '  qc.ASOQSO_ID=:qso_id'
      'order by qc.ASOCONFIRMATIONSERVICE_ID,qc.REGISTEREDDATE')
    Left = 160
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'qso_id'
        ParamType = ptUnknown
      end>
  end
  object qryQSOAwardItems: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select a.ADIF_FIELD,ai.name,a.refdxcc_id from award a, awarditem' +
        ' ai, receivedawardcredit rac'
      
        'where ai.refaward_id=a.award_id and rac.asoawarditem_id=ai.award' +
        'item_id'
      'and a.ADIF_FIELD is not null and rac.asoqso_id=:qso_id'
      'order by a.NAME,ai.NAME')
    Left = 232
    Top = 24
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'qso_id'
        ParamType = ptUnknown
      end>
  end
  object qryQSOCount: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select count(*) From qso')
    Left = 320
    Top = 16
  end
  object qryAwards: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * From award')
    Left = 24
    Top = 224
  end
  object qryLogServices: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from onlinelogservice')
    Left = 88
    Top = 168
  end
  object qryOnlineLogInteraction: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from OnlineLogInteraction where'
      '  asoqso_id=:qsoid and asologservice_id=:logserviceid')
    Left = 144
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'qsoid'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'logserviceid'
        ParamType = ptUnknown
      end>
  end
  object qryOnlineLogInteractions: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select * from OnlineLogInteraction oli, OnlineLogService ols whe' +
        're'
      '  ols.logservice_id=oli.asologservice_id and'
      '  asoqso_id=:qsoid')
    Left = 208
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'qsoid'
        ParamType = ptUnknown
      end>
  end
  object qryInsertOLI: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      
        'insert into ONLINELOGINTERACTION(ASOQSO_ID, ASOLOGSERVICE_ID, RE' +
        'FINDICATOR, UPLOADDATE) values'
      '(:qso_id,:logservice_id,:refindicator,:uploaddate)')
    Transaction = dmConnection.IBTransaction1
    Left = 120
    Top = 224
  end
  object qryUpdateOLI: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'update ONLINELOGINTERACTION'
      'set REFINDICATOR=:refindicator,UPLOADDATE=:uploaddate'
      'where'
      'ASOQSO_ID=:qso_id and ASOLOGSERVICE_ID=:logservice_id')
    Transaction = dmConnection.IBTransaction1
    Left = 184
    Top = 224
  end
end
