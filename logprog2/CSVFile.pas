unit CSVFile;

{
  Purpose:
    Parser for csv-files in the format used to keep our 50MHz logs.
  Status:
    Discriminates SWL-reports
  Date:
    2021-02-02
}

interface

uses
  Classes;

type
  TCSVFile = class(TObject)
  private
    FPower: single;
    FRecNo: integer;
    FCallsign: string;
    FRSTs: string;
    FMode: string;
    FRSTr: string;
    FStartTime: TDateTime;
    FComplete: boolean;
    FO_Pwr: single;
    FManager: string;
    FNotes: string;
    FName: string;
    FLocator: string;
    FQTH: string;
    FQSLR: string;
    FQSLS: string;
    FO_Ant: string;
    FEndTime: TDateTime;
    FFilename: string;
    FReadFail: boolean;

    FOpen : boolean;
    FLoggen : TStringList;
    FIndex : integer;
    FDate : string;
    FTime : string;
    FSameTime : integer;
    FPropagation: String;
    FSquare: string;

    procedure SetFilename(const Value: string);
    procedure Fetch;
    function GetEOF: boolean;
  public
    constructor Create;
    destructor Destroy; override;

    procedure Open;
    procedure Next;
    procedure Close;
    property EOF : boolean read GetEOF;
    property ReadFail : boolean read FReadFail;

    property Filename : string read FFilename write SetFilename;
    property RecNo : integer read FRecNo;
    property StartTime : TDateTime read FStartTime;
    property Mode : string read FMode;
    property Power : single read FPower;
    property Callsign : string read FCallsign;
    property RSTs : string read FRSTs;
    property RSTr : string read FRSTr;
    property Locator : string read FLocator;
    property Name : string read FName;
    property QTH : string read FQTH;
    property O_Ant : string read FO_Ant;
    property O_Pwr : single read FO_Pwr;
    property EndTime : TDateTime read FEndTime;
    property QSLS : string read FQSLS;
    property QSLR : string read FQSLR;
    property Notes : string read FNotes;
    property Square : string read FSquare;
    property Complete : boolean read FComplete;
    property Manager : string read FManager;
    property Propagation : String read FPropagation;
  end;

implementation

uses
  SysUtils, Math;

{ TCSVFile }

procedure TCSVFile.Close;
begin
  FLoggen.Free;
end;

constructor TCSVFile.Create;
begin
  inherited;

  FPower:=0;
  FRecNo:=0;
  FCallsign:='';
  FRSTs:='';
  FMode:='';
  FRSTr:='';
  FStartTime:=0.0;
  FComplete:=false;
  FO_Pwr:=0.0;
  FManager:='';
  FNotes:='';
  FName:='';
  FLocator:='';
  FQTH:='';
  FQSLR:='';
  FQSLS:='';
  FO_Ant:='';
  FSquare:='';
  FEndTime:=0.0;
  FFilename:='';
  FReadFail:=false;
end;

destructor TCSVFile.Destroy;
begin
  if (FOpen) then
    Close;
  inherited;
end;

procedure TCSVFile.Fetch;


  function NextItem(var S : String) : string;

  var
    R : string;

  begin
    R:='';
    while (Length(S)>0) and (S[1]<>#9) do begin
      R:=R+S[1];
      Delete(S,1,1);
    end;
    if Length(S)>0 then
      Delete(S,1,1);
    Result:=R;
  end;

  function PowerOf(var S : string) : single;

  var
    UnitPos : integer;
    QuantityS : string;
    Quantity : single;
    OldDecSep : char;

  begin
    while (length(S)>0) and (S[1] in ['0'..'9', ',', '.']) do begin
      if S[1]=',' then
        QuantityS:=QuantityS+'.'
      else
        QuantityS:=QuantityS+S[1];
      Delete(S,1,1);
    end;

    try
      OldDecSep:=DecimalSeparator;
      DecimalSeparator:='.';
      Quantity:=StrToFloat(QuantityS);
      DecimalSeparator:=OldDecSep;
    except
      Quantity:=-1;
    end;

    if Quantity>=0 then begin
      UnitPos:=Pos(S+'#','mW#dBm#dBW#W#');
      try
        case UnitPos of
        1 : begin //mW
              Result:=Quantity/1000;
            end;
        4 : begin //dBm
              Result:=Math.Power(10,(Quantity-3));
            end;
        8 : begin //dBW
              Result:=Math.Power(10,Quantity);
            end;
        else
            begin //W
              Result:=Quantity
            end;
        end;
      except
        Result:=0;
      end;
    end else
      Result:=0;
  end;

var
  CurrentRow : string;
  Temp : string;
  RepPos : integer;
  Prop : string;


begin
  if not(EOF) then begin
    CurrentRow:=FLoggen[FIndex];

    //Remove any "'s from the read string
    RepPos:=Pos('�',CurrentRow);
    while RepPos>0 do begin
      CurrentRow:=Copy(CurrentRow,1,RepPos-1)+'"'+Copy(CurrentRow,RepPos+1,Length(CurrentRow)-RepPos);
      RepPos:=Pos('�',CurrentRow);
    end;

    //Pop paperlog record number off the string;
    Temp:=NextItem(CurrentRow);
    FRecNo:=0;
    if Temp<>'' then
      try
        FRecNo:=StrToInt(Temp);
      except
        FRecNo:=0;
      end;

    //Pop date off the string. We need to keep it because its only on rows where a new day starts
    Temp:=NextItem(CurrentRow); //Dat
    if Temp<>'' then FDate:=Temp;


    //pop time off the string.
    Temp:=NextItem(CurrentRow); //Tid

    //Special handling for repeating times. Add a second to the new one if
    //same time as last.
    if Temp=FTime then begin //Same time as last
       FSameTime:=FSameTime+1;
    end else begin
       FSameTime:=0;
    end;
    FReadFail:=false;
    try
      FStartTime:=StrToDateTime(FDate+' '+Temp+':'+Format('%2.2d',[FSameTime]));
    except
      FReadFail:=true;
    end;

    if not(FReadFail) then begin
      FTime:=Temp;

      //Pop Mode off string
      Temp:=NextItem(CurrentRow);
      if Temp<>'' then FMode:=Temp;

      //Pop Power off string
      Temp:=NextItem(CurrentRow);
      if Temp<>'' then FPower:=PowerOf(Temp);

      //Pop worked stations callsign off the string
      FCallsign:=NextItem(CurrentRow);

      //Pop given report off the string
      FRSTs:=NextItem(CurrentRow);
      if pos('A',FRSTs)>0 then Prop:='AUR';

      //Pop received report off the string
      FRSTr:=NextItem(CurrentRow);
      if pos('A',FRSTr)>0 then Prop:='AUR';

      //Pop locator off the string
      FLocator:=NextItem(CurrentRow);

      FName:=NextItem(CurrentRow);
      FQTH:=NextItem(CurrentRow);
      FO_Ant:=NextItem(CurrentRow);
      Temp:=NextItem(CurrentRow);
      if Temp<>'' then
        FO_Pwr:=PowerOf(Temp)
      else
        FO_Pwr:=0.0;

      //Pop end time off the string
      Temp:=NextItem(CurrentRow);
      if Temp<>'' then begin
        FEndTime:=StrToDateTime(FDate+' '+Temp);
      end else
        FEndTime:=0;

      FQSLS:=NextItem(CurrentRow);
      FQSLR:=NextItem(CurrentRow);

      FNotes:=NextItem(CurrentRow);

      FSquare:=NextItem(CurrentRow); // Square
      Temp:=NextItem(CurrentRow); // New # or not
      Temp:=NextItem(CurrentRow); // Empty

      FComplete:=Copy(NextItem(CurrentRow),1,1)<>'N';

      FManager:=NextItem(CurrentRow); //Manager

      Temp:=NextItem(CurrentRow); //Propagation
      if (Temp<>'') then
        Prop:=Temp;
      FPropagation:=Prop;

    end;
  end;
end;

function TCSVFile.GetEOF: boolean;
begin
  if FOpen then begin
    result:=FIndex>=FLoggen.Count;
  end else
    Result:=true;
end;

procedure TCSVFile.Next;
begin
  inc(FIndex);
  Fetch;
end;

procedure TCSVFile.Open;
begin
  if (FFilename<>'') then begin
    if FOpen then
      Close;
    FLoggen:=TStringList.Create;
    Floggen.LoadFromFile(FFilename);
    FIndex:=1;
    FOpen:=true;
    Fetch;
  end;
end;


procedure TCSVFile.SetFilename(const Value: string);
begin
  FFilename := Value;
end;

end.
