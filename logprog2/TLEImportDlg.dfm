object dlgTLEImport: TdlgTLEImport
  Left = 2609
  Top = 271
  Width = 510
  Height = 393
  Caption = 'Satellite element import (TLE)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 51
    Height = 13
    Caption = 'File to load'
  end
  object btnClose: TButton
    Left = 416
    Top = 328
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 0
    OnClick = btnCloseClick
  end
  object Button1: TButton
    Left = 8
    Top = 184
    Width = 75
    Height = 25
    Caption = 'Run import'
    TabOrder = 1
    OnClick = Button1Click
  end
  object edtSatName: TEdit
    Left = 8
    Top = 152
    Width = 121
    Height = 21
    Enabled = False
    TabOrder = 2
    Text = 'AO-10'
  end
  object rgFormat: TRadioGroup
    Left = 8
    Top = 64
    Width = 121
    Height = 73
    Caption = 'File Format'
    ItemIndex = 1
    Items.Strings = (
      '2-line'
      '3-line')
    TabOrder = 3
    OnClick = rgFormatClick
  end
  object edtTLEFileName: TEdit
    Left = 8
    Top = 24
    Width = 457
    Height = 21
    TabOrder = 4
    Text = 'D:\d\LOG\TLE\AO-10\ao-10.txt'
  end
  object Button2: TButton
    Left = 472
    Top = 24
    Width = 25
    Height = 25
    Caption = '...'
    TabOrder = 5
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'txt'
    Filter = '*.txt'
    Left = 432
    Top = 72
  end
end
