unit GeoConstants;

// Constants for satellite calculations.
// Originating from the program plan13.bas by James Miller, G3RUH.
// Checked and sometimes complemented towards other sources.
// Each non-trivial constant marked where the value comes from.

interface

(* WGS-84 Earth Ellipsoid *)
const RE = 6378137.0;                           // [m] Semi major axis. Source: NIMA TR8350.2 (3-1)
const FL = 1.0 / 298.257223563;                 // [-] Flattening. Source: NIMA TR8350.2 (3-2)

const RP = RE * (1.0 - FL);                     // [m] Semi minor axis. Derived
const XX = RE * RE;                             // [m^2], equator radius squared
const ZZ = RP * RP;                             // [m^2], polar radius squared

//**WARNING**
//I have not managed to track down an independent source for the value of this constant.
//In fact, all independent sources found show other values.
//More so, the tropical year can vary up to 30 minutes.
//As used in the original context, the plan13.bas program by James Miller, G3RUH, it is
//only used to calculate the rotational speed of the station looking at a satellite
//in order to then calculate the doppler shift. For this I believe it is ok to use
//just about any value since the influence on the end result will be minimal.
//The value should NOT be used for anything that leads to accumulation or multiplication
//over long times.
//
//Furthermore I'm not really sure if the use to find the rotational speed is correct
//either. As I have understood it, the tropical years length is the time it takes
//the earth to make one turn around the sun. The variations are due to the gravitational
//pull of other bodies in the solar system and precession.
//The rotation of earth around its own axis is much more stable varying only by fractions
//of a second from year to year. And my uneducated guess is that this rotation is what
//really should be used for rotational speed calculation.
//const YT = 365.2421970;                       // Tropical year, days - 1990ish version of plan13.bas
const YT = 365.2421874;                         // Tropical year, days - 2000 and 2014 version of plan13.bas
//**END-OF-WARNING**

const WW = 2.0 * Pi / YT;                       // [?] Earth's rotation rate, rads/whole day
const WE = 2.0 * Pi + WW;                       // [/day] Earth's rotation rate
const W0 = WE / 86400;                          // [/s] Earth's rotation rate

const GM : double = 3.986004418E14;             // [m^3/s^2] Earth's gravitational constant. Source: EGM96 (11.2-8)
const J2 : double = 1.08262668355315E-3;        // 2nd Zonal coeff, Earth's gravity Field. Source: ? orekit.org refers to it as EGM96 but is not in EGM96...

(* GHAA, Year YG, Jan 0.0 *)
//const YG : integer = 2010;
CONST YG : INTEGER = 1990;
//const G0 : double = 99.5578; //99d 33.468' (Navsoft nautical almanac 2009-12-31 : 99d 33.4')
CONST G0 : DOUBLE = 99.4033;

//1990 : 99.4033 = 99d 24.198'  (?)
//2000 : 98.9821 = 98d 58.926'  (Navsoft nautical almanac 1999-12-31 : 98d 58.7')
//2014 : 99.5828 = 99d 34.968'  (Navsoft nautical almanac 2013-12-31 : 99d 35.1')


(* MA Sun and rate, deg, deg/day *)
const MAS0 : double = 356.4485;
const MASD : double = 0.98560028;

(* Sideral and solar data. Never needs changing. Valid to year 2000+ *)

(* Sun's inclination *)
const INS = 23.4380 * Pi / 180.0;               // [�] Value from 1990 version of plan13. Source: Unknown
const CNS = 0.9174910254023181106384992436677147427224606896929056892831370080; //cos(INS);
const SNS = 0.3977564811617817792394520941690862752546178846094886437955259933; //sin(INS);

(* Sun's equation of center terms *)
const EQC1 = 0.03343;                           // [ ] Value from 1990 version of plan13. Source: Unknown
const EQC2 = 0.00035;                           // [ ] Value from 1990 version of plan13. Source: Unknown

function FNday(year, month, day : integer) : double;

implementation

(* Convert date to day number
*
* Function returns a general day number from year, month, and day.
* Value is (JulianDay - 1721409.5) or (AmsatDay + 722100)
* Valid only for dates between 1900-march-01 -- 2100-feb-28 since there
* is no compensation for mod 100 and mod 400 years.
*
*)
function FNday(year, month, day : integer) : double;

//**WARNING**
//This constant for a mean year is valid only for date calculations as done
//below.
const YM = 365.25;                              // Mean year, days
//**END-OF-WARNING**

var
  GeneralDate : double;

begin
  if (month <= 2) then begin
    dec(year);
    inc(month,12);
  end;

  GeneralDate := trunc(year * YM+0.0000005) + trunc((month + 1) * 30.6) + (day - 428);
  Result:=GeneralDate;
end;


end.
