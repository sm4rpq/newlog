object dlgExportXML: TdlgExportXML
  Left = 2310
  Top = 510
  Width = 649
  Height = 220
  Caption = 'Export log in XML format'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object spdExportFile: TSpeedButton
    Left = 608
    Top = 40
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = spdExportFileClick
  end
  object btnExport: TButton
    Left = 280
    Top = 88
    Width = 75
    Height = 25
    Caption = 'Export'
    TabOrder = 0
    OnClick = btnExportClick
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 16
    Width = 625
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 1
  end
  object btnClose: TButton
    Left = 280
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Close'
    ModalResult = 1
    TabOrder = 2
  end
  object edtExportFile: TEdit
    Left = 8
    Top = 40
    Width = 593
    Height = 21
    TabOrder = 3
  end
  object odFilename: TOpenDialog
    Left = 592
    Top = 88
  end
end
