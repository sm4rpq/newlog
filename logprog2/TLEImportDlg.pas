unit TLEImportDlg;

{
  Purpose:
    Provides import dialogue for Two/Three-Line-Elements. Aka satellite orbit data.
  Status:
    Just started
    Transaction handling needed
    SpeedButton for file selection not handled
    Progress indicator needed
    Button enable/disable
    Fixed size for dialogue
  Date:
    2021-04-15
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TdlgTLEImport = class(TForm)
    btnClose: TButton;
    Button1: TButton;
    edtSatName: TEdit;
    rgFormat: TRadioGroup;
    OpenDialog1: TOpenDialog;
    edtTLEFileName: TEdit;
    Label1: TLabel;
    Button2: TButton;
    procedure btnCloseClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure rgFormatClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgTLEImport: TdlgTLEImport;

implementation

uses
  SpaceObject;

{$R *.dfm}

procedure TdlgTLEImport.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TdlgTLEImport.Button1Click(Sender: TObject);

var
  F : textfile;
  Line1,Line2,Line3 : string;
  Sat : TSpaceObject;
  ElementSetNumber : integer;

begin
  assignfile(F,edtTLEFileName.Text);
  reset(F);
  ElementSetNumber:=1;
  while not eof(F) do begin
    //Bubble lines
    Line1:=Line2;

    if rgFormat.ItemIndex=0 then
      Line1:='0 '+edtSatName.Text;

    Line2:=Line3;

    //Read next line
    ReadLn(F,Line3);

    //Check if lineset is valid
    if (Length(Line1)<=26) and (Copy(Line2,1,1)='1') and (Copy(Line3,1,1)='2') then begin
      //It was, parse
      Sat:=TSpaceObject.Create;
      Sat.Parse2Line(Line1,Line2,Line3);
      if Sat.Valid then begin
        Sat.Store(ElementSetNumber);
        inc(ElementSetNumber);
      end;
      Sat.Free;
    end;

  end;
  CloseFile(F);
end;

procedure TdlgTLEImport.rgFormatClick(Sender: TObject);
begin
  edtSatName.Enabled:=rgFormat.ItemIndex=0;
end;

end.
