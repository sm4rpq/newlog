unit TaclogImportDlg;

{
  Purpose:
    Provides import dialogue for taclog .dat-files.
  Status:
    Mostly works.
    Contest identification could be done differently (read better)
  Date:
    2021-04-14
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Tacfile, ComCtrls;

type
  TdlgTaclogImport = class(TForm)
    Label2: TLabel;
    edtImportPath: TEdit;
    SpeedButton1: TSpeedButton;
    btnExecute: TButton;
    OKBtn: TButton;
    Label1: TLabel;
    ProgressBar1: TProgressBar;
    procedure btnExecuteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FileList : TStringList;
    Logfile : TLogfile;
    CurCall : string;
    CurOperator : string;
    CurLocId : integer;
    CurContest : integer;
    vucc_id : integer;
    Fail : boolean;
    procedure ImportQSO;
    procedure Traverse(Root : string);
  public
    { Public declarations }
  end;

var
  dlgTaclogImport: TdlgTaclogImport;

implementation

uses
  FileCtrl, DateUtils,
  MyStrList, Connection, ImportData, Misc, Settings;

{$R *.dfm}

function StrToDateTime(DS, TS : string) : TDateTime;

var
  y : integer;
  m : integer;
  d : integer;
  h : integer;
  n : integer;

begin
  y:=StrToInt(Copy(DS,1,4));
  m:=StrToInt(Copy(DS,5,2));
  d:=StrToInt(Copy(DS,7,2));
  h:=StrToInt(Copy(TS,1,2));
  n:=StrToInt(Copy(TS,3,2));
  Result:=EncodeDate(y,m,d)+EncodeTime(h,n,0,0);
end;

procedure TdlgTaclogImport.ImportQSO;

var
  qso_id : integer;
  dxcc_id : integer;
  CallList : TMyStringList;
  StartTime : TDateTime;
  ContactedStation : TContactedStation;

begin
  CallList:=TMyStringList.Create;
  with dmDataImport.qryInsertQSO do begin
    QSO_Id:=dmConnection.Get_Unique_Id;
    ParamByName('QSO_Id').AsInteger:=QSO_Id;

    CallList.SetDelimitedText(Logfile.Call);

    StartTime:=StrToDateTime(LogFile.QSODate,LogFile.QSOTime);

    dxcc_id:=dmConnection.Get_Dxcc_Id(CallList[0],StartTime);

    ContactedStation:=dmDataImport.RegisterContactedStation(Logfile.Call,'','');

    //Query for a location record
    with dmDataImport.qryLocation do begin
      paramByName('UsedLoc').AsString:=Logfile.OwnLoc+'%';
      ParamByName('UsedCall').AsString:=CurCall;
      Open;
      if not(EOF) then begin
        CurLocId:=FieldByName('UsedLocation_Id').AsInteger;
      end;
      Close;
    end;

    ParamByName('StartTime').AsDateTime:=StartTime;
    ParamByName('EndTime').IsNull:=True;
    ParamByName('Call').AsString:=Logfile.Call;
    ParamByName('Power').IsNull:=true;
    ParamByName('Frequency_TX').AsFloat:=Logfile.Freq;
    ParamByName('Frequency_RX').AsFloat:=Logfile.Freq;
    ParamByName('Rpt_S').AsString:=Logfile.rst_sent;
    ParamByName('Serial_S').AsString:=LogFile.no_sent;
    ParamByName('Rpt_R').AsString:=Logfile.rst_rcvd;
    ParamByName('Serial_R').AsString:=LogFile.no_rcvd;
    ParamByName('Locator').AsString:=Logfile.Loc;
    ParamByName('QTH').IsNull:=true;
    ParamByName('Complete').IsNull:=true;
    ParamByName('Comment').IsNull:=true;
    ParamByName('O_Pwr').IsNull:=true;
    ParamByName('O_Ant').IsNull:=true;

    ParamByName('Paperlogref').IsNull:=true;

    ParamByName('REFMode').AsString:=Logfile.Mode;

    //Check if 'A' in any report, then propmode->AUR
    if (pos('A',LogFile.rst_rcvd)>0) or (pos('A',LogFile.rst_sent)>0) then
      ParamByName('REFPropagationName').AsString:='AUR'
    else
      ParamByName('REFPropagationName').IsNull:=true;

    ParamByName('RefOpCall').AsString:=CurOperator;
    ParamByName('RefCall').AsString:=CurCall;

    //TODO: Check if any equipment info is available
    ParamByName('RefUsedEq_Id').AsInteger:=2; //Unknown

    ParamByName('RefUsedLocation_Id').AsInteger:=CurLocId;

    ParamByName('REFBaseCall').AsString:=ContactedStation.RefBaseCall;
    ParamByName('REFInstance').AsInteger:=ContactedStation.RefInstance;

    if Dxcc_id<>0 then
      ParamByName('REFDXCC_Id').AsInteger:=dxcc_id
    else
      ParamByName('REFDXCC_Id').IsNull:=true;

    if CurContest<>0 then
      ParamByName('REFContest_Id').AsInteger:=CurContest
    else
      ParamByName('REFContest_Id').IsNull:=true;

    ParamByName('REFSatellite_Id').IsNull:=true;
    ExecQuery;

    if Length(Logfile.Loc)>=4 then
      dmDataImport.RegisterAwardCredit(vucc_id,copy(Logfile.Loc,1,4),qso_id);
  end;
  CallList.Free;
end;

procedure TdlgTaclogImport.Traverse(Root : string);

var
  Seek : TSearchRec;
  Attr : integer;
  Res : integer;
  IgnoreList : TStringList;
  Index : integer;

begin
  IgnoreList:=TStringList.Create;
  if fileExists(Root+'\qlog.ignore') then
    IgnoreList.LoadFromFile(ROot+'\QLog.ignore');
  IgnoreList.CaseSensitive:=False;
  IgnoreList.Sorted:=True;
  Attr:=faAnyFile;
  Res:=FindFirst(Root+'\*.*',Attr,Seek);
  while Res=0 do begin
    if (Seek.Name<>'.') and (Seek.Name<>'..') then begin
      if not(IgnoreList.Find(Seek.Name,Index)) then begin
        if Seek.Attr=faDirectory then
          Traverse(Root+'\'+Seek.Name)
        else if Uppercase(ExtractFileExt(Seek.Name))='.DAT' then
          FileList.Add(Root+'\'+Seek.Name);
      end;
    end;
    Res:=FindNext(Seek);
  end;
  FindClose(Seek);
  IgnoreLIst.Free;
end;

procedure TdlgTaclogImport.btnExecuteClick(Sender: TObject);

var
  i : integer;

begin
  dmSettings.TLImportPath:=edtImportPath.Text;

  dmConnection.IBTransaction1.Rollback;
  dmConnection.IBTransaction1.StartTransaction;
  Vucc_id:=dmConnection.Get_Award_Id('VUCC');
  FileList.Clear;
  if DirectoryExists(edtImportPath.Text) then
    Traverse(edtImportPath.Text);

  Label1.Caption:=IntToStr(FileList.Count)+' Filer';
  ProgressBar1.Max:=FileList.Count;
  Application.ProcessMessages;

  try
    for i:=0 to FileList.Count-1 do begin
      Logfile:=TLogfile.Construct(FileList[i]);
      Curcall:=UpperCase(Logfile.Operator);
      CurOperator:=StripCall(UpperCase(Logfile.Operator));
      CurOperator[3]:='4'; //TODO: Is there a better solution?

      CurContest:=0;
      if (Pos('M�NAD',uppercase(FileList[i]))>0) then
        CurContest:=1
      else if (Pos('KVARTAL',uppercase(FileList[i]))>0) then
        CurContest:=3
      else if (Pos('JUL',uppercase(FileList[i]))>0) then
        CurContest:=2
      else
        CurContest:=0;
      while not Logfile.EOF do begin
        ImportQSO;
        Logfile.Next;
      end;
      Logfile.Free;
      ProgressBar1.StepBy(1);
    end;
  finally
    if Fail then
      dmConnection.IBTransaction1.Rollback
    else
      dmConnection.IBTransaction1.Commit;
  end;
end;

procedure TdlgTaclogImport.FormCreate(Sender: TObject);
begin
  FileList:=TStringList.Create;
end;

procedure TdlgTaclogImport.SpeedButton1Click(Sender: TObject);

var
  InputRoot : string;

begin
  InputRoot:='';
  if SelectDirectory(InputRoot,[],0) then
    edtImportPath.Text:=InputRoot;
end;

procedure TdlgTaclogImport.FormShow(Sender: TObject);
begin
  ProgressBar1.Max:=100;
  ProgressBar1.Position:=0;

  edtImportPath.Text:=dmSettings.TLImportPath;
end;

end.
