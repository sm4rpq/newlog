unit main;

{
  Purpose:
    Main Dialogue for logging program. Provides access to logbook entries
    and sub-functions like import, export and statistics.
  Status:
    Work in progress

    TODOs for the near future:
    - Fix transaction processing!
    - R6
    - Statistics on more awards
    - ADIF export (needed for electronic QSLing aka eqsl/lotw/xxx)
    - Need a checkbox with readonly for SK and Complete.
    - Replace some datatypes in TQSORecord

    TODOs further into the future
    - QSO Input function...
    - QSL-registering functions
    - Select current location for constellation
  Date:
    2023-01-15
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, IBDatabase, IBCustomDataSet, IBTable,
  ExtCtrls, StdCtrls, IBSQL, IBQuery, Menus, Connection, Mask,
  ComCtrls, IniFiles, ovctcedt, ovctcmmn, ovctcell, ovctcstr, ovctchdr,
  ovcbase, ovctable, ovcfiler, ovcstore, ovcstate;


const
  BufferSize = 300;

type
  TQSORecord = record
    QSOId : integer; // Not to be shown, just for re-searching the same QSO
    DateTimeT : TDateTime;
    DateTime : string[20];
    Frequency : string[10];
    Mode : string[32];
    WorkedCall : string[20];
    SentRST : string[5];
    SentSer : string[5];
    RcvdRST : string[5];
    RcvdSer : String[5];
    Locator : string[10];
    Complete : string[1];
    DXCC : string[32];
    Name : string[32];
    Nick : string[32];
    QTH : string[40];
    SK : boolean;
    Comment : string[120];
    OwnCall : string[20];
    OwnLoc : string[10];
    PaperLogRef : integer;

    SWL : string[1];
    Propagation : string[32];
    HeightAMSL : double;
    SatName : string[32];
  end;
  TBuffer = record
    RangeLow : integer;
    RangeHigh : integer;
    Values : array[0..BufferSize-1] of TQSORecord;
  end;
  TfrmMain = class(TForm)
    Panel2: TPanel;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    itmExit: TMenuItem;
    N1: TMenuItem;
    itmPrintSetup: TMenuItem;
    itmPrint: TMenuItem;
    N2: TMenuItem;
    itmOpenLogbook: TMenuItem;
    itmNewLogbook: TMenuItem;
    itmImport: TMenuItem;
    itmImportCSV: TMenuItem;
    itmImportTaclog: TMenuItem;
    itmExport: TMenuItem;
    itmExportSquaresQGIS: TMenuItem;
    itmExportCompleteXML: TMenuItem;
    itmExportADIF: TMenuItem;
    N3: TMenuItem;
    Statistics1: TMenuItem;
    itmStatSquare: TMenuItem;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    StatusBar1: TStatusBar;
    btnClearFilter: TButton;
    btnFilter: TButton;
    itmImportXML: TMenuItem;
    itmStatDXCC: TMenuItem;
    itmStatOther: TMenuItem;
    QSL1: TMenuItem;
    itmRegisterQSLRcvd: TMenuItem;
    itmPrintQSLLabels: TMenuItem;
    itmImportTLE: TMenuItem;
    Visa1: TMenuItem;
    itmShowConstellation: TMenuItem;
    itmSendEQSL: TMenuItem;
    itmRegisterEQSL: TMenuItem;
    itmShowMap: TMenuItem;
    N4: TMenuItem;
    OvcTable1: TOvcTable;
    OvcTCSLeft: TOvcTCString;
    OvcTCSRight: TOvcTCString;
    ComboBox1: TComboBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    edtWeekday: TEdit;
    OvcFormState1: TOvcFormState;
    OvcRegistryStore1: TOvcRegistryStore;
    cbSilentKey: TCheckBox;
    edtFrequency: TEdit;
    edtMode: TEdit;
    edtWorkedCall: TEdit;
    edtLocatorQ: TEdit;
    edtStarttime: TEdit;
    edtRptS: TEdit;
    edtRptR: TEdit;
    edtDXCC: TEdit;
    edtName: TEdit;
    edtNick: TEdit;
    edtQTH: TEdit;
    edtSerS: TEdit;
    edtSerR: TEdit;
    edtComment: TEdit;
    edtRefCall: TEdit;
    edtLocatorUL: TEdit;
    edtPaperlogRef: TEdit;
    cbComplete: TCheckBox;
    itmQslRules: TMenuItem;
    procedure itmExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure itmImportCSVClick(Sender: TObject);
    procedure itmExportSquaresQGISClick(Sender: TObject);
    procedure itmExportCompleteXMLClick(Sender: TObject);
    procedure itmExportADIFClick(Sender: TObject);
    procedure itmImportTaclogClick(Sender: TObject);
    procedure itmImportXMLClick(Sender: TObject);
    procedure itmStatSquareClick(Sender: TObject);
    procedure itmStatDXCCClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnClearFilterClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure itmStatOtherClick(Sender: TObject);
    procedure itmImportTLEClick(Sender: TObject);
    procedure itmShowConstellationClick(Sender: TObject);
    procedure OvcTable1GetCellData(Sender: TObject; RowNum,
      ColNum: Integer; var Data: Pointer; Purpose: TOvcCellDataPurpose);
    procedure OvcTable1ActiveCellChanged(Sender: TObject; RowNum,
      ColNum: Integer);
    procedure ComboBox1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OvcTable1GetCellAttributes(Sender: TObject; RowNum,
      ColNum: Integer; var CellAttr: TOvcCellAttributes);
    procedure itmQslRulesClick(Sender: TObject);
  private
    { Private declarations }
    IniFile : TIniFile;
    Buffers : array[0..2] of TBuffer;
    CacheRow : integer;
    CacheIndex : integer;
    SelectedRecord : integer;

    Hits : integer;
    Misses : integer;
    ExtraSelect : string;
    procedure SetupBuffers;
    function GetBuffer(RowNum : integer) : integer;
    procedure LoadBuffer(Buffer : integer; RowNum : integer);
    
    procedure ConnectToDb;
    function FuncEnabled(Func : string) : boolean;
    procedure SetQuery(var aQuery : TIBQuery; aCount : boolean; aLimitLow : integer; aLimitHigh : integer);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses CSVImportDlg, ExportSquareListDlg, XmlExport, AdifExport,
  TaclogImportDlg, GeoUnit, Vector, XMLImportDlg, SquareStatDlg,
  DXCCStatDlg, MainFilterDlg, OtherStatDlg, TLEImportDlg, ConstellationDlg,
  Common, QSLRules, QSLRulesDlg;

{$R *.dfm}

procedure TfrmMain.itmExitClick(Sender: TObject);
begin
  Close;
end;

function TfrmMain.FuncEnabled(Func : String) : boolean;
begin
  Result:=iniFile.ReadString('Functions',Func,'T')<>'F';
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  IniFile:=TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  try
    //File menu
    itmNewLogbook.Enabled:=FuncEnabled('EnableNew');
    itmOpenLogbook.Enabled:=FuncEnabled('EnableOpen');
    itmImportCSV.Enabled:=FuncEnabled('EnableImportCSV');
    itmImportTaclog.Enabled:=FuncEnabled('EnableImportTaclog');
    itmImportXML.Enabled:=FuncEnabled('EnableImportXML');
    itmImportTLE.Enabled:=FuncEnabled('EnableImportTLE');
    itmExportSquaresQGIS.Enabled:=FuncEnabled('EnableExportQGIS');
    itmExportCompleteXML.Enabled:=FuncEnabled('EnableExportXML');
    itmExportADIF.Enabled:=FuncEnabled('EnableExportADIF');
    itmPrint.Enabled:=FuncEnabled('EnablePrint');
    itmPrintSetup.Enabled:=FuncEnabled('EnablePrintSetup');

    //Statistics menu
    itmStatSquare.Enabled:=FuncEnabled('EnableStatSqr');
    itmStatDXCC.Enabled:=FuncEnabled('EnableStatDXCC');
    itmStatOther.Enabled:=FuncEnabled('EnableStatOther');

    //QSL Menu
    itmQslRules.Enabled:=FuncEnabled('EnableQSLRules');
    itmPrintQSLLabels.Enabled:=FuncEnabled('EnablePrintPQSLLabels');
    itmRegisterQSLRcvd.Enabled:=FuncEnabled('EnableRegPQSL');
    itmSendEQSL.Enabled:=FuncEnabled('EnableSendEQSL');
    itmRegisterEQSL.Enabled:=FuncEnabled('EnableRegEQSL');

    //Show menu
    itmShowConstellation.Enabled:=FuncEnabled('EnableShowConstellation');
    itmShowMap.Enabled:=FuncEnabled('EnableShowMap');

  finally
    iniFile.Free;
  end;

  ConnectToDb;
  SetupBuffers;
  OvcTable1.OnActiveCellChanged(TObject(0),OvcTable1.ActiveRow,OvcTable1.ActiveCol);
end;

procedure TfrmMain.SetQuery(var aQuery: TIBQuery; aCount: boolean; aLimitLow,
  aLimitHigh: integer);
begin
  with aQuery.SQL do begin
    Clear;
    Add('SELECT');
    if aCount then
      Add('count(*)')
    else begin
      Add(' q.qso_id,');
      Add(' q.starttime,q.frequency_tx,q.refmode,q.call,q.rpt_s,q.serial_s,q.rpt_r,q.serial_r,q.locator as locatorq,q.complete,');
      Add(' c.name,c.nick,c.issilentkey,q.qth,');
      Add(' q.comment,q.refdxcc_id,coalesce(de.svnamn,de.name),');
      Add(' q.refcall,q.paperlogref,');
      Add(' q.sat_mode,q.isswl,');
      Add(' q.refpropagationname,q.endtime,ul.locator as locatorul,ul.heightamsl as heightul,sat.name as satname');
    end;
    Add('FROM qso q');
    Add('INNER JOIN contactedstation c on c.basecall=q.refbasecall and c.instance=q.refinstance');
    Add('INNER JOIN UsedLocation ul on ul.usedlocation_id=q.refusedlocation_id');
    Add('LEFT OUTER JOIN dxcc_entity de on de.dxcc_id=q.refdxcc_id');
    Add('LEFT OUTER JOIN satellite sat on sat.satellite_id=q.refsatellite_id');

    if dlgMainFilter.FilterSet or (ExtraSelect<>'') then
      Add('where');
    if dlgMainFilter.CallsignFilter<>'' then
      Add(dlgMainFilter.CallsignFilter);
    if dlgMainFilter.LocatorFilter<>'' then begin
      if dlgMainFilter.CallsignFilter<>'' then
        Add('and');
      Add(dlgMainFilter.LocatorFilter);
    end;
      if ExtraSelect<>'' then begin
        if (dlgMainFilter.CallsignFilter<>'') or (dlgMainFilter.LocatorFilter<>'') then
          Add('and');
        Add(ExtraSelect);
      end;

    if not(aCount) then begin
      Add('ORDER BY');
      Add('  q.starttime');
    end;

    if (not(aCount) and (aLimitLow>0) and (aLimitHigh>0)) then begin
      Add('ROWS '+IntToStr(aLimitLow)+' TO '+IntToStr(aLimitHigh));
    end;
  end;
end;

procedure TfrmMain.ConnectToDb;
begin
end;

procedure TfrmMain.itmImportCSVClick(Sender: TObject);
begin
  dlgCSVImport.ShowModal;
end;

procedure TfrmMain.itmExportSquaresQGISClick(Sender: TObject);
begin
  dlgExportSquareList.ShowModal;
end;

procedure TfrmMain.itmExportCompleteXMLClick(Sender: TObject);
begin
  dlgExportXML.ShowModal;
end;

procedure TfrmMain.itmExportADIFClick(Sender: TObject);
begin
  dlgExportADIF.ShowModal;
end;

procedure TfrmMain.itmImportTaclogClick(Sender: TObject);
begin
  dlgTaclogImport.ShowModal;
end;

procedure TfrmMain.itmImportXMLClick(Sender: TObject);
begin
  dlgXMLImport.ShowModal;
end;

procedure TfrmMain.itmStatSquareClick(Sender: TObject);
begin
  dlgStatSquare.ShowModal;
end;

procedure TfrmMain.itmStatDXCCClick(Sender: TObject);
begin
  dlgStatDXCC.ShowModal;
end;

procedure TfrmMain.btnFilterClick(Sender: TObject);
begin
  dlgMainFilter.ShowModal;
  btnClearFilter.Enabled:=dlgMainFilter.FilterSet;
//  ConnectToDb;
  OvcTable1.ActiveRow:=1;
  SetupBuffers;
  OvcTable1.Repaint;
  OvcTable1ActiveCellChanged(Sender,OvcTable1.ActiveRow,OvcTable1.ActiveCol);
end;

procedure TfrmMain.btnClearFilterClick(Sender: TObject);
begin
  dlgMainFilter.Clear;
  btnClearFilter.Enabled:=dlgMainFilter.FilterSet;
//  ConnectToDb;
  OvcTable1.ActiveRow:=1;
  SetupBuffers;
  OvcTable1.Repaint;
  OvcTable1ActiveCellChanged(Sender,OvcTable1.ActiveRow,OvcTable1.ActiveCol);
end;

procedure TfrmMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then btnFilterClick(Sender);
end;

procedure TfrmMain.itmStatOtherClick(Sender: TObject);
begin
  dlgStatOther.ShowModal;
end;

procedure TfrmMain.itmImportTLEClick(Sender: TObject);
begin
  dlgTLEImport.ShowModal;
end;

procedure TfrmMain.itmShowConstellationClick(Sender: TObject);
begin
  dlgConstellation.Show;
end;

const
  ColHeads : array[0..12] of string[20] =
    ('Datum/Tid','Anropssignal','Frekvens','S�nd RST','Mott. RST','Lokator',
     'Namn','QTH','Komplett','SWL','Trafiks�tt','V�gutbredn.','Egen Signal');

procedure TfrmMain.OvcTable1GetCellData(Sender: TObject; RowNum,
  ColNum: Integer; var Data: Pointer; Purpose: TOvcCellDataPurpose);

var
  PBuffer : ^TBuffer;

begin
  if RowNum=0 then
    Data:=@ColHeads[ColNum]
  else begin
    dec(RowNum); // Fit into 0-based indexes (0 in the grid is the header)
    if (RowNum<>CacheRow) then begin
      CacheIndex:=GetBuffer(RowNum);
      CacheRow:=RowNum;
      inc(Misses);
    end else
      Inc(Hits);
    PBuffer:=@Buffers[CacheIndex];
    with PBuffer^.Values[RowNum-PBuffer^.RangeLow] do begin
      case ColNum of
      0 : Data:=@DateTime;
      1 : Data:=@WorkedCall;
      2 : Data:=@Frequency;
      3 : Data:=@SentRST;
      4 : Data:=@RcvdRST;
      5 : Data:=@Locator;
      6 : Data:=@Name;
      7 : Data:=@QTH;
      8 : Data:=@Complete;
      9 : Data:=@SWL;
      10: Data:=@Mode;
      11: Data:=@Propagation;
      12: Data:=@OwnCall;
      end;
    end;
  end;
end;

function TfrmMain.GetBuffer(RowNum: integer): integer;

var
  i : integer;
  Dist : integer;
  MaxDist : integer;
  LoadBuf : integer;

begin
  for i:=0 to 2 do begin
    if (Buffers[i].RangeLow<=RowNum) and (RowNum<=Buffers[i].RangeHigh) then begin
      Result:=i;
      exit;
    end;
  end;

  MaxDist:=0;
  LoadBuf:=0;
  for i:=0 to 2 do begin
    with Buffers[i] do
      Dist:=abs((RangeLow+RangeHigh) div 2 - RowNum);
    if Dist>MaxDist then begin
      MaxDist:=Dist;
      LoadBuf:=i;
    end;
  end;
  if (LoadBuf>=0) then
    LoadBuffer(LoadBuf,RowNum);
  Result:=LoadBuf;
end;

procedure TfrmMain.LoadBuffer(Buffer, RowNum: integer);

var
  FirstRow : integer;
  LastRow : integer;
  i : integer;

begin
  FirstRow:=(RowNum div BufferSize)*BufferSize;
  LastRow:=FirstRow+BufferSize-1;
  Buffers[Buffer].RangeLow:=FirstRow;
  Buffers[Buffer].RangeHigh:=LastRow;

  SetQuery(dmConnection.qryQSOList,false,FirstRow+1,LastRow+1);
  dmConnection.qryQSOList.Open;

  for i:=0 to BufferSize-1 do begin
    with Buffers[Buffer].Values[i] do begin
      if not(dmConnection.qryQSOList.EOF) then begin
        QSOId:=dmConnection.qryQSOList.FieldByName('QSO_Id').AsInteger;
        DateTimeT:=dmConnection.qryQSOList.FieldByName('Starttime').AsDateTime;
        DateTime:=FormatDateTime('yyyy-mm-dd hh:nn:ss',DateTimeT);
        Frequency:=dmConnection.qryQSOList.FieldByName('Frequency_tx').AsString;
        Mode:=dmConnection.qryQSOList.FieldByName('refMode').AsString;
        WorkedCall:=dmConnection.qryQSOList.FieldByName('Call').AsString;
        SentRST:=dmConnection.qryQSOList.FieldByName('RPT_S').AsString;
        SentSer:=dmConnection.qryQSOList.FieldByName('SERIAL_S').AsString;
        RcvdRST:=dmConnection.qryQSOList.FieldByName('RPT_R').AsString;
        RcvdSer:=dmConnection.qryQSOList.FieldByName('SERIAL_R').AsString;
        Locator:=dmConnection.qryQSOList.FieldByName('LocatorQ').AsString;
        if (dmConnection.qryQSOList.FieldByName('Complete').AsString<>'F') then
          Complete:='X'
        else
          Complete:='';
        DXCC:=dmConnection.qryQSOList.FieldByName('COALESCE').AsString;
        Name:=dmConnection.qryQSOList.FieldByName('Name').AsString;
        Nick:=dmConnection.qryQSOList.FieldByName('Nick').AsString;
        QTH:=dmConnection.qryQSOList.FieldByName('QTH').AsString;
        SK:=False;
        if not(dmConnection.qryQSOList.FieldByName('ISSILENTKEY').IsNull) then
          SK:=dmConnection.qryQSOList.FieldByName('ISSILENTKEY').AsString='T';


        Comment:=dmConnection.qryQSOList.FieldByName('comment').AsString;
        OwnCall:=dmConnection.qryQSOList.FieldByName('refcall').AsString;
        OwnLoc:=dmConnection.qryQSOList.FieldByName('locatorul').AsString;
        PaperLogRef:=dmConnection.qryQSOList.FieldByName('paperlogref').AsInteger;

        if (dmConnection.qryQSOList.FieldByName('isswl').AsString='T') then
          SWL:='X'
        else
          SWL:='';
        Propagation:=dmConnection.qryQSOList.FieldByName('refpropagationname').AsString;;;
        HeightAMSL:=dmConnection.qryQSOList.FieldByName('heightul').AsFloat;
        SatName:=dmConnection.qryQSOList.FieldByName('SATNAME').AsString;

        dmConnection.qryQSOList.Next;
      end else begin
        QSOId:=0;
        DateTime:='';
        Frequency:='';
        Mode:='';
        WorkedCall:='';
        SentRST:='';
        SentSer:='';
        RcvdRST:='';
        RcvdSer:='';
        Locator:='';
        Complete:='';
        DXCC:='';
        Name:='';
        Nick:='';
        QTH:='';
        SK:=false;
        Comment:='';
        OwnCall:='';
        OwnLoc:='';
        PaperLogRef:=0;

        SWL:='';
        Propagation:='';
        HeightAMSL:=0.0;
        SatName:='';
      end;
    end;
  end;
  dmConnection.qryQSOList.Close;
end;

procedure TfrmMain.SetupBuffers;

var
  QSOCount : integer;

begin
  SetQuery(dmConnection.qryQSOList,true,0,0);
  with dmConnection.qryQSOList do begin
    Open;
    QSOCount:=Fields[0].AsInteger;
    Close;
  end;
  StatusBar1.Panels[0].Text:=IntToStr(QSOCount)+' poster';
  OvcTable1.RowLimit:=QSOCount+1;

  LoadBuffer(0,0);
  LoadBuffer(1,BufferSize);
  LoadBuffer(2,2*BufferSize);

  CacheRow:=-1;
  CacheIndex:=-1;
end;

procedure TfrmMain.OvcTable1ActiveCellChanged(Sender: TObject; RowNum,
  ColNum: Integer);

var
  Buffer : integer;
  PBuffer : ^TBuffer;
  DOW : Integer;

begin
  if (RowNum=0) then exit;
  Buffer:=GetBuffer(RowNum-1);
  PBuffer:=@Buffers[Buffer];

  SelectedRecord:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].QSOId;
  ConnectToDb;

  edtStarttime.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].DateTime;
  DOW:=DayOfWeek(PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].DateTimeT);
  edtWeekday.Text:=Copy('S�M�TiOnToFrL�',(DOW-1)*2+1,2);
  edtFrequency.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Frequency;
  edtMode.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Mode;
  edtWorkedCall.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].WorkedCall;
  edtRptS.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].SentRST;
  edtSerS.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].SentSer;
  edtRptR.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].RcvdRST;
  edtSerR.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].RcvdSer;
  edtLocatorQ.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Locator;
  cbComplete.Checked:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Complete='X';
  edtDXCC.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].DXCC;
  edtName.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Name;
  edtNick.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Nick;
  edtQTH.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].QTH;
  cbSilentKey.Checked:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].SK;
  edtComment.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Comment;
  edtRefCall.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].OwnCall;
  edtLocatorUL.Text:=PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].OwnLoc;
  if PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].PaperLogRef>0 then
    edtPaperlogRef.Text:=IntToStr(PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].PaperLogRef)
  else
    edtPaperlogRef.Text:='';

  dlgConstellation.setQSOData(
    PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].DateTimeT,
    PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].SatName,
    PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].OwnLoc,
    PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].HeightAMSL,
    PBuffer^.Values[RowNum-PBuffer^.RangeLow-1].Locator
  );
end;

procedure TfrmMain.ComboBox1Change(Sender: TObject);
begin
(*
Alla
50MHz
144MHz
432MHz
1296MHz
SM4RPP
SM4RPQ
Hemma
Bl�sj�n
*)
  case ComboBox1.ItemIndex of
    0 : ExtraSelect:='';
    1 : ExtraSelect:='q.Frequency_TX>=50 and q.Frequency_Tx<=52';
    2 : ExtraSelect:='q.Frequency_TX>=144 and q.Frequency_Tx<=146';
    3 : ExtraSelect:='q.Frequency_TX>=432 and q.Frequency_Tx<=438';
    4 : ExtraSelect:='q.Frequency_TX>=1240 and q.Frequency_Tx<=1300';
    5 : ExtraSelect:='q.refopcall=''SM4RPP''';
    6 : ExtraSelect:='q.refopcall=''SM4RPQ''';
    7 : ExtraSelect:='ul.locator like ''JO%''';
    8 : ExtraSelect:='ul.locator like ''JP%''';
    9 : ExtraSelect:='q.refpropagationname=''AUR''';
    10: ExtraSelect:='q.refpropagationname=''SAT''';
    11: ExtraSelect:='q.refpropagationname=''MS''';
  end;
  OvcTable1.ActiveRow:=1;
  SetupBuffers;
  OvcTable1.Repaint;
  OvcTable1ActiveCellChanged(self,1,0);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  OvcRegistryStore1.KeyName:=ConfigKey;
end;

procedure TfrmMain.OvcTable1GetCellAttributes(Sender: TObject; RowNum,
  ColNum: Integer; var CellAttr: TOvcCellAttributes);

var
  PBuffer : ^TBuffer;

begin
  if RowNum=0 then
    exit
  else begin
    dec(RowNum); // Fit into 0-based indexes (0 in the grid is the header)
    if (RowNum<>CacheRow) then begin
      CacheIndex:=GetBuffer(RowNum);
      CacheRow:=RowNum;
      inc(Misses);
    end else
      Inc(Hits);
    PBuffer:=@Buffers[CacheIndex];
    with PBuffer^.Values[RowNum-PBuffer^.RangeLow] do begin
//      if (ColNum=1) then
        if SK then
          CellAttr.caFontColor:=clRed
        else if Complete='' then
          CellAttr.caFontColor:=clBlue;
    end;
  end;
end;

procedure TfrmMain.itmQslRulesClick(Sender: TObject);
begin
  dlgQSLRules.ShowModal;
end;

end.
