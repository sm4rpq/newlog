unit CSVImportDlg;

{
  Purpose:
    Provides the import dialogue for CSV files. Those are our 50MHz logs
    currently kept on paper and in excel. Save as csv in excel to create
    importable files.
  Status:
    Lacks handling of SWL-reports
  Date:
    2021-04-14
}

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls, 
  Buttons, ExtCtrls, Dialogs, IBSQL, DB, IBCustomDataSet, IBTable, IBQuery;

type
  TdlgCSVImport = class(TForm)
    OKBtn: TButton;
    cboOwnCall: TComboBox;
    edtImportPath: TEdit;
    SpeedButton1: TSpeedButton;
    OpenDialog1: TOpenDialog;
    btnExecute: TButton;
    Label1: TLabel;
    Label2: TLabel;
    OpenDialog2: TOpenDialog;
    procedure btnExecuteClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure edtImportPathChange(Sender: TObject);
  private
    { Private declarations }
    FStartPath : string;
  public
    { Public declarations }
  end;

var
  dlgCSVImport: TdlgCSVImport;

implementation

uses
  ImportData, CSVFile, Connection, MyStrList, Settings;

{$R *.dfm}

procedure TdlgCSVImport.btnExecuteClick(Sender: TObject);

  procedure SetOrNull(Param : string; Value : String);
  begin
    if Value<>'' then
      dmDataImport.qryInsertQSO.ParamByName(Param).AsString:=Value
    else
      dmDataImport.qryInsertQSO.ParamByName(Param).IsNull:=True;
  end;

  procedure NameSplit(FullName : string; var Name : string; var Nick: string);

  var
    ParPos : integer;

  begin
    ParPos:=Pos('(',FullName);
    if ParPos>0 then begin
      Nick:=Copy(FullName,1,ParPos-1);
      Name:=Copy(FullName,ParPos+1,Length(FullName)-ParPos-1);
    end else begin
      Name:=FullName;
      Nick:='';
    end;
  end;

type
  TOperatingCondition = record
    RefCall : string;
    RefOpCall : string;
    RefUsedEq_Id : integer;
    RefUsedLocation_Id : integer;
  end;

var
  OwnId    : integer;
  CSVFile : TCSVFile;
  Fail : boolean;
  qso_id : integer;
  dxcc_id : integer;
  CallList : TMyStringList;
  vucc_id : integer;
  OperatingCondition : TOperatingCondition;
  i : integer;
  Ltr : string;
  ContactedStation : TContactedStation;

begin
  dmSettings.CSVImportPath:=ExtractFilePath(edtImportPath.Text);

  Fail:=False;
  CallList:=TMyStringList.Create;
  CallList.Delimiter:='/';
  Vucc_id:=dmConnection.Get_Award_Id('VUCC');

  if (cboOwnCall.ItemIndex>-1) and FileExists(edtImportPath.Text) then begin
    OwnId:=integer(cboOwnCall.Items.Objects[cboOwnCall.ItemIndex]);
    with dmDataImport.qryCondition do begin
      ParamByName('Opc_Id').AsInteger:=OwnId;
      Open;
      if not(EOF) then begin
        OperatingCondition.RefCall:=FieldByName('REFCall').AsString;
        OperatingCondition.RefOpCall:=FieldByName('REFOpCall').AsString;
        OperatingCondition.RefUsedLocation_Id:=FieldByName('REFUsedLocation_Id').AsInteger;
        OperatingCondition.RefUsedEq_Id:=FieldByName('REFUsedEq_Id').AsInteger;
      end;
      Close;
    end;

    CSVFile:=TCSVFile.Create;

    try
      CSVFile.Filename:=edtImportPath.Text;
      CSVFile.Open;

      while not(CSVFile.EOF) do begin
        if not(CSVFile.ReadFail) then with dmDataImport.qryInsertQSO do begin
          QSO_Id:=dmConnection.Get_Unique_Id;
          ParamByName('QSO_Id').AsInteger:=QSO_Id;

          CallList.SetDelimitedText(CSVFile.Callsign);
          dxcc_id:=dmConnection.Get_Dxcc_Id(CallList[0],CSVFile.StartTime);

          ContactedStation:=dmDataImport.RegisterContactedStation(CSVFIle.Callsign,CSVFile.Name,CSVFile.Manager);

          ParamByName('StartTime').AsDateTime:=CSVFile.StartTime;
          if CSVFile.EndTime<>0.0 then begin
            ParamByName('EndTime').AsDateTime:=CSVFile.EndTime;
          end else
            ParamByName('EndTime').IsNull:=True;
          ParamByName('Call').AsString:=CSVFile.Callsign;
          ParamByName('Power').AsFloat:=CSVFile.Power;
          ParamByName('Frequency_TX').AsFloat:=50.0;
          ParamByName('Frequency_RX').AsFloat:=50.0;
          SetOrNull('Rpt_S',CSVFile.RSTs);
          SetOrNull('Rpt_R',CSVFile.RSTr);
          SetOrNull('Locator',CSVFile.Locator);
          SetOrNull('QTH',CSVFile.QTH); //QTH
          if (CSVFile.Complete) then
            SetOrNull('Complete','') // Complete?
          else
            SetOrNull('Complete','F'); // Complete?
          SetOrNull('Comment',CSVFile.Notes);
          if (CSVFile.O_Pwr>1E-7) then
            ParamByName('O_Pwr').AsFloat:=CSVFile.O_Pwr
          else
            ParamByName('O_Pwr').IsNull:=true;
          SetOrNull('O_Ant',CSVFile.O_Ant); //Ant

          ParamByName('Paperlogref').AsInteger:=CSVFile.RecNo;

          ParamByName('REFMode').AsString:=CSVFile.Mode;
          SetOrNull('REFPropagationName',CSVFile.Propagation);

          ParamByName('REFCall').AsString:=OperatingCondition.RefCall;
          ParamByName('REFOpCall').AsString:=OperatingCondition.RefOpCall;
          ParamByName('REFUsedEq_Id').AsInteger:=OperatingCondition.RefUsedEq_Id;
          ParamByName('REFUsedLocation_Id').AsInteger:=OperatingCondition.RefUsedLocation_Id;

          ParamByName('REFBaseCall').AsString:=ContactedStation.RefBaseCall;
          ParamByName('REFInstance').AsInteger:=ContactedStation.RefInstance;

          if Dxcc_id<>0 then
            ParamByName('REFDXCC_Id').AsInteger:=dxcc_id
          else
            ParamByName('REFDXCC_Id').IsNull:=true;

          ParamByName('REFContest_Id').IsNull:=true;
          ParamByName('REFSatellite_Id').IsNull:=true;

          ExecQuery;

          if CSVFIle.QSLR<>'' then
            for i:=1 to Length(CSVFile.QSLR) do begin
              Ltr:=Copy(CSVFile.QSLR,i,1);
              if Ltr='X' then Ltr:='B';
              if Ltr<>'O' then //SWL - Ignore for now
                dmDataImport.RegisterReceivedQSL(QSO_Id,Ltr);
            end;
          if CSVFIle.QSLS<>'' then
            for i:=1 to Length(CSVFile.QSLS) do begin
              Ltr:=Copy(CSVFile.QSLS,i,1);
              if Ltr='X' then Ltr:='B';
              if Ltr<>'O' then //SWL - Ignore for now
                dmDataImport.RegisterSentQSL(QSO_Id,Ltr);
            end;

          if CSVFile.Square<>'' then
            dmDataImport.RegisterAwardCredit(vucc_id,CSVFile.Square,qso_id); //include rconf_id

        end; //if not readfail
        CSVFile.Next;
      end;
    finally
      CSVFile.Free;
      if Fail then
        dmConnection.IBTransaction1.Rollback
      else
        dmConnection.IBTransaction1.Commit;
    end;
  end;
end;

procedure TdlgCSVImport.FormShow(Sender: TObject);
begin
  dmDataImport.qryConditions.Open;
  cboOwnCall.Items.Clear;
  with dmDataImport.qryConditions do begin
    First;
    while not Eof do begin
      cboOwnCall.Items.AddObject(
        FieldByName('NAME').AsString,
        TObject(FieldByName('OPERATINGCONDITION_ID').AsInteger));
      Next;
    end;
  end;
  cboOwnCall.ItemIndex:=-1;
  dmDataImport.qryConditions.Close;

  FStartPath:=dmSettings.CSVImportPath;
end;

procedure TdlgCSVImport.SpeedButton1Click(Sender: TObject);
begin
  if (FStartPath<>'') then
    OpenDialog1.InitialDir:=FStartPath;
  if (OpenDialog1.Execute) then
    edtImportPath.Text:=OpenDialog1.FileName;
end;

procedure TdlgCSVImport.edtImportPathChange(Sender: TObject);
begin
  btnExecute.Enabled:=FileExists(edtImportPath.Text);
end;

end.
