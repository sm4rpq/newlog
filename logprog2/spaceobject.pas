unit SpaceObject;

{
  Purpose:
    Provide functions for interacting with kepler elements in the database and
    do some mathematical operations on them for calculating satellites
    orbital position.
    Math based on plan13 in qrptracker on github. That is in turn based on
    G3RUHs old plan13 basic program.
  Status:
    Need to do something about the fixed GHAA problem.
    Subsat point calculations not correct (spheroid vs ellipsoid)
    Continue work on refactoring variable names.
  Date:
    2021-05-08
}

interface

uses
  SysUtils, Vector;

type
  TSpaceObject = class(TObject)
  private
    (* keplerians *)

    (* Row 1 *)
    m_tle_Name                  : string;

    (* Row 2 *)
    m_tle_Satnum1               : integer;
    m_tle_Classification        : char;
    m_tle_InternationalDesignator : string;
    m_tle_EpochYear             : integer;
    m_tle_EpochDay              : double;
    m_tle_MMPrim                : double;
    m_tle_MMBis                 : double;
    m_tle_BStarDrag             : double;
    m_tle_Elementset            : integer;

    (* Row 3 *)
    m_tle_Satnum2               : integer;
    m_tle_Inclination           : double;
    m_tle_Raan                  : double;
    m_tle_Eccentricity          : double;
    m_tle_ArgOfPerigee          : double;
    m_tle_MeanAnomaly           : double;
    m_tle_MeanMotion            : double;
    m_tle_RevnoAtEpoch          : integer;


    (* These are a property of the spacecraft itself, I think *)
    ALON                : double;
    ALAT                : double;

    (*Calculated things*)
    m_ElementSetId      : string;
    m_EpochDay          : double;
    m_EpochTime         : double;

    m_raan              : double;
    m_Inclination       : double;
    m_MeanAnomaly       : double;
    m_MeanMotion        : double;
    m_MeanMotionPrim    : double;
    m_ArgoOfPerigee     : double;

    N0                  : double;       (* Mean motion [s-1] *)
    A0                  : double;       (* Semi-Major axis [m] *)
    B0                  : double;       (* Semi-Minor axis [m] *)
    SI                  : extended;     (* Sine of inclination *)
    CI                  : extended;     (* Cosine of inclination *)
    QD                  : double;       (* Node precession rate [day-1] *)
    WD                  : double;       (* Perigee Precession rate, [day-1]  *)
    DC                  : double;       (* Drag coefficient *)
    GHAE                : double;
    MRSE                : double;
    MASE                : double;

    DN                  : double;       (* Daynumber for calculations, whole day *)
    TN                  : double;       (* Daynumber for calculations, fractional day *)

    A                   : T3DVector;

    m_AntVec            : T3DVector;

    FSLon               : double;
    FSLat               : double;
    FHeight             : double;

    FValid              : boolean;
    FSatNumber          : integer;
    FLoadTime           : TDateTime;

    procedure SetSatNumber(const Value: integer);
    function GetAge: Double;

  public
    RevNumber           : integer;
    S                   : T3DVector;
    V                   : T3DVector;

    procedure Parse2Line(line1, line2, line3 : string);
    procedure Prepare;
    procedure Store(ElementSetNumber : integer);
    procedure Load(SatNum : Integer; When : TDateTime); overload;
    procedure Load(When : TDateTime); overload;

    procedure setTime(T : TDateTime);
    procedure SatVec;

    property Valid: boolean read FValid;

    property SatNumber : integer read FSatNumber write SetSatNumber;
    property Age : double read GetAge;

    property SLon : double read FSLon;
    property SLat : double read FSLat;
    property Height : double read FHeight;
  end;
  EElementParseError = class(Exception);

implementation

uses
  Math, Geoconstants, SatData, IBQuery, DateUtils;

procedure TSpaceObject.Parse2Line(line1, line2, line3 : string);

var
  SavePoint : char;
  Mant : string;
  Exp  : string;

begin
  SavePoint := DecimalSeparator;
  try
    DecimalSeparator:='.';

    m_tle_Name := copy(line1,3,24);

    m_tle_Satnum1 := strtoint(copy(line2, 3, 5));
    m_tle_Satnum2 := strtoint(copy(line3, 3, 5));
    if (m_tle_Satnum1<>m_tle_Satnum2) then
      raise EElementParseError.create('Different satelite numbers for line1 and line2');

    m_tle_Classification := copy(line2, 9, 1)[1];
    m_tle_InternationalDesignator := copy(line2, 10, 8);
    m_tle_Elementset := strtoint(copy(line2,65,4));

    m_ElementSetId := Copy(line2,19,14);

    if copy(line2,19,2)<'57' then begin
      m_tle_EpochYear := StrToInt(copy(Line2,19,2))+2000;
      m_ElementSetId:='20'+m_ElementSetId;
    end else begin
      m_tle_EpochYear := StrToInt(copy(Line2,19,2))+1900;
      m_ElementSetId:='19'+m_ElementSetId;
    end;

    m_tle_EpochDay := StrToFloat(copy(Line2,21,12));
    m_tle_MMPrim := StrToFloat(Copy(Line2,34,10));

    Mant:=copy(Line2,45,6);
    Insert('0.',Mant,2);
    while pos(' ',Mant)>0 do
      delete(Mant,Pos(' ',Mant),1);
    Exp:=copy(Line2,51,2);
    Insert('E',Exp,1);
    m_tle_MMBis := StrToFloat(Mant+Exp);

    Mant:=copy(Line2,54,6);
    Insert('0.',Mant,2);
    Exp:=copy(Line2,60,2);
    Insert('E',Exp,1);
    m_tle_BstarDrag := StrToFloat(Mant+Exp);
    m_tle_ElementSet := StrToInt(copy(Line2,65, 4));

    m_tle_Inclination := StrToFloat(copy(line3, 9, 8));
    m_tle_Raan := StrToFloat(Copy(line3, 18, 8));
    m_tle_Eccentricity := StrToFloat('0.' + copy(line3,27, 7));
    m_tle_ArgOfPerigee := StrToFloat(copy(line3,35, 8));
    m_tle_MeanAnomaly := StrToFloat(copy(line3,44, 8));
    m_tle_MeanMotion := StrToFloat(copy(line3,53, 11));
    m_tle_RevnoAtEpoch := StrToInt(copy(line3,64, 5));

    Prepare;

    if trim(m_tle_InternationalDesignator)<>'' then
      FValid:=true
    else
      FValid:=false;
  except
    FValid:=false;
  end;

  DecimalSeparator:=SavePoint;
end;

procedure TSpaceObject.Prepare;

var
  PC : double;
  TEG : double;
  CO, SO : extended;
  CL, SL : extended;

begin
  (* Convert angles to radians, etc. *)
  m_Raan := DegToRad(m_tle_Raan);
  m_Inclination := DegToRad(m_tle_Inclination);
  m_ArgoOfPerigee := DegToRad(m_tle_ArgOfPerigee);
  m_MeanAnomaly := DegToRad(m_tle_MeanAnomaly);
  m_MeanMotion := m_tle_MeanMotion * 2.0 * Pi;  (* From revs/day to radians/day *)
  m_MeanMotionPrim := m_tle_MMPrim * 2.0 * Pi;  (* From revs/day^2 to radians/day^2 *)

  (* Convert satellite epoch to Day No. and fraction of a day *)
  m_EpochDay := FNday(m_tle_EpochYear, 1, 0) + trunc(m_tle_EpochDay);
  m_EpochTime := m_tle_EpochDay - trunc(m_tle_EpochDay);

  (* Average Precession rates *)
  N0 := m_MeanMotion / 86400.0;                 (* Mean motion rads/s                *)
  A0 := power(GM / N0 / N0, 1.0 / 3.0);         (* Semi major axis m                 *)
  B0 := A0 * sqrt(1.0 - m_tle_Eccentricity * m_tle_Eccentricity);  (* Semi minor axis m                *)
  sincos(m_Inclination,SI,CI);
  PC := RE * A0 / (B0 * B0);
  PC := 1.5 * J2 * PC * PC * m_MeanMotion;      (* Precession const, rad/day         *)
  QD := -PC * CI;                               (* Node Precession rate, rad/day     *)
  WD := PC *(5.0 * CI*CI - 1.0) / 2.0;          (* Perigee Precession rate, rad/day  *)
  DC := -2.0 * m_MeanMotionPrim / m_MeanMotion / 3.0; (* Drag coeff                        *)

  (* Bring Sun data to satellite epoch *)
  TEG := (m_EpochDay - FNday(YG, 1, 0)) + m_EpochTime;  (* Elapsed Time: Epoch - YG          *)
  GHAE := DegToRad(G0) + TEG * WE;              (* GHA Aries, epoch                  *)
  MRSE := DegToRad(G0) + (TEG * WW) + Pi;       (* Mean RA Sun at Sat Epoch          *)
  MASE := DegToRad(MAS0 + MASD * TEG);          (* Mean MA Sun                       *)

  (* Antenna unit vector in orbit plane coordinates *)
  ALON := 180.0;
  ALAT := 0.0;
  sincos(DegToRad(ALON),SO,CO);
  sincos(DegToRad(ALAT),SL,CL);
  m_AntVec.X := -CL * CO;
  m_AntVec.Y := -CL * SO;
  m_AntVec.Z := -SL;
end;

procedure TSpaceObject.Store(ElementSetNumber : integer);

var
  db_name : string;
  db_designator : string;
  rowfound : boolean;

begin
  db_name:='';
  db_designator:='';
  with dmSatData.qryFindSat do begin
    ParamByName('Satellite_Id').AsInteger:=m_tle_Satnum1;
    rowfound:=false;
    Open;
    if not(EOF) then begin
      rowfound:=true;
      db_name:=FieldByName('name').AsString;
      db_designator:=FieldByName('designator').AsString;
    end;
    Close;
  end;
  if not(rowfound) then begin
    with dmSatData.tblSats do begin
      Open;
      Insert;
      FieldByName('satellite_id').AsInteger:=m_tle_Satnum1;
      FieldByName('name').AsString:=m_tle_Name;
      FieldByName('designator').AsString:=m_tle_InternationalDesignator;
      Post;
      Close;
    end;
  end;

  with dmSatData.qryFindElement do begin
    ParamByName('satellite_id').AsInteger:=m_tle_Satnum1;
    ParamByName('elementset_id').AsString:=m_ElementSetId;
    rowfound:=false;
    Open;
    if not(EOF) then begin
      rowfound:=true;
    end;
    Close;
  end;
  if not(rowfound) then begin
    with dmSatData.tblElements do begin
      Open;
      Insert;
      FieldByName('REFSatellite_id').AsInteger:=m_tle_Satnum1;
      FieldByName('ElementSet_Id').AsString:=m_ElementSetId;
      FieldByName('elementset_number').AsInteger:=ElementSetNumber;//m_tle_Elementset;

      FieldByName('epoch_year').AsInteger:=m_tle_EpochYear;
      FieldByName('epoch_day').AsFloat:=m_tle_EpochDay;
      FieldByName('revnumatepoch').AsInteger:=m_tle_RevnoAtEpoch;

      FieldByName('meanmotion').AsFloat:=m_tle_MeanMotion;
      FieldByName('meanmotionprim').AsFloat:=m_tle_MMPrim;
      FieldByName('meanmotionbis').AsFloat:=m_tle_MMBis;
      FieldByName('bstardragterm').AsFloat:=m_tle_BStarDrag;

      FieldByName('eccentricity').AsFloat:=m_tle_Eccentricity;
      FieldByName('inclination').AsFloat:=m_tle_Inclination;
      FieldByName('raan').AsFloat:=m_tle_Raan;
      FieldByName('argumentofperigee').AsFloat:=m_tle_ArgOfPerigee;
      FieldByName('meananomaly').AsFloat:=m_tle_MeanAnomaly;

      Post;
      Close;
    end;
  end;

end;

procedure TSpaceObject.Load(SatNum : integer; When : TDateTime);

var
  Epoch : double;

begin
  FValid:=False;
  m_tle_Satnum1:=0;
  Epoch:=1000*YearOf(When)+DayOfTheYear(When)+1;

  with dmSatData.qryFindSat do begin
    ParamByName('Satellite_Id').AsInteger:=SatNum;
    Open;
    while not(EOF) do begin
      m_tle_Satnum1:=SatNum;
      m_tle_Satnum2:=SatNum;
      m_tle_Name:=FieldByName('name').AsString;
      m_tle_InternationalDesignator:=FieldByName('Designator').AsString;
      Next;
    end;
    Close;
  end;

  if (m_tle_Satnum1=SatNum) then begin
    with dmSatData.qryGetElement do begin
      ParamByName('Satellite_id').AsInteger:=SatNum;
      ParamByName('epoch').AsFloat:=Epoch;
      Open;
      if not(EOF) then begin
        m_tle_EpochYear:=FieldByName('EPOCH_YEAR').AsInteger;
        m_tle_EpochDay:=FieldByName('EPOCH_DAY').AsFloat;
        m_tle_RevnoAtEpoch:=FieldByName('REVNUMATEPOCH').AsInteger;
        m_tle_MeanMotion:=FieldByName('MEANMOTION').AsFloat;
        m_tle_MMPrim:=FieldByName('MEANMOTIONPRIM').AsFloat;
        m_tle_MMBis:=FieldByName('MEANMOTIONBIS').AsFloat;
        m_tle_BStarDrag:=FieldByName('BSTARDRAGTERM').AsFloat;
        m_tle_Eccentricity:=FieldByName('ECCENTRICITY').AsFloat;
        m_tle_Inclination:=FieldByName('INCLINATION').AsFloat;
        m_tle_Raan:=FieldByName('RAAN').AsFloat;
        m_tle_ArgOfPerigee:=FieldByName('ARGUMENTOFPERIGEE').AsFloat;
        m_tle_MeanAnomaly:=FieldByName('MEANANOMALY').AsFloat;
        FLoadTime:=now;
        Self.Prepare;
        FValid:=True;
      end;
      Close;
    end;
  end;
end;

procedure TSpaceObject.SetSatNumber(const Value: integer);
begin
  FSatNumber := Value;
end;

procedure TSpaceObject.Load(When: TDateTime);
begin
  Load(FSatNumber,WHen);
end;

function TSpaceObject.GetAge: double;
begin
  result:=now-FLoadTime;
end;

procedure TSpaceObject.setTime(T : TDateTime);

var
  yearIn, monthIn, mDayIn, hourIn, minIn, secIn, millisIn : word;

begin
  DecodeDateTime(T,yearIn,monthIn,mDayIn,hourIn,MinIn,SecIn,millisIn);
  DN := trunc(FNday(yearIn, monthIn, mDayIn));
  TN := (hourIn + (minIn + (secIn / 60.0)) / 60.0) / 24.0;
end;

(* Calculate satellite position at DN, TN *)

procedure TSpaceObject.satvec();

var
  T,DT          : double;
  KD,KDP,M      : double;
  DR            : integer;
  EA,D          : double;
  CosEA,SinEA   : extended;
  DNOM          : double;
  A,B           : double;
  AP,CW,SW      : double;
  RAAN,CQ,SQ    : double;
  RS            : double;

  CX            : T3DVector;
  CY            : T3DVector;
  CZ            : T3DVector;
  SAT           : T3DVector;
  ANT           : T3DVector;
  VEL           : T3DVector;

  GHAA : double;

begin
  T := (DN - m_EpochDay) + (TN - m_EpochTime);                  (* Elapsed T since epoch             *)

  DT := DC * T / 2.0;                                           (* Linear drag terms                *)
  KD := 1.0 + 4.0 * DT;
  KDP := 1.0 - 7.0 * DT;
  M := m_MeanAnomaly + m_MeanMotion * T * (1.0 - 3.0 * DT);     (* Mean anomaly at YR,/ TN           *)
  DR := trunc(M / (2.0 * Pi));                                  (* Strip out whole no of revs        *)
  M := M - DR * 2.0 * Pi;                                       (* M now in range 0 - 2PI            *)
  RevNumber := m_tle_RevnoAtEpoch + DR;                         (* Current orbit number              *)

  (* Solve M = EA - m_Eccentricity * sin(EA) for EA given M, by Newton's method  *)
  EA := M;                                                      (* Initial solution                  *)
  repeat
    sincos(EA,SinEA,CosEA);
    DNOM := 1.0 - m_tle_Eccentricity * CosEA;
    D := (EA - m_tle_Eccentricity * SinEA - M) / DNOM;          (* Change EA to better resolution    *)
    EA := EA - D;                                               (* by this amount until converged    *)
  until (abs(D) <= 1.0E-5);

  (* Distances *)
  A := A0 * KD;
  B := B0 * KD;
  RS := A * DNOM;

  (* Calculate satellite position and velocity in plane of ellipse *)
  S.x := A * (CosEA - m_tle_Eccentricity);
  V.x := -A * SinEA / DNOM * N0;
  S.y := B * SinEA;
  V.y := B * CosEA / DNOM * N0;

  AP := m_ArgoOfPerigee + WD * T * KDP;
  CW := cos(AP);
  SW := sin(AP);

  RAAN := m_Raan + QD * T * KDP;
  CQ := cos(RAAN);
  SQ := sin(RAAN);

  (* Plane -> celestial coordinate transformation, [C] = [RAAN]*[IN]*[AP] *)
  CX.x := CW * CQ - SW * CI * SQ;
  CX.y := -SW * CQ - CW * CI * SQ;
  CX.z := SI * SQ;
  CY.x := CW * SQ + SW * CI * CQ;
  CY.y := -SW * SQ + CW * CI * CQ;
  CY.z := -SI * CQ;
  CZ.x := SW * SI;
  CZ.y := CW * SI;
  CZ.z := CI;

  (* Compute satellite's position vector, ANTenna axis unit vector    *)
  (*   and velocity  in celestial coordinates. (Note: Sz = 0, Vz = 0) *)
  SAT.x := S.x * CX.x + S.y * CX.y;
  ANT.x := m_AntVec.X * CX.x + m_AntVec.y * CX.y + m_AntVec.z * CX.z;
  VEL.x := V.x * CX.x + V.y * CX.y;
  SAT.y := S.x * CY.x + S.y * CY.y;
  ANT.y := m_AntVec.x * CY.x + m_AntVec.y * CY.y + m_AntVec.z * CY.z;
  VEL.y := V.x * CY.x + V.y * CY.y;
  SAT.z := S.x * CZ.x + S.y * CZ.y;
  ANT.z := m_AntVec.x * CZ.x + m_AntVec.y * CZ.y + m_AntVec.z * CZ.z;
  VEL.z := V.x * CZ.x + V.y * CZ.y;

  (* Also express SAT, ANT, and VEL in geocentric coordinates *)
  GHAA := GHAE + WE * T;       (* GHA Aries at elapsed time T *)
  sincos(-GHAA,SinEA,CosEA);

  S.x := SAT.x * CosEA - SAT.y * SinEA;
  self.A.x := ANT.x * CosEA - ANT.y * SinEA;
  V.x := VEL.x * CosEA - VEL.y * SinEA;
  S.y := SAT.x * SinEA + SAT.y * CosEA;
  self.A.y := ANT.x * SinEA + ANT.y * CosEA;
  V.y := VEL.x * SinEA + VEL.y * CosEA;
  S.z := SAT.z;
  self.A.z := ANT.z;
  V.z := VEL.z;

   //TODO: The calculations below assume a spherical earth.
   //      Lon and Lat calculated are geocentric, not geodetic
   //      Hight assumes Equator radius 
  (* Calculate sub-satellite Lat/Lon *)
  FSLon := RadToDeg(arctan2(S.y, S.x));  (* Lon, + East  *)
  FSLat := RadToDeg(arcsin(S.z / RS));   (* Lat, + North *)
  FHeight := Magnitude(S)-RE;  //Should this be S or SAT???
end;

end.
