object frmMain: TfrmMain
  Left = 845
  Top = 149
  Width = 1024
  Height = 768
  Caption = 'QLog'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDefaultPosOnly
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 722
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1014
      Height = 200
      Align = alTop
      TabOrder = 1
      DesignSize = (
        1014
        200)
      object Label1: TLabel
        Left = 264
        Top = 8
        Width = 60
        Height = 13
        Caption = 'Anropssignal'
      end
      object Label2: TLabel
        Left = 8
        Top = 8
        Width = 52
        Height = 13
        Caption = 'Datum & Tid'
      end
      object Label3: TLabel
        Left = 152
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Frekvens'
      end
      object Label4: TLabel
        Left = 488
        Top = 8
        Width = 81
        Height = 13
        Caption = 'Mottagen rapport'
      end
      object Label5: TLabel
        Left = 392
        Top = 8
        Width = 61
        Height = 13
        Caption = 'S'#228'nd rapport'
      end
      object Label6: TLabel
        Left = 208
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Trafiks'#228'tt'
      end
      object Label7: TLabel
        Left = 584
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Lokator'
      end
      object Label8: TLabel
        Left = 8
        Top = 56
        Width = 28
        Height = 13
        Caption = 'Namn'
      end
      object Label9: TLabel
        Left = 456
        Top = 56
        Width = 23
        Height = 13
        Caption = 'QTH'
      end
      object Label10: TLabel
        Left = 728
        Top = 8
        Width = 56
        Height = 13
        Caption = 'DXCC-Land'
      end
      object Label11: TLabel
        Left = 8
        Top = 104
        Width = 63
        Height = 13
        Caption = 'Anteckningar'
      end
      object Label12: TLabel
        Left = 456
        Top = 104
        Width = 55
        Height = 13
        Caption = 'Egen signal'
      end
      object Label13: TLabel
        Left = 728
        Top = 104
        Width = 80
        Height = 13
        Caption = 'Rad i papperslog'
      end
      object Label14: TLabel
        Left = 232
        Top = 56
        Width = 28
        Height = 13
        Caption = 'Kallas'
      end
      object Label15: TLabel
        Left = 592
        Top = 104
        Width = 64
        Height = 13
        Caption = 'Egen Lokator'
      end
      object Label16: TLabel
        Left = 8
        Top = 152
        Width = 25
        Height = 13
        Caption = 'Urval'
      end
      object Label17: TLabel
        Left = 680
        Top = 56
        Width = 14
        Height = 13
        Caption = 'SK'
      end
      object Label18: TLabel
        Left = 672
        Top = 8
        Width = 41
        Height = 13
        Caption = 'Komplett'
      end
      object btnClearFilter: TButton
        Left = 928
        Top = 168
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = #197'terst'#228'll'
        Enabled = False
        TabOrder = 0
        OnClick = btnClearFilterClick
      end
      object btnFilter: TButton
        Left = 840
        Top = 168
        Width = 75
        Height = 25
        Anchors = [akTop, akRight]
        Caption = 'Filtrera...'
        TabOrder = 1
        OnClick = btnFilterClick
      end
      object ComboBox1: TComboBox
        Left = 8
        Top = 168
        Width = 817
        Height = 21
        ItemHeight = 13
        TabOrder = 2
        Text = 'Alla'
        OnChange = ComboBox1Change
        Items.Strings = (
          'Alla'
          '50MHz'
          '144MHz'
          '432MHz'
          '1296MHz'
          'SM4RPP'
          'SM4RPQ'
          'Hemma'
          'Bl'#229'sj'#246'n'
          'Aurora'
          'Satellit'
          'Meteorscatter')
      end
      object edtWeekday: TEdit
        Left = 120
        Top = 24
        Width = 25
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 3
      end
      object cbSilentKey: TCheckBox
        Left = 680
        Top = 72
        Width = 25
        Height = 17
        TabOrder = 4
      end
      object edtFrequency: TEdit
        Left = 152
        Top = 24
        Width = 49
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
      object edtMode: TEdit
        Left = 208
        Top = 24
        Width = 49
        Height = 21
        ReadOnly = True
        TabOrder = 6
      end
      object edtWorkedCall: TEdit
        Left = 264
        Top = 24
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 7
      end
      object edtLocatorQ: TEdit
        Left = 584
        Top = 24
        Width = 81
        Height = 21
        ReadOnly = True
        TabOrder = 8
      end
      object edtStarttime: TEdit
        Left = 8
        Top = 24
        Width = 105
        Height = 21
        ReadOnly = True
        TabOrder = 9
      end
      object edtRptS: TEdit
        Left = 392
        Top = 24
        Width = 41
        Height = 21
        ReadOnly = True
        TabOrder = 10
      end
      object edtRptR: TEdit
        Left = 488
        Top = 24
        Width = 41
        Height = 21
        ReadOnly = True
        TabOrder = 11
      end
      object edtDXCC: TEdit
        Left = 728
        Top = 24
        Width = 217
        Height = 21
        ReadOnly = True
        TabOrder = 12
      end
      object edtName: TEdit
        Left = 8
        Top = 72
        Width = 217
        Height = 21
        ReadOnly = True
        TabOrder = 13
      end
      object edtNick: TEdit
        Left = 232
        Top = 72
        Width = 217
        Height = 21
        ReadOnly = True
        TabOrder = 14
      end
      object edtQTH: TEdit
        Left = 456
        Top = 72
        Width = 217
        Height = 21
        ReadOnly = True
        TabOrder = 15
      end
      object edtSerS: TEdit
        Left = 432
        Top = 24
        Width = 49
        Height = 21
        ReadOnly = True
        TabOrder = 16
      end
      object edtSerR: TEdit
        Left = 528
        Top = 24
        Width = 49
        Height = 21
        ReadOnly = True
        TabOrder = 17
      end
      object edtComment: TEdit
        Left = 8
        Top = 120
        Width = 441
        Height = 21
        ReadOnly = True
        TabOrder = 18
      end
      object edtRefCall: TEdit
        Left = 456
        Top = 120
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 19
      end
      object edtLocatorUL: TEdit
        Left = 592
        Top = 120
        Width = 121
        Height = 21
        ReadOnly = True
        TabOrder = 20
      end
      object edtPaperlogRef: TEdit
        Left = 728
        Top = 120
        Width = 57
        Height = 21
        ReadOnly = True
        TabOrder = 21
      end
      object cbComplete: TCheckBox
        Left = 672
        Top = 24
        Width = 33
        Height = 17
        TabOrder = 22
      end
    end
    object StatusBar1: TStatusBar
      Left = 1
      Top = 702
      Width = 1014
      Height = 19
      Panels = <
        item
          Width = 100
        end
        item
          Width = 50
        end
        item
          Width = 50
        end>
      SimplePanel = False
    end
    object OvcTable1: TOvcTable
      Left = 1
      Top = 201
      Width = 1014
      Height = 501
      RowLimit = 400
      LockedCols = 0
      LeftCol = 0
      ActiveCol = 0
      Access = otxReadOnly
      Align = alClient
      GridPenSet.NormalGrid.NormalColor = clBtnShadow
      GridPenSet.NormalGrid.Style = psDot
      GridPenSet.NormalGrid.Effect = geBoth
      GridPenSet.LockedGrid.NormalColor = clBtnShadow
      GridPenSet.LockedGrid.Style = psSolid
      GridPenSet.LockedGrid.Effect = ge3D
      GridPenSet.CellWhenFocused.NormalColor = clBlack
      GridPenSet.CellWhenFocused.Style = psSolid
      GridPenSet.CellWhenFocused.Effect = geBoth
      GridPenSet.CellWhenUnfocused.NormalColor = clBlack
      GridPenSet.CellWhenUnfocused.Style = psDash
      GridPenSet.CellWhenUnfocused.Effect = geBoth
      LockedRowsCell = OvcTCSLeft
      Options = [otoNoRowResizing, otoNoColResizing]
      TabOrder = 0
      OnActiveCellChanged = OvcTable1ActiveCellChanged
      OnGetCellData = OvcTable1GetCellData
      OnGetCellAttributes = OvcTable1GetCellAttributes
      CellData = (
        'frmMain.OvcTCSLeft'
        'frmMain.OvcTCSRight')
      RowData = (
        20)
      ColData = (
        112
        False
        True
        'frmMain.OvcTCSLeft'
        124
        False
        True
        'frmMain.OvcTCSLeft'
        64
        False
        True
        'frmMain.OvcTCSRight'
        54
        False
        True
        'frmMain.OvcTCSLeft'
        54
        False
        True
        'frmMain.OvcTCSLeft'
        60
        False
        True
        'frmMain.OvcTCSLeft'
        196
        False
        True
        'frmMain.OvcTCSLeft'
        196
        False
        True
        'frmMain.OvcTCSLeft'
        45
        False
        True
        'frmMain.OvcTCSLeft'
        28
        False
        True
        'frmMain.OvcTCSLeft'
        50
        False
        True
        'frmMain.OvcTCSLeft'
        50
        False
        True
        'frmMain.OvcTCSLeft'
        124
        False
        True
        'frmMain.OvcTCSLeft')
    end
  end
  object MainMenu1: TMainMenu
    Left = 984
    Top = 136
    object File1: TMenuItem
      Caption = '&File'
      object itmNewLogbook: TMenuItem
        Caption = '&New'
      end
      object itmOpenLogbook: TMenuItem
        Caption = '&Open...'
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object itmImport: TMenuItem
        Caption = 'Import'
        object itmImportCSV: TMenuItem
          Caption = 'CSV'
          OnClick = itmImportCSVClick
        end
        object itmImportTaclog: TMenuItem
          Caption = 'Taclog'
          OnClick = itmImportTaclogClick
        end
        object itmImportXML: TMenuItem
          Caption = 'XML'
          OnClick = itmImportXMLClick
        end
        object itmImportTLE: TMenuItem
          Caption = 'TLE'
          OnClick = itmImportTLEClick
        end
      end
      object itmExport: TMenuItem
        Caption = 'Export'
        object itmExportSquaresQGIS: TMenuItem
          Caption = 'Square List for QGIS'
          OnClick = itmExportSquaresQGISClick
        end
        object itmExportCompleteXML: TMenuItem
          Caption = 'Complete XML file'
          OnClick = itmExportCompleteXMLClick
        end
        object itmExportADIF: TMenuItem
          Caption = 'ADIF'
          OnClick = itmExportADIFClick
        end
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object itmPrint: TMenuItem
        Caption = '&Print...'
      end
      object itmPrintSetup: TMenuItem
        Caption = 'P&rint Setup...'
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object itmExit: TMenuItem
        Caption = 'E&xit'
        OnClick = itmExitClick
      end
    end
    object N4: TMenuItem
      Caption = '&Inmatning'
      Visible = False
    end
    object Statistics1: TMenuItem
      Caption = '&Statistik'
      object itmStatSquare: TMenuItem
        Caption = 'Rutor'
        OnClick = itmStatSquareClick
      end
      object itmStatDXCC: TMenuItem
        Caption = 'DXCC'
        OnClick = itmStatDXCCClick
      end
      object itmStatOther: TMenuItem
        Caption = 'Andra Diplom'
        OnClick = itmStatOtherClick
      end
    end
    object QSL1: TMenuItem
      Caption = '&QSL'
      object itmQslRules: TMenuItem
        Caption = 'QSL-regler'
        OnClick = itmQslRulesClick
      end
      object itmPrintQSLLabels: TMenuItem
        Caption = 'Skriv ut etiketter f'#246'r fysiska QSL'
      end
      object itmRegisterQSLRcvd: TMenuItem
        Caption = 'Registrera mottagna fysiska QSL'
      end
      object itmSendEQSL: TMenuItem
        Caption = 'Skicka elektroniska QSL'
      end
      object itmRegisterEQSL: TMenuItem
        Caption = 'Registrera mottagna elektroniska QSL'
      end
    end
    object Visa1: TMenuItem
      Caption = 'Visa'
      object itmShowConstellation: TMenuItem
        Caption = 'Satellitkonstellation'
        OnClick = itmShowConstellationClick
      end
      object itmShowMap: TMenuItem
        Caption = 'Karta'
      end
    end
  end
  object OvcTCSLeft: TOvcTCString
    Color = clWindow
    Table = OvcTable1
    TableColor = False
    Left = 432
    Top = 240
  end
  object OvcTCSRight: TOvcTCString
    Adjust = otaCenterRight
    Color = clWindow
    Table = OvcTable1
    TableColor = False
    Left = 488
    Top = 240
  end
  object OvcFormState1: TOvcFormState
    Options = [fsState, fsPosition]
    Section = 'TfrmMain'
    Storage = OvcRegistryStore1
    Left = 984
    Top = 32
  end
  object OvcRegistryStore1: TOvcRegistryStore
    KeyName = 'Software\SM4RPQ\LogProg2'
    Left = 952
    Top = 32
  end
end
