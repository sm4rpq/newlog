unit SatData;

{
  Purpose:
    Data module for components to access satellite tables.
  Status:
    Work in progress
  Date:
    2021-04-25
}

interface

uses
  SysUtils, Classes, IBQuery, IBDatabase, DB, IBCustomDataSet, IBTable,
  Connection;

type
  TdmSatData = class(TDataModule)
    tblSats: TIBTable;
    tblElements: TIBTable;
    qryFindSat: TIBQuery;
    qryFindElement: TIBQuery;
    qryGetElement: TIBQuery;
    qryActiveSats: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmSatData: TdmSatData;

implementation

{$R *.dfm}

end.
