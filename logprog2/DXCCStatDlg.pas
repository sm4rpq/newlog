unit DXCCStatDlg;

{
  Purpose:
    Dialogue for displaying dxcc statistics.
  Status:
    Show QSL Status
  Date:
    2021-03-28
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls;

type
  TdlgStatDXCC = class(TForm)
    rgCallsigns: TRadioGroup;
    rgPlace: TRadioGroup;
    rgFrequency: TRadioGroup;
    rgPropagation: TRadioGroup;
    redStat: TRichEdit;
    btnClose: TButton;
    procedure recalcStat(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgStatDXCC: TdlgStatDXCC;

implementation

uses ExportData;

{$R *.dfm}

procedure TdlgStatDXCC.recalcStat(Sender: TObject);

var
  Count     : integer;
  DxccName  : string;
  Row       : string;

begin
  with dmDataExport.qrySqrsQGIS do begin
    SQL.Clear;
    SQL.Add('select de.DXCC_ID,de.name,de.svnamn,count(*) From');
    SQL.Add(' qso q,DXCC_ENTITY de,USEDLOCATION ul');
    SQL.Add('where');
    SQL.Add(' de.DXCC_ID=q.REFDXCC_ID and');
    SQL.Add(' ul.USEDLOCATION_ID=q.REFUSEDLOCATION_ID');

    case rgCallsigns.ItemIndex of
      0 : begin //All - no condition needed
          end;
      1 : begin //SM4RPP
            SQL.Add('and q.REFOPCALL=''SM4RPP''');
          end;
      2 : begin
            SQL.Add('and q.REFOPCALL=''SM4RPQ''');
          end;
    end;
    case rgPlace.ItemIndex of
      0 : ;// No condition
      1 : begin //Hemma
            SQL.Add('and ul.locator<''JP''');
          end;
      2 : begin //Stugan
            SQL.Add('and ul.locator>''JP74''');
          end;
    end;
    case rgFrequency.ItemIndex of
      0 : ; //Alla band
      1 : //Kortv�g
          SQL.Add('and q.Frequency_rx<30.0');
      2 : //50MHz
          SQL.Add('and q.Frequency_rx>=50.0 and q.Frequency_rx<=54.0');
      3 : //144MHz
          SQL.Add('and q.Frequency_rx>=144.0 and q.Frequency_rx<=146.0');
      4 : //432MHz
          SQL.Add('and q.Frequency_rx>=432.0 and q.Frequency_rx<=432.0');
      5 : //1296MHz
          SQL.Add('and q.Frequency_rx>=1240.0 and q.Frequency_rx<=1300.0');
    end;
    case rgPropagation.ItemIndex of
      0 : ; //Allt
      1 : // Ej repeater
          SQL.Add('and (q.refpropagationname not in (''RPT'',''SAT'') or q.refpropagationname is null)');
      2 : // Aurora
          SQL.Add('and (q.refpropagationname=''AUR'' or q.refpropagationname=''AUE'')');
      3 : // Satellit
          SQL.Add('and q.refpropagationname=''SAT''');
      4 : // Meteorscatter
          SQL.Add('and q.refpropagationname=''MS''');
    end;
    SQL.Add('and (complete<>''F'' or complete is null)');
    SQL.Add('group by de.DXCC_ID,de.name,de.SVNAMN order by de.dxcc_id');

    Open;
    redStat.Clear;
    Count:=0;
    redStat.Lines.Add('');
    redStat.Lines.Add('Ant.QSO    DXCC-Land');
    redStat.Lines.Add('------------------------------------------------------');
    while not EOF do begin
      if FieldByName('svnamn').IsNull then
        DxccName:=FieldByName('Name').AsString
      else
        DxccName:=FieldByName('SvNamn').AsString;

      Row:=Format('%7d    %s',[
//        FieldByName('DXCC_Id').AsInteger,
        FieldByName('Count').AsInteger,
        DxccName]);
      redStat.Lines.Add(Row);
      inc(Count);
      Next;
    end;
    Close;
  end;
  redStat.Lines.Insert(0,'Totalt antal DXCC: '+IntToStr(Count));
end;

procedure TdlgStatDXCC.FormShow(Sender: TObject);
begin
  recalcStat(Sender);
end;

end.
