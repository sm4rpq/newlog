unit Groundstation;

{
  Purpose:
    Provide functions for calculations on a position re: satellite tracking.
    Math based on plan13 in qrptracker on github. That is in turn based on
    G3RUHs old plan13 basic program.
  Status:
    Needs integration with the base class TGeoCoord.
  Date:
    2021-04-25
}

interface

uses
  GeoUnit, Vector;

type
  TGroundStation = class(TGeoCoord)
  public
    E : T3DVector;
    N : T3DVector;
    O : T3DVector;
    U : T3DVector;
    VO : T3DVector;

    procedure setLocation(lat, lon : double; height : integer);
  end;

implementation

uses
  Math, GeoConstants;

procedure TGroundStation.setLocation(lat, lon : double; height : integer);

var
  HT : double;
  CL,SL,CO,SO : double;
  D : double;
  Rx,Rz : double;

begin
  HT := height; // this needs to be in m

  CL := cos(DegToRad(lat));
  SL := sin(DegToRad(lat));
  CO := cos(DegToRad(lon));
  SO := sin(DegToRad(lon));

  D := sqrt(XX * CL * CL + ZZ * SL * SL);

  Rx := XX / D + HT;
  Rz := ZZ / D + HT;

  (* Observer's unit vectors Up EAST and NORTH in geocentric coordinates *)
  U.x := CL * CO;
  E.x := -SO;
  N.x := -SL * CO;

  U.y := CL * SO;
  E.y := CO;
  N.y := -SL * SO;

  U.z := SL;
  E.z := 0;
  N.z := CL;

  (* Observer's XYZ coordinates at earth's surface *)
  O.x := Rx * U.x;
  O.y := Rx * U.y;
  O.z := Rz * U.z;

  (* Observer's velocity, geocentric coordinates *)
  VO.x := -O.y * W0;
  VO.y := O.x * W0;
  VO.Z := 0.0;
end;

end.
