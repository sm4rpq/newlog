unit XMLImportDlg;

{
  Purpose:
    Provides import dialogue for xml-files exported from vqlog or this program
  Status:
    Find a way to avoid duplicates
    Must handle updates of contactedstation
    Not fully compatible with the programs own XML output format.
    Remove hardcoded award ids for us state and iota
  Date:
    2021-04-14
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ComCtrls, XMLIntf, XMLDOC, MyStrList;

type
  TdlgXMLImport = class(TForm)
    btnClose: TButton;
    Label2: TLabel;
    edtImportPath: TEdit;
    btnExecute: TButton;
    lblImport: TLabel;
    pbProgress: TProgressBar;
    btnSelect: TSpeedButton;
    btnRollback: TButton;
    btnCommit: TButton;
    OpenDialog1: TOpenDialog;
    procedure btnCloseClick(Sender: TObject);
    procedure btnSelectClick(Sender: TObject);
    procedure edtImportPathChange(Sender: TObject);
    procedure btnExecuteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    vucc_id : integer;
    Fail : boolean;
    CallList : TMyStringList;
    FStartPath : string;

    procedure ProcessLogNode(Node : IXMLNode);
  public
    { Public declarations }
  end;

var
  dlgXMLImport: TdlgXMLImport;

implementation

uses
  ImportData, Connection, Settings;

{$R *.dfm}

procedure TdlgXMLImport.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TdlgXMLImport.btnSelectClick(Sender: TObject);
begin
  if (FStartPath<>'') then
    OpenDialog1.InitialDir:=FStartPath;
  if OpenDialog1.Execute then begin
    edtImportPath.Text:=OpenDialog1.FileName;
  end;
end;

procedure TdlgXMLImport.edtImportPathChange(Sender: TObject);
begin
  if FileExists(edtImportPath.Text) then begin
    edtImportPath.Font.Color:=clWindowText;
    btnExecute.Enabled:=True;
  end else begin
    edtImportPath.Font.Color:=clRed;
    btnExecute.Enabled:=False;
  end;
end;

procedure TdlgXMLImport.btnExecuteClick(Sender: TObject);

var
  Document : IXMLDocument;
  i        : integer;

begin
  dmSettings.XMLImportPath:=ExtractFilePath(edtImportPath.Text);

  btnClose.Enabled:=False;
  Application.ProcessMessages;

  CallList:=TMyStringList.Create;
  CallList.Delimiter:='/';
  Fail:=False;
  try
    Document:=LoadXMLDocument(edtImportPath.Text);
    pbProgress.Min:=0;
    pbProgress.Max:=Document.DocumentElement.ChildNodes.Count;
    pbProgress.Position:=0;

    dmConnection.IBTransaction1.Rollback;
    dmConnection.IBTransaction1.StartTransaction;
    Vucc_id:=dmConnection.Get_Award_Id('VUCC');

    with Document.DocumentElement do
      for i:=0 to ChildNodes.Count-1 do
        ProcessLogNode(ChildNodes[i]);

//    Fail:=True; //TODO: Remove, just for test
  finally
    CallList.Free;
    if Fail then
      dmConnection.IBTransaction1.Rollback
    else
      dmConnection.IBTransaction1.Commit;
  end;

  btnClose.Enabled:=True;
end;

procedure TdlgXMLImport.FormCreate(Sender: TObject);
begin
  edtImportPathChange(Sender);
end;

procedure TdlgXMLImport.ProcessLogNode(Node: IXMLNode); //Node is a <record> element

  procedure SetOrNull(Param : string; Value : String); overload;
  begin
    if Value<>'' then
      dmDataImport.qryInsertQSO.ParamByName(Param).AsString:=Value
    else
      dmDataImport.qryInsertQSO.ParamByName(Param).IsNull:=True;
  end;

  procedure SetOrNull(Param : string; Value : OLEVariant); overload;
  begin
    if varisnull(Value) then
      dmDataImport.qryInsertQSO.ParamByName(Param).IsNull:=True
    else
      dmDataImport.qryInsertQSO.ParamByName(Param).AsString:=Value;
  end;

  function ToString(Value : OleVariant) : string;
  begin
    if VarIsNull(Value) then
      result:=''
    else
      result:=Value;
  end;

  function ToStringI(Value : OleVariant) : string;
  begin
    if VarIsNull(Value) then
      result:='0'
    else
      result:=Value;
  end;

  procedure NameSplit(FullName : string; var Name : string; var Nick: string);

  var
    ParPos : integer;

  begin
    ParPos:=Pos('(',FullName);
    if ParPos>0 then begin
      Nick:=Copy(FullName,1,ParPos-1);
      Name:=Copy(FullName,ParPos+1,Length(FullName)-ParPos-1);
    end else begin
      Name:=FullName;
      Nick:='';
    end;
  end;

  function ParseDate(D,T : string) : TDateTime;

  var
    Year : integer;
    Month : integer;
    Day : integer;
    Hour : integer;
    Minute : integer;

  begin
    Year:=StrToInt(Copy(D,1,4));
    Month:=StrToInt(Copy(D,6,2));
    Day:=StrToInt(Copy(D,9,2));
    Hour:=StrToInt(Copy(T,1,2));
    Minute:=StrToInt(Copy(T,4,2));
    Result:=EncodeDate(Year,Month,Day)+EncodeTime(Hour,Minute,0,0);
  end;

  function RemapMode(Mode : string) : string;
  begin
    if Mode='FSK' then
      Result:='JT6M'
    else if Mode='HELL' then
      Result:='UNKNOWN'
    else
      Result:=Mode;
  end;

type
  TOperatingCondition = record
    RefCall : string;
    RefOpCall : string;
    RefUsedEq_Id : integer;
    RefUsedLocation_Id : integer;
  end;

var
  BaseCall  : string;
  qso_id    : integer;
  dxcc_id   : integer;
  OperatingCondition : TOperatingCondition;
  StartTime : TDateTime;
  EndTime   : TDateTime;
  AwardItem : String;
  ContactedStation : TContactedStation;
  Sat_Id    : integer;


begin
  with dmDataImport.qryInsertQSO do begin
    //Get a new QSO id
    QSO_Id:=dmConnection.Get_Unique_Id;
    ParamByName('QSO_Id').AsInteger:=QSO_Id;

    pbProgress.StepBy(1);
    BaseCall:=Node.ChildValues['RootCall'];
    lblImport.Caption:=BaseCall;
    Application.ProcessMessages;
    CallList.SetDelimitedText(Node.ChildValues['Call']);

    StartTime:=ParseDate(Node.ChildValues['DateOn'],Node.ChildValues['TimeOn']);
    dxcc_id:=dmConnection.Get_Dxcc_Id(CallList[0],StartTime);

    with dmDataImport.qryLocation do begin
      ParamByName('usedcall').AsString:=ToString(Node.ChildValues['OwnCall']);
      ParamByName('usedloc').AsString:=ToString(Node.ChildValues['OwnLoc'])+'%';
      Open;
      if not(EOF) then begin
        OperatingCondition.RefCall:=FieldByName('RefCall').AsString;
        OperatingCondition.RefOpCall:=FieldByName('RefOpCall').AsString;
        OperatingCondition.RefUsedEq_Id:=FieldByName('RefUsedEq_Id').AsInteger;
        OperatingCondition.RefUsedLocation_Id:=FieldByName('RefUsedLocation_Id').AsInteger;
      end else begin
        OperatingCondition.RefCall:='';
        OperatingCondition.RefOpCall:='';
        OperatingCondition.RefUsedEq_Id:=0;
        OperatingCondition.RefUsedLocation_Id:=0;
      end;
      Close;
    end;

    ContactedStation:=dmDataImport.RegisterContactedStation(Node.ChildValues['Call'],ToString(Node.ChildValues['Name']),ToString(Node.ChildValues['Manager']));

    ParamByName('StartTime').AsDateTime:=StartTime;
    if VarIsNull(Node.ChildValues['DateOff']) then
      ParamByName('EndTime').IsNull:=True
    else begin
      EndTime:=ParseDate(Node.ChildValues['DateOn'],Node.ChildValues['DateOff']);
      ParamByName('EndTime').AsDateTime:=EndTime;
    end;
    ParamByName('Call').AsString:=Node.ChildValues['Call'];
    ParamByName('Power').AsFloat:=StrToFloat(ToStringI(Node.ChildValues['Pwr']));
    ParamByName('Frequency_TX').AsDouble:=StrToFloat(Node.ChildValues['FreqUp']);
    if Node.ChildValues['FreqDn']<>'0' then
      ParamByName('Frequency_RX').AsDouble:=StrToFloat(Node.ChildValues['FreqDn'])
    else
      ParamByName('Frequency_RX').AsDouble:=StrToFloat(Node.ChildValues['FreqUp']);
    SetOrNull('Rpt_S',Node.ChildValues['RSTs']);
    SetOrNull('Rpt_R',Node.ChildValues['RSTr']);
    SetOrNull('Locator',Node.ChildValues['Locator']);

    SetOrNull('QTH',Node.ChildValues['Place']); //QTH
    if (Node.ChildValues['Complete']<>'NO') then
      SetOrNull('Complete','') // Complete?
    else
      SetOrNull('Complete','F'); // Complete?
    SetOrNull('Comment',Node.ChildValues['Notes']);
    ParamByName('O_Pwr').IsNull:=true;
    SetOrNull('O_Ant',''); //Ant

    ParamByName('Paperlogref').IsNull:=true;

    ParamByName('REFMode').AsString:=RemapMode(Node.ChildValues['Mode']);
    SetOrNull('REFPropagationName',Node.ChildValues['Propagation']);

    ParamByName('REFCall').AsString:=OperatingCondition.RefCall;
    ParamByName('REFOpCall').AsString:=OPeratingCondition.RefOpCall;;
    ParamByName('REFUsedEq_Id').AsInteger:=OperatingCondition.RefUsedEq_Id;
    ParamByName('REFUsedLocation_Id').AsInteger:=OperatingCondition.RefUsedLocation_Id;

    ParamByName('REFBaseCall').AsString:=ContactedStation.RefBaseCall;
    ParamByName('REFInstance').AsInteger:=ContactedStation.RefInstance;

    if Dxcc_id<>0 then
      ParamByName('REFDXCC_Id').AsInteger:=dxcc_id
    else
      ParamByName('REFDXCC_Id').IsNull:=true;

    ParamByName('REFContest_Id').IsNull:=true;

    if VarIsNull(Node.ChildValues['SatMode']) then
      ParamByName('Sat_Mode').IsNull:=True
    else
      ParamByName('Sat_Mode').AsString:=ToString(Node.ChildValues['SatMode']);

    if ToString(Node.ChildValues['Sat'])<>'' then begin
      with dmDataImport.qryFindSat do begin
        ParamByName('Name').AsString:='%'+ToString(Node.ChildValues['Sat'])+'%';
        Open;
        if not(EOF) then
          Sat_Id:=FieldByName('Satellite_id').AsInteger
        else begin
          ShowMessage('Satellite named '''+Node.ChildValues['Sat']+''' not found');
          Sat_Id:=0;
        end;
        Close;
      end;
      if Sat_Id<>0 then
        ParamByName('REFSatellite_Id').AsInteger:=Sat_Id
      else
        ParamByName('REFSatellite_Id').IsNull:=true;
    end else
        ParamByName('REFSatellite_Id').IsNull:=true;

    ExecQuery;

    if ToString(Node.ChildValues['QSLRcvd'])<>'' then
      dmDataImport.RegisterReceivedQSL(QSO_Id,ToString(Node.ChildValues['QSLRcvd']));
    if ToString(Node.ChildValues['QSLSent'])<>'' then
      dmDataImport.RegisterSentQSL(QSO_Id,ToString(Node.ChildValues['QSLSent']));

    AwardItem:=Copy(ToString(Node.ChildValues['Locator']),1,4);
    if AwardItem<>'' then
      dmDataImport.RegisterAwardCredit(vucc_id,AwardItem,qso_id); //include rconf_id

    AwardItem:=ToString(Node.ChildValues['USState']);
    if AwardItem<>'' then
      dmDataImport.RegisterAwardCredit(9,AwardItem,qso_id);

    AwardItem:=ToString(Node.ChildValues['IOTA']);
    if AwardItem<>'' then
      dmDataImport.RegisterAwardCredit(10,AwardItem,qso_id);

  end;
end;

procedure TdlgXMLImport.FormShow(Sender: TObject);
begin
  FStartPath:=dmSettings.XMLImportPath;
end;

end.
