unit ExportSquareListDlg;

{
  Purpose:
    Provide export of a list of squares for use in QGIS to create a nice looking
    map of worked squares on 50MHz
  Status:
    Kind of works but:
      Band selection needs overhaul
  Date:
    2023-02-01
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, StdCtrls, ComCtrls, Buttons, Vector;

type
  TdlgExportSquareList = class(TForm)
    btnRun: TButton;
    pbProgress: TProgressBar;
    btnClose: TButton;
    edtExportFile: TEdit;
    spdExportFile: TSpeedButton;
    odFilename: TOpenDialog;
    lvUsedCalls: TListView;
    lvOperatorCalls: TListView;
    lvUsedLocations: TListView;
    cbxOperatingCondition: TComboBox;
    lvBand: TListView;
    btnAllBands: TButton;
    ComboBox1: TComboBox;
    Label1: TLabel;
    cbxMode: TComboBox;
    cbxProp: TComboBox;
    Label2: TLabel;
    Label3: TLabel;
    btnAllUsedCalls: TButton;
    procedure spdExportFileClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbxOperatingConditionSelect(Sender: TObject);
    procedure lvUsedLocationsMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btnRunClick(Sender: TObject);
    procedure btnAllBandsClick(Sender: TObject);
    procedure btnAllUsedCallsClick(Sender: TObject);
  private
    { Private declarations }
    procedure doSelect(lvView : TListView; Item : string);
  public
    { Public declarations }
  end;

var
  dlgExportSquareList: TdlgExportSquareList;

implementation

{$R *.dfm}

uses
  Settings, ExportData, GeoUnit;

type
  TOperatingCondition = class(TObject)
    RefCall : string;
    RefOpCall : string;
    RefUsedLocation : integer;
  end;

procedure TdlgExportSquareList.btnRunClick(Sender: TObject);

var
  Square : string;
  Count  : integer;
  FirstQ : TDateTime;
  Lon    : double;
  Lat    : double;
  OutStr : string;
  OutLst : TStringList;
  RowCnt : integer;
  i      : integer;
  Query  : string;
  VuccId : integer;


begin
  OutLst:=TStringList.Create;
  OutLst.Add('"SQUARE"'#9'"Count"'#9'"LON"'#9'"LAT"'#9'"FIRST"');

  with dmDataExport.qryAwards do begin
    Open;
    while not(EOF) do begin
      if UpperCase(FieldByName('Name').AsString)='VUCC' then
        VuccId:=FieldByName('Award_Id').AsInteger;
      Next;
    end;
    Close
  end;

  with dmDataExport.qrySqrsQGIS do begin
    SQL.Clear;
    SQL.Add('select ai.name,count(*),min(q.starttime) From');
    SQL.Add(' awarditem ai,qso q,receivedawardcredit rac,usedlocation ul');
    SQL.Add('where');
    SQL.Add(' rac.ASOQSO_ID=q.QSO_ID and');
    SQL.Add(' rac.ASOAWARDITEM_ID=ai.AWARDITEM_ID and');
    SQL.Add(' ai.REFAWARD_ID='+IntToStr(VuccId)+' and');
    SQL.Add(' ul.USEDLOCATION_ID=q.REFUSEDLOCATION_ID');

    //Handle used call conditions
    Query:='';
    for i:=0 to lvUsedCalls.Items.Count-1 do begin
      if lvUsedCalls.Items[i].Checked then begin
        if Query<>'' then Query:=Query+' or ';
        Query:=Query+'refcall='''+lvUsedCalls.Items[i].Caption+'''';
      end;
    end;
    if Query<>'' then
      SQL.Add('and ('+Query+')');

    //Handle operator call conditions
    Query:='';
    for i:=0 to lvOperatorCalls.Items.Count-1 do begin
      if lvOperatorCalls.Items[i].Checked then begin
        if Query<>'' then Query:=Query+' or ';
        Query:=Query+'refopcall='''+lvOperatorCalls.Items[i].Caption+'''';
      end;
    end;
    if Query<>'' then
      SQL.Add('and ('+Query+')');

    //Handle locations conditions
    Query:='';
    for i:=0 to lvUsedLocations.Items.Count-1 do begin
      if lvUsedLocations.Items[i].Checked then begin
        if Query<>'' then Query:=Query+' or ';
        Query:=Query+'ul.locator='''+lvUsedLocations.Items[i].Caption+'''';
      end;
    end;
    if Query<>'' then
      SQL.Add('and ('+Query+')');

    //Handle band conditions
    if lvBand.Items[0].Checked then
      SQL.Add('and frequency_tx<30');
    if lvBand.Items[1].Checked then
      SQL.Add('and frequency_tx>=50 and frequency_tx<=52');
    if lvBand.Items[2].Checked then
      SQL.Add('and frequency_tx>=144 and frequency_tx<=146');
    if lvBand.Items[3].Checked then
      SQL.Add('and frequency_tx>=432 and frequency_tx<=438');
    if lvBand.Items[4].Checked then
      SQL.Add('and frequency_tx>=1240 and frequency_tx<=1300');

    //Only complete QSOs count
    SQL.Add('and (complete<>''F'' or complete is null)');

    //Mode
    if cbxMode.ItemIndex>0 then
      SQL.Add('and REFMODE='''+cbxMode.Text+'''');

    //Propagation
    if cbxProp.ItemIndex>0 then
      SQL.Add('and REFPROPAGATIONNAME='''+cbxProp.Text+'''')
    else
      SQL.Add('and ((REFPROPAGATIONNAME<>''SAT'' and REFPROPAGATIONNAME<>''RPT'') or refpropagationname is null)');

    //Tail
    SQL.Add('group by ai.name');

//    SQL.SaveToFile('test.sql');

    Open;
    RowCnt:=0;

    while not(Eof) do begin
      inc(RowCnt);
      pbProgress.Position:=RowCnt mod pbProgress.Max;
      Square:=trim(Fields.FieldByName('Name').AsString);
      Count:=Fields.FieldByName('Count').AsInteger;
      FirstQ:=Fields.FieldByName('Min').AsDateTime;
      if length(square)=4 then begin
        Lon:=(ord(Square[1])-65-9)*20+(ord(Square[3])-48)*2+1;
        Lat:=(ord(Square[2])-65-9)*10+(ord(Square[4])-48)+0.5;
        OutStr:=Format('"%4s"%1s%d%1s%g%1s%g%1s%8s',[Square,#9,Count,#9,Lon,#9,Lat,#9,FormatDateTime('yyyymmdd',FirstQ)]);
        OutLst.Add(OutStr);
      end;
      Next;
    end;
    pbProgress.Position:=pbProgress.Max;
    Close;
  end;

  OutLst.SaveToFile(edtExportFile.Text);
  OutLst.Free;

  dmSettings.QGISSquareListPath:=edtExportFile.Text;
end;


procedure TdlgExportSquareList.spdExportFileClick(Sender: TObject);
begin
  if odFilename.Execute then begin
    edtExportFile.Text:=odFilename.FileName;
  end;
end;

procedure TdlgExportSquareList.FormShow(Sender: TObject);

var
  Item : TListItem;
  OpCond : TOperatingCondition;

begin
  edtExportFile.Text:=dmSettings.QGISSquareListPath;

  with dmDataExport.qryOperatingCondition do begin
    Open;
    cbxOperatingCondition.Items.Clear;
    while not(EOF) do begin
      OpCond:=TOperatingCondition.Create;
      OpCond.RefCall:=FieldByName('REFCALL').AsString;
      OpCond.RefOpCall:=FieldByName('REFOPCALL').AsString;
      OpCOnd.RefUsedLocation:=FieldByName('REFUsedLocation_Id').AsInteger;
      cbxOperatingCondition.Items.AddObject(FieldByName('Name').AsString,OpCond);
      Next;
    end;
    Close;
    cbxOperatingCondition.Text:='';
  end;

  with dmDataExport.qryUsedCalls do begin
    Open;
    lvUsedCalls.Clear;
    while not(EOF) do begin
      Item:=lvUsedCalls.Items.Add();
      Item.Caption:=FieldByName('Call').AsString;
      Item.SubItems.Add(FieldByName('Name').AsString);
      Next;
    end;
    Close;
    lvUsedCalls.Columns[0].Width:=-1;
    lvUsedCalls.Columns[1].Width:=-1;
  end;

  with dmDataExport.qryOperators do begin
    Open;
    lvOperatorCalls.Clear;
    while not(EOF) do begin
      Item:=lvOperatorCalls.Items.Add();
      Item.Caption:=FieldByName('OpCall').AsString;
      Item.SubItems.Add(FieldByName('Name').AsString);
      Next;
    end;
    Close;
    lvOperatorCalls.Columns[0].Width:=-1;
    lvOperatorCalls.Columns[1].Width:=-1;
  end;

  with dmDataExport.qryUsedLocs do begin
    Open;
    lvUsedLocations.Clear;
    while not(EOF) do begin
      Item:=lvUsedLocations.Items.Add();
      Item.Caption:=FieldByName('Locator').AsString;
      Item.SubItems.Add(FieldByName('Name').AsString);
      Next;
    end;
    Close;
    lvUsedLocations.Columns[0].Width:=-1;
    lvUsedLocations.Columns[1].Width:=-1;
  end;

  with dmDataExport.qryRefModes do begin
    Open;
    cbxMode.Clear;
    cbxMode.Items.Add('<Alla>');
    while not(EOF) do begin
      cbxMode.Items.Add(FieldByName('REFMODE').AsString);
      Next;
    end;
    Close;
    cbxMode.ItemIndex:=0;
  end;

  with dmDataExport.qryRefProp do begin
    Open;
    cbxProp.Clear;
    cbxProp.Items.Add('<Alla>');
    while not(EOF) do begin
      cbxProp.Items.Add(FieldByName('REFPROPAGATIONNAME').AsString);
      Next;
    end;
    Close;
    cbxProp.ItemIndex:=0;
  end;
end;

procedure TdlgExportSquareList.cbxOperatingConditionSelect(
  Sender: TObject);

var
  OpCond : TOperatingCondition;

begin
  if cbxOperatingCondition.ItemIndex>=0 then begin
    OpCond:=TOperatingCondition(cbxOperatingCondition.Items.Objects[cbxOperatingCondition.ItemIndex]);
    if assigned(OpCond) then begin
      doSelect(lvUsedCalls,OpCond.RefCall);
      doSelect(lvOperatorCalls,OpCond.RefOpCall);

    end;
  end;
end;

procedure TdlgExportSquareList.doSelect(lvView: TListView; Item: string);

var
  i : integer;
  anItem : TListItem;

begin
  for i:=0 to lvView.Items.Count-1 do begin
    anItem:=lvView.Items[i];
    anItem.Checked:=anItem.Caption=Item;
  end;
end;

procedure TdlgExportSquareList.lvUsedLocationsMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

var
  i : integer;

  P : TGeoCoordList;
  R : TGeoCoordList;
  Point : TGeoCoord;
  Circle : TGeoCircle;
  Res : TGeoCoord;
  S : String;
  L : integer;

begin
  L:=0;
  for i:=0 to lvUsedLocations.Items.Count-1 do begin
    if lvUsedLocations.Items[i].Checked then begin
      inc(L);
      SetLength(P,L);
      Point:=TGeoCoord.Create;
      Point.Locator:=lvUsedLocations.Items[i].Caption;
      P[L-1]:=Point;
    end;
  end;
  SetLength(R,0);
  Circle:=Welzl(P,R);

  Res:=TGeoCoord.Create;
  Res.Cartesian:=Circle.CenterC;

  S:=Format('Center: %s Radius: %5.1f km',[Res.Locator,Circle.RadiusC/1000.0]);
  Label1.Caption:=S;

  for i:=0 to length(P)-1 do begin
    P[i].Free;
    P[i]:=nil;
  end;
end;


procedure TdlgExportSquareList.btnAllBandsClick(Sender: TObject);

var
  i : integer;

begin
  for i:=0 to lvBand.Items.Count-1 do begin
    lvBand.Items[i].Checked:=true;
  end;
end;

procedure TdlgExportSquareList.btnAllUsedCallsClick(Sender: TObject);

var
  i : integer;

begin
  for i:=0 to lvUsedCalls.Items.Count-1 do begin
    lvUsedCalls.Items[i].Checked:=true;
  end;
end;

end.
