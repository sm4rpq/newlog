unit ConstellationDlg;

{
  Purpose:
    Dialogue to present satellite constellation.
  Status:
    AOS/LOS should be calculated
    Data for some fields not calculated
    Identification for sats in rose should be added
    Labels for directions/elevations
    Dynamic # of rings in rose depending on diagram size
    Spinboxes for time-adjustment should be moved out to a separate dialogue
    More Statusbar things
    Set other locator not working in real-time mode
  Date:
    2021-05-27
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, ExtCtrls, GroundStation, ovctcmmn, ovctcell,
  ovctcstr, ovctchdr, ovcbase, ovctable, ovctcbef, ovctcsim, StdCtrls, Spin;

type
  TdlgConstellation = class(TForm)
    pbConstellation: TPaintBox;
    Splitter1: TSplitter;
    StatusBar1: TStatusBar;
    PopupMenu1: TPopupMenu;
    Timer1: TTimer;
    itmRealTime: TMenuItem;
    itmQSOTime: TMenuItem;
    OvcTable1: TOvcTable;
    OvcTCColHead1: TOvcTCColHead;
    OvcTCSimpleField1: TOvcTCSimpleField;
    Panel1: TPanel;
    speHourOfs: TSpinEdit;
    itmSetOtherLoc: TMenuItem;
    Label1: TLabel;
    Label2: TLabel;
    speMinOfs: TSpinEdit;
    procedure pbConstellationPaint(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure itmRealTimeClick(Sender: TObject);
    procedure itmQSOTimeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure OvcTable1GetCellData(Sender: TObject; RowNum,
      ColNum: Integer; var Data: Pointer; Purpose: TOvcCellDataPurpose);
    procedure OvcTable1GetCellAttributes(Sender: TObject; RowNum,
      ColNum: Integer; var CellAttr: TOvcCellAttributes);
    procedure itmSetOtherLocClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FStartTime: TDateTime;
    FSat : string;
    FStn1 : TGroundStation;
    FStn2 : TGroundStation;
    FStnDefault : TGroundStation;
    FTable : TStringList;
    FNumberValid : integer;
    FColMap : array[0..19] of integer;
    procedure UpdateSatList;
  public
    procedure setQSOData(Time : TDateTime; Sat : string; Loc1 : string; Loc1Height : double; Loc2 : string);
    { Public declarations }
  end;

var
  dlgConstellation: TdlgConstellation;

implementation

uses DateUtils,SatData,SpaceObject,SatPath,Misc;

{$R *.dfm}

type
  TSatRow = class(TObject)
  private
    Sat : TSpaceObject;
    Name : string[32];
    
    AAz : string[5];
    AEl : string[5];
    AEld : double;
    AAOS : string[8];
    ALOS : string[8];
    ARange : string[8];
    ARRate : string[8];

    BAz : string[5];
    BEl : string[5];
    BEld : double;
    BAOS : string[8];
    BLOS : string[8];
    BRange : string[8];
    BRRate : string[8];

    SSPLat : string[12];
    SSPLon : string[12];
    Height : String[7];
    Track : array[-2..4] of record
                              Az : double;
                              El : double;
                            end;
  public
    Constructor create;
    Destructor destroy;
  end;

  TFieldOption = record
    Name     : string;
    Width    : integer;
    Priority : integer;
  end;

const
  TableFields : array[0..19] of TFieldOption =
    (
     (Name: 'Satellite'; Width: 50; Priority: 1),  //  0
     (Name: 'Az';        Width: 44; Priority: 1),  //  1
     (Name: 'El';        Width: 44; Priority: 1),  //  2
     (Name: 'AOS';       Width: 50; Priority: 1),  //  3
     (Name: 'LOS';       Width: 50; Priority: 1),  //  4
     (Name: 'RXDop';     Width: 50; Priority: 2),  //  5
     (Name: 'TXDop';     Width: 50; Priority: 3),  //  6
     (Name: 'Range';     Width: 50; Priority: 4),  //  7
     (Name: '';          Width: 10; Priority: 1),
     (Name: 'Az';        Width: 44; Priority: 1),  //  9
     (Name: 'El';        Width: 44; Priority: 1),  // 10
     (Name: 'AOS';       Width: 50; Priority: 1),  // 11
     (Name: 'LOS';       Width: 50; Priority: 1),  // 12
     (Name: 'RXDop';     Width: 50; Priority: 8),  // 13
     (Name: 'TXDop';     Width: 50; Priority: 8),  // 14
     (Name: 'Range';     Width: 50; Priority: 5),  // 15
     (Name: '';          Width: 10; Priority: 6),
     (Name: 'Lat';       Width: 50; Priority: 7),  // 17
     (Name: 'Lon';       Width: 50; Priority: 7),  // 18
     (Name: 'Hgt';       Width: 50; Priority: 6)   // 19
    );
  MaxPrio = 8;

procedure TdlgConstellation.pbConstellationPaint(Sender: TObject);

var
  C : TPoint;
  S : integer;
  i : integer;
  j : integer;
  B : boolean;
  a : double;

  PosX : integer;
  PosY : integer;
  Plot : boolean;
  r : integer;
  circleCount : integer;

  function El(Elevation : double) : double;
  begin
    Result:=1-Elevation/Pi*2.0;
  end;

  function PX(Azimuth,Elevation : double) : integer;
  begin
    Result:=round(sin(Azimuth)*El(Elevation)*S+C.X);
  end;

  function PY(Azimuth,Elevation : double) : integer;
  begin
    Result:=round(-cos(Azimuth)*El(Elevation)*S+C.Y);
  end;

begin
  pbConstellation.Canvas.Pen.Width:=1;
  pbConstellation.Canvas.Pen.Color:=clBlack;
  C.X:=pbConstellation.ClientRect.BottomRight.X div 2;
  C.Y:=pbConstellation.ClientRect.BottomRight.Y div 2;

  S:=pbConstellation.ClientRect.BottomRight.X;
  if pbConstellation.ClientRect.BottomRight.Y<S then
    S:=pbConstellation.ClientRect.BottomRight.Y;
  S:=S*2 div 5;

  with pbConstellation.Canvas do begin
    Brush.Color:=clWhite;
    Brush.Style:=bsSolid;
    FillRect(Canvas.ClipRect);
    Brush.Style:=bsClear;

    Pen.Color:=clBlack;
    CircleCount:=3;
    if S>220 then CircleCount:=6;
    if S>300 then CircleCount:=9;
    for i:=CircleCount downto 0 do begin
      a:=pi/2*(1-i/CircleCount);
      r:=round(S*El(a));
      Ellipse(C.X-r,C.Y-r,C.X+r,C.Y+r);
    end;
    
    for i:=0 to 12 do begin
      a:=i*pi/6;
      PosX:=PX(a,0);
      PosY:=PY(a,0);
      MoveTo(C.X,C.Y);
      LineTo(PosX,PosY);
    end;
  end;

  for i:=0 to FTable.Count-1 do begin
    with TSatRow(FTable.Objects[i]).Track[0] do begin
      PosX:=PX(Az,El);
      PosY:=PY(Az,El);
      Plot:=El>=0.0;
    end;
    if Plot then begin
      pbConstellation.Canvas.Pen.Width:=1;
      pbConstellation.Canvas.Pen.Color:=clBlack;
      pbConstellation.Canvas.Ellipse(PosX-3,PosY-3,PosX+3,PosY+3);

      B:=false;
      for j:=-2 to 4 do begin
        with TSatRow(FTable.Objects[i]).Track[j] do begin
          PosX:=PX(Az,El);
          PosY:=PY(Az,El);
          Plot:=El>=0.0;
        end;
        if Plot then begin
          if not(B) then begin
            pbConstellation.Canvas.MoveTo(PosX,PosY);
            B:=True;
          end else begin
            if j>0 then begin
              pbConstellation.Canvas.Pen.Width:=2;
              pbConstellation.Canvas.Pen.Color:=clBlue;
            end else begin
              pbConstellation.Canvas.Pen.Width:=1;
              pbConstellation.Canvas.Pen.Color:=clRed;
            end;
            pbConstellation.Canvas.LineTo(PosX,PosY);
          end;
        end;
      end;


    end;
  end;
end;

procedure TdlgConstellation.Timer1Timer(Sender: TObject);

var
  i     : integer;
  G1,G2 : TGroundStation;
  P : TSatPath;
  T : TDateTime;
  R : TSatRow;
  Ofs : integer;
  Time : TSystemTime;

begin
  //Recalculate
  if itmQSOTime.Checked then begin
    if Assigned(FStn1) then
      G1:=FStn1
    else
      G1:=FStnDefault;
    G2:=FStn2;
  end else begin
    G1:=FStnDefault;
    G2:=nil;
//    G2:=FStn2;
  end;

  P:=TSatPath.Create;

  GetSystemTime(Time);

  if itmRealTime.Checked then
    T:=EncodeDateTime(Time.wYear,TIme.wMonth,Time.wDay,Time.wHour,time.wMinute,time.wSecond,time.wMilliseconds)
  else
    T:=FStartTime+speHourOfs.Value/24.0+speMinOfs.Value/1440.0;

  StatusBar1.Panels[0].Text:=FormatDateTime('yyyy-mmm-dd hh:nn:ss',T);

  try
    for i:=0 to FTable.Count-1 do begin
      R:=TSatRow(FTable.Objects[i]);
      if assigned(R.Sat) and (R.Sat.Age>1.0) then
        R.Sat.Load(T);
      if R.Sat.Valid then begin
        P.setStation(G1);
        P.setSat(R.Sat);

        (* Calculate track for graph *)
        for ofs:=-2 to 4 do begin
          R.Sat.setTime(T+ofs/1440.0);
          P.calculate;
          R.Track[ofs].Az:=P.Azimuth;
          R.Track[ofs].El:=P.Elevation;
        end;

        R.SSPLat:=FormatFloat('0.0',R.Sat.SLat);
        R.SSPLon:=FormatFloat('0.0',R.Sat.SLon);
        R.Height:=FormatFloat('0',R.Sat.Height/1000);

        (* Calculate data for table *)
        R.Sat.setTime(T);
        P.calculate;
        R.AAz:=FormatFloat('0.0',RadToDeg(P.Azimuth));
        R.AEl:=FormatFloat('0.0',RadToDeg(P.Elevation));
        R.AEld:=P.Elevation;
        R.ARange:=FormatFloat('0',P.Range/1000);

        if Assigned(G2) Then begin
          P.setStation(G2);
          P.calculate;
          R.BAz:=FormatFloat('0.0',RadToDeg(P.Azimuth));
          R.BEl:=FormatFloat('0.0',RadToDeg(P.Elevation));
          R.BEld:=P.Elevation;
          R.BRange:=FOrmatFloat('0',P.Range/1000);
        end else begin
          R.BAz:='';
          R.BEl:='';
          R.BEld:=-1.0;
          R.BRange:='';
        end;
      end else begin
        R.AAz:='';
        R.AEl:='';
        R.BAz:='';
        R.BEl:='';
      end;
      OvcTable1.Repaint;
      pbConstellation.Repaint;
    end;

  finally
    P.Free;
  end;
end;

procedure TdlgConstellation.itmRealTimeClick(Sender: TObject);
begin
  itmRealTime.Checked:=not(itmRealTime.Checked);
  itmQSOTime.Checked:=not(itmRealTime.Checked);
  UpdateSatList;
end;

procedure TdlgConstellation.itmQSOTimeClick(Sender: TObject);
begin
  itmQSOTime.Checked:=not(itmQSOTime.Checked);
  itmRealTime.Checked:=not(itmQSOTime.Checked);
  UpdateSatList;
end;

procedure TdlgConstellation.FormShow(Sender: TObject);
begin
  itmRealTime.Checked:=true;
  UpdateSatList;
end;

procedure TdlgConstellation.UpdateSatList;

var
  i    : integer;
  R : TSatRow;

begin
  for i:=0 to FTable.Count-1 do
    TSatRow(FTable.Objects[i]).Free;
  FTable.Clear;

  with dmSatData.qryActiveSats do begin
    if itmRealTime.Checked then
      ParamByName('thedate').AsDateTime:=now
    else
      ParamByName('thedate').AsDateTime:=FStartTime;
    Open;
    while not(EOF) do begin
      if FieldByName('SATELLITE_ID').AsInteger>0 then begin
        R:=TSatRow.Create;
        FTable.AddObject(FieldByName('NAME').AsString,R);
        R.Sat.SatNumber:=FieldByName('SATELLITE_ID').AsInteger;
        R.Name:=FieldByName('NAME').AsString;
        R.AEld:=-1.0;
      end;
      Next;
    end;
    Close;
  end;
  OvcTable1.RowLimit:=FTable.Count+1;

  if assigned(FStn1) and itmQSOTime.Checked then
    StatusBar1.Panels[1].Text:=FStn1.Locator
  else
    StatusBar1.Panels[1].Text:='';

  if assigned(FStn2) and itmQSOTime.Checked then
    StatusBar1.Panels[2].Text:=FStn2.Locator
  else
    StatusBar1.Panels[2].Text:='';
end;

procedure TdlgConstellation.setQSOData(Time: TDateTime; Sat, Loc1 : string; Loc1Height : double;
  Loc2: string);

var
  La,Lo : double;
  
begin
  FStartTime:=Time;

  if Loc1<>'' then begin
    if not(Assigned(FStn1)) then
      FStn1:=TGroundStation.Create;
    FStn1.Locator:=Loc1;
    FStn1.GetGeodetic(La,Lo);
    FStn1.setLocation(La,Lo,trunc(Loc1Height));
  end else if Assigned(FStn1) then begin
    FStn1.Free;
    FStn1:=nil;
  end;

  if Loc2<>'' then begin
    if not(Assigned(FStn2)) then
      FStn2:=TGroundStation.Create;
    FStn2.Locator:=Loc2;
    FStn2.GetGeodetic(La,Lo);
    FStn2.setLocation(La,Lo,0);
  end else if Assigned(FStn2) then begin
    FStn2.Free;
    FStn2:=nil;
  end;

  FSat:=Sat;

  if itmQSOTime.Checked then begin
    UpdateSatList;
    Timer1Timer(self);
  end;

  OvcTable1.Repaint;
end;

procedure TdlgConstellation.FormCreate(Sender: TObject);
begin
  FStn1:=nil;
  FStn2:=nil;
  FStnDefault:=TGroundStation.Create;
  FStnDefault.setLocation(59.232321,14.592153,100);
  FTable:=TStringList.Create;
  FNumberValid:=0;
end;

procedure TdlgConstellation.FormDestroy(Sender: TObject);
begin
  FStn1.Free;
  FStn2.Free;
  FStnDefault.Free;
end;

{ TSatRow }

constructor TSatRow.create;
begin
  Sat:=TSpaceObject.Create;
end;

destructor TSatRow.destroy;
begin
  Sat.Free;
end;

procedure TdlgConstellation.OvcTable1GetCellData(Sender: TObject; RowNum,
  ColNum: Integer; var Data: Pointer; Purpose: TOvcCellDataPurpose);

begin
  if RowNum>FTable.Count then
    exit;

  if RowNum=0 then
    exit;

  with TSatRow(FTable.Objects[RowNum-1]) do begin
    case FColMap[ColNum] of
       0 : Data:=@Name;
       1 : Data:=@AAz;
       2 : Data:=@AEl;
//       3 : Data:=@.. AOS
//       4 : Data:=@.. LOS
//       5 : Data:=@.. RXDoppler
//       6 : Data:=@.. TXDoppler
       7 : Data:=@ARange;
       // Empty
       9 : Data:=@BAz;
      10 : Data:=@BEl;
//      11 : Data:=@.. AOS
//      12 : Data:=@.. LOS
//      13 : Data:=@.. RXDoppler
//      14 : Data:=@.. TXDoppler
      15 : Data:=@BRange;
      // Empty
      17 : Data:=@SSPLat;
      18 : Data:=@SSPLon;
      19 : Data:=@Height;
    end;
  end;
end;

procedure TdlgConstellation.OvcTable1GetCellAttributes(Sender: TObject;
  RowNum, ColNum: Integer; var CellAttr: TOvcCellAttributes);
begin
  if RowNum>FTable.Count then
    exit;

  if RowNum=0 then
    exit;

  case FColMap[ColNum] of
    0 : begin
          if TSatRow(FTable.Objects[RowNum-1]).Name=FSat then
            CellAttr.caColor:=clLime;
          if (TSatRow(FTable.Objects[RowNum-1]).AEld>=0.0) and (TSatRow(FTable.Objects[RowNum-1]).BEld>=0.0)  then
            CellAttr.caFont.Style:=[fsBold];
        end;
    2 : begin
          if TSatRow(FTable.Objects[RowNum-1]).AEld>=0.0 then
            CellAttr.caFont.Style:=[fsBold];
        end;
    10 : begin
          if TSatRow(FTable.Objects[RowNum-1]).BEld>=0.0 then
            CellAttr.caFont.Style:=[fsBold];
        end;
  end;
end;

procedure TdlgConstellation.itmSetOtherLocClick(Sender: TObject);

var
  NewLoc : string;
  La,Lo : double;

begin
  if Assigned(FStn2) then
    NewLoc:=InputBox('Enter other locator','Locator',FStn2.Locator)
  else
    NewLoc:=InputBox('Enter other locator','Locator','');
  NewLoc:=UpperCase(NewLoc);
  if NewLoc<>'' then begin
    if not(Assigned(FStn2)) then
      FStn2:=TGroundStation.Create;
    FStn2.Locator:=NewLoc;
    FStn2.GetGeodetic(La,Lo);
    FStn2.setLocation(La,Lo,0);
  end;
end;

procedure TdlgConstellation.FormResize(Sender: TObject);

var
  Prio : integer;
  FitsPrio : integer;
  TotalWidth : integer;
  Column : integer;
  ColumnCount : integer;

begin
  Prio:=1;
  FitsPrio:=1;
  TotalWidth:=0;
  repeat
    for Column:=Low(TableFields) to High(TableFields) do
      if TableFields[Column].Priority=Prio then
        TotalWidth:=TotalWidth+TableFields[Column].Width;
    if TotalWidth<ClientWidth-4 then
      FitsPrio:=Prio;
    inc(Prio);
  until (TotalWidth>ClientWidth) or (Prio=MaxPrio+1);

  ColumnCount:=0;
  for Column:=Low(TableFields) to High(TableFields) do
    if TableFields[Column].Priority<=FitsPrio then begin
      FColMap[ColumnCount]:=Column;
      inc(ColumnCount);
    end;

  OvcTable1.ColCount:=ColumnCount;
  OvcTCColHead1.Headings.Clear;
  OvcTCColHead1.Headings.Add(TableFields[0].Name);
  for Column:=1 to ColumnCount-1 do begin
    OvcTable1.Columns[Column].DefaultCell:=OvcTCSimpleField1;
    OvcTable1.Columns[Column].Width:=TableFields[FColMap[Column]].Width;
    OvcTCColHead1.Headings.Add(TableFields[FColMap[Column]].Name);
  end;
end;

end.
