object dlgMainFilter: TdlgMainFilter
  Left = 2350
  Top = 291
  BorderStyle = bsDialog
  Caption = 'Filter'
  ClientHeight = 153
  ClientWidth = 273
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefaultPosOnly
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 8
    Width = 36
    Height = 13
    Caption = 'Callsign'
  end
  object Label2: TLabel
    Left = 16
    Top = 56
    Width = 36
    Height = 13
    Caption = 'Locator'
  end
  object Button1: TButton
    Left = 184
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = Button1Click
  end
  object edtCallsignFilter: TEdit
    Left = 16
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object edtLocatorFilter: TEdit
    Left = 16
    Top = 72
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Button2: TButton
    Left = 16
    Top = 120
    Width = 75
    Height = 25
    Caption = 'Nollst'#228'll'
    TabOrder = 2
    OnClick = Button2Click
  end
end
