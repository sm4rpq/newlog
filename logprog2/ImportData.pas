unit ImportData;

{
  Purpose:
    Datamodule with components used during import
  Status:
    Work in progress
    Some problem with translation of swedish 7S/8S callsigns
    Managers not handled on update
  Date:
    2021-03-28
}

interface

uses
  SysUtils, Classes, IBSQL, DB, IBCustomDataSet, IBQuery, Connection;

type
  TContactedStation = record
    RefBaseCall : string;
    RefInstance : integer;
  end;
  TdmDataImport = class(TDataModule)
    qryConditions: TIBQuery;
    qryConditionsOPERATINGCONDITION_ID: TIntegerField;
    qryConditionsNAME: TIBStringField;
    qryConditionsREFCALL: TIBStringField;
    qryConditionsREFOPCALL: TIBStringField;
    qryConditionsREFUSEDEQ_ID: TIntegerField;
    qryConditionsREFUSEDLOCATION_ID: TIntegerField;
    qryFindContactedStation: TIBQuery;
    qryInsertQSO: TIBSQL;
    qryInsertContactedStation: TIBSQL;
    qryInsertCSInteraction: TIBSQL;
    qryInsertQConf: TIBSQL;
    qryInsertSntConf: TIBSQL;
    qryInsertRcvConf: TIBSQL;
    qryAwardItem: TIBQuery;
    qryInsertAwardItem: TIBSQL;
    qryInsertAwardCredit: TIBSQL;
    qryCondition: TIBQuery;
    qryConditionOPERATINGCONDITION_ID: TIntegerField;
    qryConditionNAME: TIBStringField;
    qryConditionREFCALL: TIBStringField;
    qryConditionREFOPCALL: TIBStringField;
    qryConditionREFUSEDEQ_ID: TIntegerField;
    qryConditionREFUSEDLOCATION_ID: TIntegerField;
    qryLocation: TIBQuery;
    qryLocationUSEDLOCATION_ID: TIntegerField;
    qryLocationREFCALL: TIBStringField;
    qryLocationREFUSEDLOCATION_ID: TIntegerField;
    qryLocationREFUSEDEQ_ID: TIntegerField;
    qryLocationREFOPCALL: TIBStringField;
    qryFindCSInteraction: TIBQuery;
    qryUpdateContactedStation: TIBSQL;
    qryFindSat: TIBQuery;
  private
    { Private declarations }
    function RegisterQSL(QSO_Id : integer; Service : string) : integer ;
  public
    { Public declarations }
    procedure RegisterAwardCredit(Award : integer; Credit : string; QSO_Id : integer);
    procedure RegisterReceivedQSL(QSO_Id : integer; Service : string);
    procedure RegisterSentQSL(QSO_Id : integer; Service : String);
    function RegisterContactedStation(Call : string; InputName : string; Manager : string) : TContactedStation;
  end;

  function StripCall(FullCall : String) : String;

var
  dmDataImport: TdmDataImport;

implementation

{$R *.dfm}

{ TdmDataImport }

procedure TdmDataImport.RegisterAwardCredit(Award: integer; Credit: string; QSO_Id : integer);

var
  CreditId : integer;

begin
  CreditId:=0;
  with qryAwardItem do begin
    ParamByName('REFAward_Id').AsInteger:=Award;
    ParamByName('Name').AsString:=Credit;
    Open;
    if not(EOF) then begin
      CreditId:=FieldByName('awarditem_id').AsInteger;
    end;
    Close;
  end;
  if (CreditId=0) then begin
    CreditId:=dmConnection.Get_Unique_Id;
    with qryInsertAwardItem do begin
      ParamByName('AwardItem_Id').AsInteger:=CreditId;
      ParamByName('Name').AsString:=Credit;
      ParamByName('REFAward_Id').AsInteger:=Award;
      ExecQuery;
    end;
  end;
  with qryInsertAwardCredit do begin
    ParamByName('ASOQSO_Id').AsInteger:=QSO_Id;
    ParamByName('ASOAwardItem_Id').AsInteger:=CreditId;
    ExecQuery;
  end;
end;

function StripCall(FullCall : String) : String;

var
  TempCall : String[10];
  p        : integer;

begin
  p:=Pos('/',FullCall);
  if p>0 then begin
    if p>4 then
      TempCall:=Copy(FullCall,1,p-1)
    else begin
      TempCall:=Copy(FullCall,p+1,Length(FullCall)-p);
      p:=Pos('/',TempCall);
      if p>0 then
        TempCall:=Copy(TempCall,1,p-1);
    end;
  end else
    TempCall:=FullCall;

  Result:=TempCall;
end;

function TdmDataImport.RegisterContactedStation(Call,
  InputName: string; Manager : string): TContactedStation;

  procedure NameSplit(FullName : string; var Name : string; var Nick: string);

  var
    ParPos : integer;

  begin
    ParPos:=Pos('(',FullName);
    if ParPos>0 then begin
      Nick:=trim(Copy(FullName,1,ParPos-1));
      Name:=trim(Copy(FullName,ParPos+1,Length(FullName)-ParPos-1));
    end else begin
      Name:=FullName;
      Nick:='';
    end;
  end;

var
  BaseCall : string;
  Name     : string;
  Nick     : string;
  MaxInstance : integer;
  DbName   : string;
  DbNick   : string;

begin
  //Get just the callsign. This is the key into then ContactedStation table
  BaseCall:=StripCall(Call);  //SM3/DL7XXX/M -> DL7XXX
  Result.RefBaseCall:=BaseCall;

  //Split name into parts. (Uses paranthesis to include real name vs nick exchanged during qso)
  NameSplit(InputName,Name,Nick);

  //Query for ContactedStations matching basecall
  Result.RefInstance:=0;
  MaxInstance:=0;

  qryFindContactedStation.ParamByName('BaseCall').AsString:=BaseCall;
  qryFindContactedStation.Open;

  //Loop over all existing instances of basecall
  while not(qryFindContactedStation.EOF) and (Result.RefInstance=0) do begin
    //Get the name and nick from database. Null-safe
    if not(qryFindContactedStation.FieldByName('Name').IsNull) then
      DbName:=qryFindContactedStation.FieldByName('Name').AsString
    else
      DbName:='';

    if not(qryFindContactedStation.FieldByName('Nick').IsNull) then
      DbNick:=qryFindContactedStation.FieldByName('Nick').AsString
    else
      DbNick:='';

    //Match conditions:
    // Nick/Name in database blank => Anything should match
    // Nick/Name investigated blank => Anything should match
    // Name=DbName => Possible match
    //  Nick=DbNick => Match
    //  DbNick or Nick Blank, match

    if (Name+Nick='') or
       (DbName+DbNick='') or
       ((DbName=Name) and
        ((DbNick=Nick) or
         (Nick='') or
         (DbNick='')) //Blank or matching nick should match
       ) then begin
       //Found a matching record
       Result.RefInstance:=qryFindContactedStation.FieldByName('Instance').AsInteger;
     end;
     MaxInstance:=qryFindContactedStation.FieldByName('Instance').AsInteger;
    qryFindContactedStation.Next;
  end;
  qryFindContactedStation.Close;

  //If not found, insert a new one
  if Result.RefInstance=0 then begin
    inc(MaxInstance);
    Result.RefInstance:=MaxInstance;
    qryInsertContactedStation.ParamByName('BaseCall').AsString:=BaseCall;
    qryInsertContactedStation.ParamByName('Instance').AsInteger:=MaxInstance;
    qryInsertContactedStation.ParamByName('Name').AsString:=Name;
    qryInsertContactedStation.ParamByName('Nick').AsString:=Nick;
    qryInsertContactedStation.ParamByName('Manager').AsString:=Manager; // Manager
    qryInsertContactedStation.ExecQuery;
  end else begin
    //May need to update. Depends on:
    //If dbNick='' and Nick<>'' or
    //dbName='' and Name<>''
    if ((dbName='') and (Name<>'')) or
       ((dbNick='') and (Nick<>'')) then begin
      qryUpdateContactedStation.ParamByName('BaseCall').AsString:=Result.RefBaseCall;
      qryUpdateContactedStation.ParamByName('Instance').AsInteger:=Result.RefInstance;
      if Name='' then
        qryUpdateContactedStation.ParamByName('Name').AsString:=dbName
      else
        qryUpdateContactedStation.ParamByName('Name').AsString:=Name;
      if Nick='' then
        qryUpdateContactedStation.ParamByName('Nick').AsString:=dbNick
      else
        qryUpdateContactedStation.ParamByName('Nick').AsString:=Nick;
      qryUpdateContactedStation.ExecQuery;
    end;
  end;
end;

function TdmDataImport.RegisterQSL(QSO_Id: integer; Service: string) : integer;

var
  Has_CSI : boolean;
  cs_id : integer;
  conf_id : integer;

begin
  Has_CSI:=False;

  if Service='B' then
    cs_id:=0
  else if Service='D' then
    Cs_id:=1
  else
    cs_id:=-1;

  with qryFindCSInteraction do begin
    ParamByName('QSO_Id').AsInteger:=QSO_id;
    ParamByName('cs_id').AsInteger:=cs_id;
    Open;
    if not(eof) then begin
      Has_CSI:=true;
    end;
    Close;
  end;

  if cs_id>=0 then begin
    if not(Has_CSI) then begin
      with dmDataImport.qryInsertCSInteraction do begin
        paramByName('ASOQSO_Id').AsInteger:=QSO_Id;
        ParamByName('ASOConfirmationService_Id').AsInteger:=cs_id;
        ExecQuery;
      end;
    end;

    conf_id:=dmConnection.Get_Unique_Id;
    with dmDataImport.qryInsertQConf do begin
      ParamByName('ASOQSO_Id').AsInteger:=QSO_Id;
      ParamByName('ASOConfirmationService_Id').AsInteger:=cs_id;
      ParamByName('Confirmation_Id').AsInteger:=conf_id;
      ParamByName('Number').AsInteger:=1;
      ParamByName('RegisteredDate').AsDateTime:=Now;
      ExecQuery;
    end;
    result:=conf_id;
  end else
    result:=0;
end;

procedure TdmDataImport.RegisterReceivedQSL(QSO_Id: integer;
  Service: string);

var
  conf_id : integer;

begin
  conf_id:=RegisterQSL(QSO_Id,Service);
  if conf_id>0 then
    with dmDataImport.qryInsertRcvConf do begin
      paramByName('GENConfirmation_Id').AsInteger:=conf_id;
      paramByName('REFQSL_RCVD').AsString:='Y';
      ExecQuery;
    end;
end;

procedure TdmDataImport.RegisterSentQSL(QSO_Id: integer; Service: String);

var
  conf_id : integer;

begin
  conf_id:=RegisterQSL(QSO_Id,Service);
  if conf_id>0 then
    with dmDataImport.qryInsertSntConf do begin
      paramByName('GENConfirmation_Id').AsInteger:=conf_id;
      paramByName('REFQSL_SENT').AsString:='Y';
      paramByName('STILLVALID').AsString:='T';
      ExecQuery;
    end;
end;

end.
