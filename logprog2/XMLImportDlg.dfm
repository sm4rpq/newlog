object dlgXMLImport: TdlgXMLImport
  Left = 2695
  Top = 402
  Width = 551
  Height = 364
  Caption = 'dlgXMLImport'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 8
    Top = 8
    Width = 51
    Height = 13
    Caption = 'File to load'
  end
  object lblImport: TLabel
    Left = 248
    Top = 56
    Width = 29
    Height = 13
    Caption = 'Signal'
  end
  object btnSelect: TSpeedButton
    Left = 512
    Top = 24
    Width = 23
    Height = 22
    Caption = '...'
    OnClick = btnSelectClick
  end
  object btnClose: TButton
    Left = 464
    Top = 304
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 0
    OnClick = btnCloseClick
  end
  object edtImportPath: TEdit
    Left = 8
    Top = 24
    Width = 497
    Height = 21
    TabOrder = 1
    Text = 'D:\delphprj\newlog\vqtoxml\diverse.xml'
    OnChange = edtImportPathChange
  end
  object btnExecute: TButton
    Left = 8
    Top = 80
    Width = 75
    Height = 25
    Caption = 'Execute'
    Enabled = False
    TabOrder = 2
    OnClick = btnExecuteClick
  end
  object pbProgress: TProgressBar
    Left = 192
    Top = 80
    Width = 150
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 3
  end
  object btnRollback: TButton
    Left = 8
    Top = 144
    Width = 161
    Height = 25
    Caption = 'Rollback'
    Enabled = False
    TabOrder = 4
  end
  object btnCommit: TButton
    Left = 488
    Top = 144
    Width = 51
    Height = 25
    Caption = 'Commit'
    Enabled = False
    TabOrder = 5
  end
  object OpenDialog1: TOpenDialog
    Filter = '*.xml'
    Left = 512
    Top = 56
  end
end
