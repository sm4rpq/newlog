object dlgStatSquare: TdlgStatSquare
  Left = 2290
  Top = 169
  BorderStyle = bsDialog
  Caption = 'Rutstatistik'
  ClientHeight = 590
  ClientWidth = 833
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDefaultPosOnly
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnClose: TButton
    Left = 752
    Top = 552
    Width = 75
    Height = 25
    Caption = 'St'#228'ng'
    ModalResult = 1
    TabOrder = 0
  end
  object rgCallsigns: TRadioGroup
    Left = 8
    Top = 8
    Width = 185
    Height = 121
    Caption = 'Signaler'
    ItemIndex = 0
    Items.Strings = (
      'B'#229'da signalerna'
      'SM4RPP'
      'SM4RPQ')
    TabOrder = 1
    OnClick = recalcStat
  end
  object rgPlace: TRadioGroup
    Left = 200
    Top = 8
    Width = 185
    Height = 121
    Caption = 'Platser'
    ItemIndex = 1
    Items.Strings = (
      'Alla platser'
      'Hemma'
      'Stugan')
    TabOrder = 2
    OnClick = recalcStat
  end
  object rgFrequency: TRadioGroup
    Left = 392
    Top = 8
    Width = 185
    Height = 121
    Caption = 'Frekvenser'
    ItemIndex = 2
    Items.Strings = (
      'Alla band'
      'Kortv'#229'g'
      '6m'
      '2m'
      '70cm'
      '23cm')
    TabOrder = 3
    OnClick = recalcStat
  end
  object rgPropagation: TRadioGroup
    Left = 584
    Top = 8
    Width = 185
    Height = 121
    Caption = 'V'#229'gutbredning'
    ItemIndex = 1
    Items.Strings = (
      'Allt'
      'Ej repeater eller satellit'
      'Aurora'
      'Satellit'
      'Meteorscatter')
    TabOrder = 4
    OnClick = recalcStat
  end
  object PageControl1: TPageControl
    Left = 8
    Top = 136
    Width = 817
    Height = 433
    ActivePage = Textrapport
    TabIndex = 0
    TabOrder = 5
    object Textrapport: TTabSheet
      Caption = 'Textrapport'
      ImageIndex = 1
      object redStat: TRichEdit
        Left = 0
        Top = 0
        Width = 809
        Height = 405
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
    object Karta: TTabSheet
      Caption = 'Karta'
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 809
        Height = 405
        Align = alClient
      end
    end
  end
end
