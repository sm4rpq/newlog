unit MainFilterDlg;

{
  Purpose:
    Provide means to set filters on what to list in main window.
    Currently only supports callsign and locator
  Status:
    Work in progress

    TODOs:
      More filter options
      - Name
      - Date
      - Band
      - Mode
      - Propagation
      - Award items
  Date:
    2021-03-28
}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TdlgMainFilter = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    edtCallsignFilter: TEdit;
    Label2: TLabel;
    edtLocatorFilter: TEdit;
    Button2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    FCallsignFilter: string;
    FLocatorFilter: string;
    FFilterSet: boolean;
    { Private declarations }
  public
    { Public declarations }
    property CallsignFilter : string read FCallsignFilter;
    property LocatorFilter : string read FLocatorFilter;
    property FilterSet : boolean read FFilterSet;
    procedure Clear;
  end;

var
  dlgMainFilter: TdlgMainFilter;

implementation

{$R *.dfm}

procedure TdlgMainFilter.Button1Click(Sender: TObject);
begin
  FFilterSet:=False;
  FCallsignFilter:='';
  if edtCallsignFilter.Text<>'' then begin
    FCallsignFilter:='q.Call like '''+uppercase(trim(edtCallsignFilter.Text))+'%''';
    FFilterSet:=True;
  end;
  FLocatorFilter:='';
  if edtLocatorFilter.Text<>'' then begin
    FLocatorFilter:='q.Locator like '''+uppercase(trim(edtLocatorFilter.Text))+'%''';
    FFilterSet:=True;
  end;
end;

procedure TdlgMainFilter.Clear;
begin
  FCallsignFilter:='';
  edtCallsignFilter.Text:='';
  FLocatorFilter:='';
  edtLocatorFilter.Text:='';
  FFilterSet:=False;
end;

procedure TdlgMainFilter.FormShow(Sender: TObject);
begin
 edtCallsignFilter.SetFocus;
 //Should probably select the content of existing entry
end;

procedure TdlgMainFilter.Button2Click(Sender: TObject);
begin
  Clear;
end;

end.
