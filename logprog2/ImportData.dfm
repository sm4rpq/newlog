object dmDataImport: TdmDataImport
  OldCreateOrder = False
  Left = 2912
  Top = 163
  Height = 640
  Width = 529
  object qryConditions: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from operatingcondition opc')
    Left = 40
    Top = 16
    object qryConditionsOPERATINGCONDITION_ID: TIntegerField
      FieldName = 'OPERATINGCONDITION_ID'
      Origin = 'OPERATINGCONDITION.OPERATINGCONDITION_ID'
      Required = True
    end
    object qryConditionsNAME: TIBStringField
      FieldName = 'NAME'
      Origin = 'OPERATINGCONDITION.NAME'
      Size = 32
    end
    object qryConditionsREFCALL: TIBStringField
      FieldName = 'REFCALL'
      Origin = 'OPERATINGCONDITION.REFCALL'
      Required = True
    end
    object qryConditionsREFOPCALL: TIBStringField
      FieldName = 'REFOPCALL'
      Origin = 'OPERATINGCONDITION.REFOPCALL'
      Required = True
    end
    object qryConditionsREFUSEDEQ_ID: TIntegerField
      FieldName = 'REFUSEDEQ_ID'
      Origin = 'OPERATINGCONDITION.REFUSEDEQ_ID'
      Required = True
    end
    object qryConditionsREFUSEDLOCATION_ID: TIntegerField
      FieldName = 'REFUSEDLOCATION_ID'
      Origin = 'OPERATINGCONDITION.REFUSEDLOCATION_ID'
      Required = True
    end
  end
  object qryFindContactedStation: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from  CONTACTEDSTATION '
      'where BASECALL=:basecall'
      'order by instance')
    Left = 56
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'basecall'
        ParamType = ptUnknown
      end>
  end
  object qryInsertQSO: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'insert '
      ' into QSO('
      '  qso_id,CALL, COMMENT, '
      '  COMPLETE, ENDTIME, FREQUENCY_RX, FREQUENCY_TX,'
      ' LOCATOR, NOTE, O_ANT, O_PWR, '
      ' PAPERLOGREF, POWER, QTH, '
      ' REFBASECALL, REFDXCC_ID, REFINSTANCE, '
      ' REFMODE, REFCALL, REFOPCALL, REFUSEDEQ_ID, REFUSEDLOCATION_ID,'
      ' REFPROPAGATIONNAME, REFCONTEST_ID,'
      
        ' RPT_R, RPT_S, SERIAL_R, SERIAL_S, STARTTIME, SAT_MODE, REFSATEL' +
        'LITE_ID'
      ') values ('
      ':QSO_Id,:CALL, :COMMENT, '
      
        ':COMPLETE, :ENDTIME, :FREQUENCY_RX, :FREQUENCY_TX, :LOCATOR, :NO' +
        'TE, :O_ANT, :O_PWR, '
      ':PAPERLOGREF, :POWER, :QTH, '
      ':REFBASECALL, :REFDXCC_ID, :REFINSTANCE,'
      
        ':REFMODE, :REFCALL, :REFOPCALL, :REFUSEDEQ_ID, :REFUSEDLOCATION_' +
        'ID, :REFPROPAGATIONNAME, :REFCONTEST_ID,'
      
        ':RPT_R, :RPT_S, :SERIAL_R, :SERIAL_S, :STARTTIME, :SAT_MODE,:REF' +
        'SATELLITE_ID'
      ');')
    Transaction = dmConnection.IBTransaction1
    Left = 56
    Top = 296
  end
  object qryInsertContactedStation: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'insert into CONTACTEDSTATION('
      'BASECALL, INSTANCE, MANAGER, NAME, NICK)'
      'values '
      '(:BASECALL, :INSTANCE, :MANAGER, :NAME, :NICK)')
    Transaction = dmConnection.IBTransaction1
    Left = 56
    Top = 176
  end
  object qryInsertCSInteraction: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'insert into CONFIRMATIONSERVICEINTERACTION'
      '(ASOCONFIRMATIONSERVICE_ID, ASOQSO_ID) '
      'values'
      '(:ASOCONFIRMATIONSERVICE_ID, :ASOQSO_ID)')
    Transaction = dmConnection.IBTransaction1
    Left = 232
    Top = 360
  end
  object qryInsertQConf: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'insert into QSOCONFIRMATION'
      
        '(ASOCONFIRMATIONSERVICE_ID, ASOQSO_ID, CONFIRMATION_ID, NUMBER, ' +
        'QSLMESSAGE, REGISTEREDDATE)'
      'values'
      
        '(:ASOCONFIRMATIONSERVICE_ID, :ASOQSO_ID, :CONFIRMATION_ID, :NUMB' +
        'ER, :QSLMESSAGE, :REGISTEREDDATE)')
    Transaction = dmConnection.IBTransaction1
    Left = 328
    Top = 360
  end
  object qryInsertSntConf: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'insert into SENTCONFIRMATION'
      '(GENCONFIRMATION_ID, REFQSL_SENT,STILLVALID) values'
      '(:GENCONFIRMATION_ID, :REFQSL_SENT,:STILLVALID)')
    Transaction = dmConnection.IBTransaction1
    Left = 376
    Top = 416
  end
  object qryInsertRcvConf: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      
        'insert into RECEIVEDCONFIRMATION(GENCONFIRMATION_ID, REFQSL_RCVD' +
        ') values'
      '(:GENCONFIRMATION_ID, :REFQSL_RCVD)')
    Transaction = dmConnection.IBTransaction1
    Left = 280
    Top = 416
  end
  object qryAwardItem: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select awarditem_id from awarditem where refaward_id=:refaward_i' +
        'd and name=:name')
    Left = 440
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'refaward_id'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'name'
        ParamType = ptUnknown
      end>
  end
  object qryInsertAwardItem: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      
        'insert into awardItem values (:AWARDITEM_ID, :NAME, :REFAWARD_ID' +
        ')')
    Transaction = dmConnection.IBTransaction1
    Left = 440
    Top = 72
  end
  object qryInsertAwardCredit: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'insert into RECEIVEDAWARDCREDIT('
      'ASOAWARDITEM_ID, '
      'ASOQSO_Id)'
      ''
      'values (:ASOAWARDITEM_ID, :ASOQSO_ID)')
    Transaction = dmConnection.IBTransaction1
    Left = 440
    Top = 136
  end
  object qryCondition: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      
        'select * from operatingcondition opc where operatingcondition_id' +
        '=:opc_id')
    Left = 128
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'opc_id'
        ParamType = ptUnknown
      end>
    object qryConditionOPERATINGCONDITION_ID: TIntegerField
      FieldName = 'OPERATINGCONDITION_ID'
      Origin = 'OPERATINGCONDITION.OPERATINGCONDITION_ID'
      Required = True
    end
    object qryConditionNAME: TIBStringField
      FieldName = 'NAME'
      Origin = 'OPERATINGCONDITION.NAME'
      Size = 32
    end
    object qryConditionREFCALL: TIBStringField
      FieldName = 'REFCALL'
      Origin = 'OPERATINGCONDITION.REFCALL'
      Required = True
    end
    object qryConditionREFOPCALL: TIBStringField
      FieldName = 'REFOPCALL'
      Origin = 'OPERATINGCONDITION.REFOPCALL'
      Required = True
    end
    object qryConditionREFUSEDEQ_ID: TIntegerField
      FieldName = 'REFUSEDEQ_ID'
      Origin = 'OPERATINGCONDITION.REFUSEDEQ_ID'
      Required = True
    end
    object qryConditionREFUSEDLOCATION_ID: TIntegerField
      FieldName = 'REFUSEDLOCATION_ID'
      Origin = 'OPERATINGCONDITION.REFUSEDLOCATION_ID'
      Required = True
    end
  end
  object qryLocation: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from operatingcondition oc,usedlocation ul where '
      '  oc.refusedlocation_id=ul.usedlocation_id and'
      '  ul.locator like :usedloc and'
      '  oc.refcall=:usedcall')
    Left = 168
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'usedloc'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'usedcall'
        ParamType = ptUnknown
      end>
    object qryLocationUSEDLOCATION_ID: TIntegerField
      FieldName = 'USEDLOCATION_ID'
      Origin = 'USEDLOCATION.USEDLOCATION_ID'
      Required = True
    end
    object qryLocationREFCALL: TIBStringField
      FieldName = 'REFCALL'
      Origin = 'OPERATINGCONDITION.REFCALL'
      Required = True
    end
    object qryLocationREFUSEDLOCATION_ID: TIntegerField
      FieldName = 'REFUSEDLOCATION_ID'
      Origin = 'OPERATINGCONDITION.REFUSEDLOCATION_ID'
      Required = True
    end
    object qryLocationREFUSEDEQ_ID: TIntegerField
      FieldName = 'REFUSEDEQ_ID'
      Origin = 'OPERATINGCONDITION.REFUSEDEQ_ID'
      Required = True
    end
    object qryLocationREFOPCALL: TIBStringField
      FieldName = 'REFOPCALL'
      Origin = 'OPERATINGCONDITION.REFOPCALL'
      Required = True
    end
  end
  object qryFindCSInteraction: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select csi.* from CONFIRMATIONSERVICEINTERACTION csi'
      
        'where csi.ASOQSO_ID=:qso_id and csi.asoconfirmationservice_id=:c' +
        's_id')
    Left = 224
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'qso_id'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'cs_id'
        ParamType = ptUnknown
      end>
  end
  object qryUpdateContactedStation: TIBSQL
    Database = dmConnection.IBDatabase1
    ParamCheck = True
    SQL.Strings = (
      'update contactedstation'
      'set nick=:nick, name=:name'
      'where basecall=:basecall and instance=:instance')
    Transaction = dmConnection.IBTransaction1
    Left = 56
    Top = 232
  end
  object qryFindSat: TIBQuery
    Database = dmConnection.IBDatabase1
    Transaction = dmConnection.IBTransaction1
    BufferChunks = 1000
    CachedUpdates = False
    SQL.Strings = (
      'select * from satellite where name like :name')
    Left = 56
    Top = 432
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'name'
        ParamType = ptUnknown
      end>
  end
end
