
create domain UniqueId INTEGER NOT NULL;

create domain Name VARCHAR(32);

create domain Callsign VARCHAR(20);

create domain Comment VARCHAR(120);

create domain Locator VARCHAR(12);

create domain UTCDateTime TIMESTAMP;

create domain Boolean CHAR(1) CHECK (VALUE IN ('T','F'));

create domain ReportString VARCHAR(6);

create domain SatMode VARCHAR(4);

/*****************************************************************************/
/*
This is what it's all about. For every contact held there will be one instance of this class.

*/
Create table QSO (
        

/* Ordinary attributes */
	QSO_Id UniqueId,
        StartTime UTCDateTime,
        EndTime UTCDateTime,
        Call Callsign,
        Power Real,
        Frequency_TX Real,
        Frequency_RX Real,
        Rpt_S ReportString,
        Rpt_R ReportString,
        Locator Locator,
        QTH Name,
        Serial_S ReportString,
        Serial_R ReportString,
        Complete Boolean,
        Random Boolean,
        Note Comment,
        Comment Comment,
        Sat_Mode SatMode,
        O_PWR Real,
        O_ANT Name,
        PaperlogRef Integer,
        isDeleted Boolean,
/* "Normal" Referential attributes */
        
        REFMeteorShower_Id UniqueId,
        REFDisplayName Name,
        REFContest_Id UniqueId,
        REFSatellite_Id UniqueId,
        REFBaseCall Callsign,
        
        REFInstance Integer,
        REFDXCC_Id UniqueId,
        REFQSO_Id UniqueId,
        REFOperatingCondition_Id UniqueId,
        REFPropagationName Name,
/* Primary key */
      Primary key(
        QSO_Id
      )

);
  
/*****************************************************************************/
/*
This is a list of known contests. Some are even specified in the ADIF standard. Like for instance
SAC-SSB
SARTG-RTTY

However it is unlikely that every contest there is will ever be in this standard so it should be possible to add your own like
NAC

(To be honest: There are too many contests)
*/
Create table Contest (
        

/* Ordinary attributes */
	Contest_Id UniqueId,
        Name Name,
        ADIF_Supported Boolean
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        Contest_Id
      )

);
  
/*****************************************************************************/
/*
This is a Fixed list specified in ADIF-standard translations to actual used list
Cross mode QSOs currently not supported
*/
Create table Mode (
        

/* Ordinary attributes */
	DisplayName Name,
        ADIF_Mode Name,
        ADIF_SubMode Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        DisplayName
      )

);
  
/*****************************************************************************/
/*
The conditions for the held QSO.

I feel uncertain if this is an actual information carrying class or if is to be treated as a template just for collecting UsedCall/Operator/Equipment combos.
*/
Create table OperatingCondition (
        

/* Ordinary attributes */
	OperatingCondition_Id UniqueId,
        Name Name,
/* "Normal" Referential attributes */
        
        REFCall Callsign,
        REFOpCall Callsign,
        REFUsedEq_Id UniqueId,
        REFUsedLocation_Id UniqueId,
/* Primary key */
      Primary key(
        OperatingCondition_Id
      )

);
  
/*****************************************************************************/
/*
There are online log services on the internet. Places where you can upload your log for different purposes.
ADIF supports three:
CLUBLOG, QRZCOM, HRDLOG, ...

But there is no reason to think these are the last or only.
*/
Create table OnlineLogservice (
        

/* Ordinary attributes */
	LogserviceId UniqueId,
        Name Name,
        UploadDateField Name,
        UploadStatusField Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        LogserviceId
      )

);
  
/*****************************************************************************/
/*
A contacted station may operate with different additions to its callsign. For instance /P /M and so on. It is still the same station and person behind it.

In some countries callsigns are changed and the "old" is re-issued to someone else.
Thus the instance attribute which must be used together with basecall to be unique
*/
Create table ContactedStation (
        

/* Ordinary attributes */
	BaseCall Callsign,
        Instance Integer,
        Nick Name,
        Name Name,
        Manager Callsign,
        IsSilentKey Boolean,
/* "Normal" Referential attributes */
        
        REFBaseCall Callsign,
        
        REFInstance Integer,
/* Primary key */
      Primary key(
        BaseCall,Instance
      )

);
  
/*****************************************************************************/
/*
LOTW, EQSL.CC, Buro, Direct, ...
*/
Create table ConfirmationService (
        

/* Ordinary attributes */
	ConfirmationService_Id UniqueId,
        Name Name,
        QSLRDateField Name,
        QSLSDateField Name,
        QSLRCVDField Name,
        QSLSENTField Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        ConfirmationService_Id
      )

);
  
/*****************************************************************************/
/*
For each QSL service interaction there can be a number of confirmations sent or received. Instances of this class indicate each of these. So when a card is received one of these are created. When a record is uploaded to LOTW another one is created.
The attributes add information about when the record was created and any specific message to be printed on the card/was printed on the card.
*/
Create table QSOConfirmation (
        

/* Ordinary attributes */
	Confirmation_Id UniqueId,
        Number Integer,
        QSLMessage Comment,
        RegisteredDate UTCDateTime
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        Confirmation_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this specialization of QSOConfirmation are created when a confirmation is received. That can be either from a online QSL service like Logbook of the world or a physical paper card.
There can be more than one instance for each QSO as there may be many cards received for the same QSO for a number of reasons.
*/
Create table ReceivedConfirmation (
        
/* Specialization attributes */
          
        GENConfirmation_Id UniqueId,

/* Ordinary attributes */
	
/* "Normal" Referential attributes */
        
        REFQSL_Rcvd Char,
/* Primary key */
      Primary key(
        
        GENConfirmation_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this specialization of QSOConfirmation are created when a confirmation is to be sent or have been sent. That can be either to an online QSL service like Logbook of the world or a physical paper card.
There can be more than one instance for each QSO as there may be many cards sent for the same QSO for a number of reasons.

Note that instances can be created without sending a confirmation. Just the intention to do so is enough.
*/
Create table SentConfirmation (
        
/* Specialization attributes */
          
        GENConfirmation_Id UniqueId,

/* Ordinary attributes */
	StillValid Boolean,
/* "Normal" Referential attributes */
        
        REFQSL_Sent Char,
/* Primary key */
      Primary key(
        
        GENConfirmation_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class are created for satellites actually used or where there is an intention or hope to work across them.

*/
Create table Satellite (
        

/* Ordinary attributes */
	Satellite_Id UniqueId,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        Satellite_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class exist for each item included in an award. For example one row for each US state, one for each swedish län and one for each german DOK.
There will be a lot of instances in this table...
*/
Create table AwardItem (
        

/* Ordinary attributes */
	AwardItem_Id UniqueId,
        Name Name,
/* "Normal" Referential attributes */
        
        REFAward_Id UniqueId,
/* Primary key */
      Primary key(
        AwardItem_Id
      )

);
  
/*****************************************************************************/
/*
For each award or diploma of interest there should be one instances of this class. For instance one for the german DOK award, one for the swedish Län award, one for VUCC and so on.
*/
Create table Award (
        

/* Ordinary attributes */
	Award_Id UniqueId,
        Name Name,
        ADIF_Field Name,
/* "Normal" Referential attributes */
        
        REFDXCC_Id UniqueId,
/* Primary key */
      Primary key(
        Award_Id
      )

);
  
/*****************************************************************************/
/*
For each meteor shower of interest there is one instance of this class. For instance Perseids, Leonids....
*/
Create table MeteorShower (
        

/* Ordinary attributes */
	MeteorShower_Id UniqueId,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        MeteorShower_Id
      )

);
  
/*****************************************************************************/
/*
There shall be an instance of every DXCC entity that has ever existed of this class. When a new entity is approved its valid from data should be added and when one is removed from the list its valid_To date should be set.
*/
Create table DXCC_Entity (
        

/* Ordinary attributes */
	DXCC_Id UniqueId,
        Name Name,
        Valid_From UTCDateTime,
        Valid_To UTCDateTime
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        DXCC_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class document places from which contacts are made.
*/
Create table UsedLocation (
        

/* Ordinary attributes */
	UsedLocation_Id UniqueId,
        Name Name,
        Locator Locator,
/* "Normal" Referential attributes */
        
        REFDXCC_Id UniqueId,
/* Primary key */
      Primary key(
        UsedLocation_Id
      )

);
  
/*****************************************************************************/
/*
Lists different station setups available for use.
*/
Create table UsedEquipment (
        

/* Ordinary attributes */
	UsedEq_Id UniqueId,
        MY_ANT Comment,
        MY_RIG Comment
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        UsedEq_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class represent the different calls that can be used for contacts. For instance separate instances for SK5BN, 7S5LH, 8S5N and SE19O. Instances can be set up before beeing used.
*/
Create table UsedCall (
        

/* Ordinary attributes */
	Call Callsign,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        Call
      )

);
  
/*****************************************************************************/
/*
Instances of this class represents the different operators making contacts recorded in this logbook. 

In most cases there would probably only be one instance of this class but in cases where a person gets 
a new callsign or where the logbooks of two different persons are mainted in paralell there may be many instances.
*/
Create table Operator (
        

/* Ordinary attributes */
	OpCall Callsign,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        OpCall
      )

);
  
/*****************************************************************************/
/*
Different atmospheric conditions allow contacts to be made. Instances of this class shall exist at least for those used (when known). These are things like TEP, IONOSCATTER, AUE and LOS among other.
*/
Create table Propagation_Mode (
        

/* Ordinary attributes */
	PropagationName Name,
        Description Comment
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        PropagationName
      )

);
  
/*****************************************************************************/
/*
There are four differrent statuses for sent confirmations defined by ADIF. One instance of this class for each.

*/
Create table QSL_Sent_Status (
        

/* Ordinary attributes */
	QSL_Sent Char,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        QSL_Sent
      )

);
  
/*****************************************************************************/
/*
There are four differrent statuses for received confirmations defined by ADIF. One instance of this class for each.
*/
Create table QSL_Rcvd_Status (
        

/* Ordinary attributes */
	QSL_Rcvd Char,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        QSL_Rcvd
      )

);
  
/*****************************************************************************/
/*
Confirmations, especially physical cards, are sent to somewhere for approval. This may be that. Have not yet decided if it is. Should it not be related to a "ReceivedConfirmation" then aswell?
*/
Create table Checker (
        

/* Ordinary attributes */
	Checker_Id UniqueId,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        Checker_Id
      )

);
  
/*****************************************************************************/
/*
The ADIF standard specifies four different status values possible for uploads to online QSL services. Instances of this class matches these status values.
*/
Create table UploadStatus (
        

/* Ordinary attributes */
	Indicator Char,
        Name Name
/* "Normal" Referential attributes */
        ,
/* Primary key */
      Primary key(
        Indicator
      )

);
  
/*****************************************************************************/
/*
Instances of this AssociationClass specifies the relation between a contact and the different log services.
An instance here is not an indicator of that the QSO has been uploaded to the actual log service. Status may indicate that it is intended to happen later.

Delete of QSOs are not supported by this constuction as you can't have instances here without having a QSO...
*/
Create table OnlineLogInteraction (
        
/* AssociationClass ends */
          
        ASOLogserviceId UniqueId,
        ASOQSO_Id UniqueId

/* Ordinary attributes */
	UploadDate UTCDateTime,
/* "Normal" Referential attributes */
        
        REFIndicator Char,
/* Primary key */
      Primary key(
        
        ASOLogserviceId,
        ASOQSO_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this AssociationClass specifies the relation between a contact and the different confirmation services.
An instance here is not an indicator of that the QSO has been uploaded to or confirmed by the actual confirmation service. Status may indicate that it is intended to happen later.

Delete of QSOs are not supported by this constuction as you can't have instances here without having a QSO...
*/
Create table ConfirmationServiceInteraction (
        
/* AssociationClass ends */
          
        ASOConfirmationService_Id UniqueId,
        ASOQSO_Id UniqueId

/* Ordinary attributes */
	
/* "Normal" Referential attributes */
        
        REFConfirmation_Id UniqueId,
/* Primary key */
      Primary key(
        
        ASOConfirmationService_Id,
        ASOQSO_Id
      )

);
  
/*****************************************************************************/
/*
Instances of the association class indicates that the qso is associated with a specific award item. For instance working station X gives you credit for a square of VUCC, an island for IOTA and perhaps a commune for WALA
*/
Create table ReceivedAwardCredit (
        
/* AssociationClass ends */
          
        ASOAwardItem_Id UniqueId,
        ASOQSO_Id UniqueId

/* Ordinary attributes */
	Approved Boolean,
/* "Normal" Referential attributes */
        ,
        REFChecker_Id UniqueId,
/* Primary key */
      Primary key(
        
        ASOAwardItem_Id,
        ASOQSO_Id
      )

);
  
/*****************************************************************************/
/*
For each used location there may be a number of location based award items granted to worked stations. And of cource the opposite is also true. 
A specific awardItem can be granted from a number of different used locations.

Instances of this association class connects these two classes in a many to many relationship.
*/
Create table LocationRelatedAwardCredit (
        
/* AssociationClass ends */
          
        ASOUsedLocation_Id UniqueId,
        ASOAwardItem_Id UniqueId

/* Ordinary attributes */
	
/* "Normal" Referential attributes */
        
/* Primary key */
      Primary key(
        
        ASOUsedLocation_Id,
        ASOAwardItem_Id
      )

);
  
      alter table QSO
       add foreign key (meteorshower)
       references
      MeteorShower(meteorshower);
      
      alter table QSO
       add foreign key (mode)
       references
      Mode(mode);
      
      alter table QSO
       add foreign key (contest)
       references
      Contest(contest);
      
      alter table QSO
       add foreign key (satellite)
       references
      Satellite(satellite);
      
      alter table QSO
       add foreign key (contactedstation)
       references
      ContactedStation(contactedstation);
      
      alter table QSO
       add foreign key (dxcc_entity)
       references
      DXCC_Entity(dxcc_entity);
      
      alter table QSO
       add foreign key (listened in on)
       references
      QSO(listened in on);
      
      alter table QSO
       add foreign key (operatingcondition)
       references
      OperatingCondition(operatingcondition);
      
      alter table QSO
       add foreign key (propagation_mode)
       references
      Propagation_Mode(propagation_mode);
      
      alter table OperatingCondition
       add foreign key (Uses)
       references
      UsedCall(Uses);
      
      alter table OperatingCondition
       add foreign key (Is working)
       references
      Operator(Is working);
      
      alter table OperatingCondition
       add foreign key (uses)
       references
      UsedEquipment(uses);
      
      alter table OperatingCondition
       add foreign key (is from)
       references
      UsedLocation(is from);
      
      alter table ContactedStation
       add foreign key (contactedstation)
       references
      ContactedStation(contactedstation);
      
      alter table ReceivedConfirmation
       add foreign key (Has as status)
       references
      QSL_Rcvd_Status(Has as status);
      
      alter table SentConfirmation
       add foreign key (Has as status)
       references
      QSL_Sent_Status(Has as status);
      
      alter table AwardItem
       add foreign key (award)
       references
      Award(award);
      
      alter table Award
       add foreign key (dxcc_entity)
       references
      DXCC_Entity(dxcc_entity);
      
      alter table UsedLocation
       add foreign key (dxcc_entity)
       references
      DXCC_Entity(dxcc_entity);
      