<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uml="http://www.eclipse.org/uml2/5.0.0/UML" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">
  <xsl:output method="text" indent="no"/>

<!--
TODO:

Specialization: Classes specializing other classes should have referential attributes
to the more general class. These should also be set as primary key

AssociationClasses: Identifying attributes from the associated classes should be
added. These are primary key.

Foreign keys: Add constraints. Need to be done by alter table after all table creations
since the order of creation can't be controlled.

References: Id attributes are set as "NOT NULL" in the created domain. This fails for
Referential attributes where the association is conditional. (0..n)

-->

<!--  Template for root node -->
  <xsl:template match="/">
    <xsl:apply-templates select="/uml:Model"/>
  </xsl:template>


<!--========================================================================-->
<!--  Template for "Model" element -->
  <xsl:template match="uml:Model">
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Package']"/>
  </xsl:template>


<!--========================================================================-->
<!--  Template for "package" element -->
  <xsl:template match="packagedElement[@xmi:type='uml:Package']">
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Package']"/>
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:DataType']"/>
    <!-- create tables for each class and association class -->
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="ctable"/>
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:AssociationClass']" mode="ctable"/>
    <!-- create referential integrity enforcement statements -->
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="aconst"/>
  </xsl:template>

<!--========================================================================-->
<!--  Template for "class" and "association class" element -->
  <xsl:template match="packagedElement[@xmi:type='uml:Class'] | packagedElement[@xmi:type='uml:AssociationClass']" mode="ctable">
/*****************************************************************************/
/*
<xsl:choose>
  <xsl:when test="ownedComment/body">
    <xsl:value-of select="ownedComment/body"/>
  </xsl:when>
  <xsl:otherwise>
    No comment written for class <xsl:value-of select="@name"/>. Please do.
  </xsl:otherwise>
</xsl:choose>
*/
Create table <xsl:value-of select="@name"/> (
        <xsl:if test="generalization">
/* Specialization attributes */
          <xsl:variable name="genclass" select="generalization/@general"/>
          <xsl:apply-templates select="../packagedElement[@xmi:id=$genclass]/ownedAttribute[@isID='true']" mode="nonref">
            <xsl:with-param name="prefix">
        GEN</xsl:with-param>
          </xsl:apply-templates><xsl:if test="position() != last()">,</xsl:if>
        </xsl:if>
        <xsl:if test="@xmi:type='uml:AssociationClass'">
/* AssociationClass ends */
          <xsl:for-each select="ownedEnd">
            <xsl:variable name="asoclass" select="@type"/>
            <xsl:apply-templates select="../../packagedElement[@xmi:id=$asoclass]/ownedAttribute[@isID='true']" mode="nonref">
            <xsl:with-param name="prefix">
        ASO</xsl:with-param>
          </xsl:apply-templates><xsl:if test="position() != last()">,</xsl:if>
          </xsl:for-each>
        </xsl:if>

/* Ordinary attributes */
	<xsl:apply-templates select="ownedAttribute[not(@association)]" mode="nonref"> <!-- discriminates association attributes -->
          <xsl:with-param name="prefix"/>
        </xsl:apply-templates>
        <xsl:if test="count(ownedAttribute[not(@association)])>0 and count(ownedAttribute[@association])>0">,</xsl:if>
/* "Normal" Referential attributes */
        <xsl:for-each select="ownedAttribute[@association]">
          <!-- loop over referred classes -->
          <xsl:variable name="refclass">
            <!--get reference to the actual class-->
            <xsl:value-of select="@type"/>
          </xsl:variable>
          <xsl:apply-templates select="../../packagedElement[@xmi:id=$refclass]/ownedAttribute[@isID='true']" mode="nonref">
            <xsl:with-param name="prefix">
        REF</xsl:with-param>
          </xsl:apply-templates><xsl:if test="position() != last()">,</xsl:if>

        </xsl:for-each>
        <xsl:if test="count(ownedAttribute)>0">,</xsl:if>
/* Primary key */
      Primary key(
        <xsl:apply-templates select="ownedAttribute[@isID='true']" mode="pk"/>
        <xsl:if test="generalization">
          <xsl:variable name="genclass" select="generalization/@general"/>
          <xsl:apply-templates select="../packagedElement[@xmi:id=$genclass]/ownedAttribute[@isID='true']" mode="pk">
            <xsl:with-param name="prefix">
        GEN</xsl:with-param>
          </xsl:apply-templates>
        </xsl:if>
        <xsl:if test="@xmi:type='uml:AssociationClass'">
          <xsl:for-each select="ownedEnd">
            <xsl:variable name="asoclass" select="@type"/>
            <xsl:apply-templates select="../../packagedElement[@xmi:id=$asoclass]/ownedAttribute[@isID='true']" mode="pk">
            <xsl:with-param name="prefix">
        ASO</xsl:with-param>
          </xsl:apply-templates><xsl:if test="position() != last()">,</xsl:if>
          </xsl:for-each>
        </xsl:if>
      )
<!--    </xsl:if>-->
);
  </xsl:template>



<!--========================================================================-->
<!--  Template for ordinary attributes -->
  <xsl:template match="ownedAttribute" mode="nonref">
    <xsl:param name="prefix"/>
<!-- attribute name -->
<xsl:value-of select="$prefix"/><xsl:value-of select="@name"/>
<!-- attribute datatype -->
      <xsl:variable name="type">
        <xsl:call-template name="id2name">
          <xsl:with-param name="id" select="@type"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:variable name="domain">
        <xsl:call-template name="mapType2Domain">
          <xsl:with-param name="modeltype" select="$type"/>
        </xsl:call-template>
      </xsl:variable>
      <xsl:text> </xsl:text>
      <xsl:value-of select="$type"/>
<!-- comma separator if not last attribute -->
<xsl:if test="position() != last()">,
        </xsl:if>
  </xsl:template>



<!--========================================================================-->
<!--  Template for primary key attributes -->
  <xsl:template match="ownedAttribute[@isID='true']" mode="pk">
    <xsl:param name="prefix"/>
<xsl:value-of select="$prefix"/><xsl:value-of select="@name"/><xsl:if test="position() != last()">,</xsl:if>
  </xsl:template>



<!--========================================================================-->
<!--  Template for datatypes -->
  <xsl:template match="packagedElement[@xmi:type='uml:DataType']">
    <xsl:variable name="modeltype">
      <xsl:value-of select="@name"/>
    </xsl:variable>
    <xsl:variable name="domain">
      <xsl:call-template name="mapType2Domain">
        <xsl:with-param name="modeltype" select="$modeltype"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="not($domain='**')">
create domain <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="$domain"/>;
</xsl:if>
  </xsl:template>

<!--========================================================================-->
<!--  Template for referential constraints -->
  <xsl:template match="packagedElement[@xmi:type='uml:Class'] | packagedElement[@xmi:type='uml:AssociationClass']" mode="aconst">
    <xsl:for-each select="ownedAttribute[@association]">
      <xsl:variable name="refclass">
        <!--get reference to the actual class-->
        <xsl:value-of select="@type"/>
      </xsl:variable>
      <xsl:variable name="attr" select="@name"/>
      alter table <xsl:value-of select="../@name"/>
       add foreign key (REF<xsl:value-of select="@name"/>)
       references
      <xsl:for-each select="../../packagedElement[@xmi:id=$refclass]">
        <xsl:value-of select="@name"/>(<xsl:value-of select="$attr"/>);
      </xsl:for-each>
    </xsl:for-each>

  </xsl:template>


<!--========================================================================-->
<!--
    Given an id to a type, returns the name of that type
-->
  <xsl:template name="id2name">
    <xsl:param name="id"/>
    <xsl:variable name="type" select="//*[@xmi:id=$id]"/>
    <xsl:value-of select="$type/@name"/>
  </xsl:template>

<!--========================================================================-->
<!--
    Given a model-type, returns the domain to use
-->

<xsl:template name="mapType2Domain">
  <xsl:param name="modeltype"/>
  <xsl:variable name="ntype" select="normalize-space($modeltype)"/>
  <xsl:choose>
    <xsl:when test="$ntype = 'UniqueId'">INTEGER NOT NULL</xsl:when>
    <xsl:when test="$ntype = 'Name'">VARCHAR(32)</xsl:when>
    <xsl:when test="$ntype = 'Callsign'">VARCHAR(20)</xsl:when>
    <xsl:when test="$ntype = 'Comment'">VARCHAR(120)</xsl:when>
    <xsl:when test="$ntype = 'Locator'">VARCHAR(12)</xsl:when>
    <xsl:when test="$ntype = 'UTCDateTime'">TIMESTAMP</xsl:when>
    <xsl:when test="$ntype = 'Real'">**</xsl:when>
    <xsl:when test="$ntype = 'Integer'">**</xsl:when>
    <xsl:when test="$ntype = 'Boolean'">CHAR(1) CHECK (VALUE IN ('T','F'))</xsl:when>
    <xsl:when test="$ntype = 'ReportString'">VARCHAR(6)</xsl:when>
    <xsl:when test="$ntype = 'Char'">**</xsl:when>
    <xsl:when test="$ntype = 'SatMode'">VARCHAR(4)</xsl:when>
    <xsl:otherwise>--ERROR--</xsl:otherwise>
  </xsl:choose>
</xsl:template>
</xsl:stylesheet>


