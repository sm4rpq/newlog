
create domain UniqueId INTEGER;
  
create domain Name VARCHAR(32);
  
create domain Callsign VARCHAR(20);
  
create domain Comment VARCHAR(120);
  
create domain Locator VARCHAR(12);
  
create domain UTCDateTime TIMESTAMP;
  
create domain Boolean CHAR(1) CHECK (VALUE IN ('T','F'));
  
create domain ReportString VARCHAR(6);
  
create domain SatMode VARCHAR(4);
  
/*****************************************************************************/
/*
This is what it's all about. For every contact held there will be one instance of this class.

*/
Create table QSO (
  QSO_Id UniqueId NOT NULL,
  StartTime UTCDateTime,
  EndTime UTCDateTime,
  Call Callsign,
  Power Real,
  Frequency_TX Double Precision,
  Frequency_RX Double Precision,
  Rpt_S ReportString,
  Rpt_R ReportString,
  Locator Locator,
  QTH Name,
  Serial_S ReportString,
  Serial_R ReportString,
  Complete Boolean,
  Random Boolean,
  Note Comment,
  Comment Comment,
  Sat_Mode SatMode,
  O_PWR Real,
  O_ANT Name,
  PaperlogRef Integer,
  isDeleted Boolean,
  isSWL Boolean,
  REFMeteorShower_Id UniqueId,
  REFMode Name NOT NULL,
  REFContest_Id UniqueId,
  REFSatellite_Id UniqueId,
  REFBaseCall Callsign NOT NULL,
  REFInstance Integer NOT NULL,
  REFDXCC_Id UniqueId,
  REFQSO_Id UniqueId,
  REFPropagationName Name,
  REFUsedEq_Id UniqueId NOT NULL,
  REFOpCall Callsign NOT NULL,
  REFCall Callsign NOT NULL,
  REFUsedLocation_Id UniqueId NOT NULL,
  
/* Primary key */
  Primary key(
        QSO_Id
      )

);
  
/*****************************************************************************/
/*
This is a list of known contests. Some are even specified in the ADIF standard. Like for instance
SAC-SSB
SARTG-RTTY

However it is unlikely that every contest there is will ever be in this standard so it should be possible to add your own like
NAC

(To be honest: There are too many contests)
*/
Create table Contest (
  Contest_Id UniqueId NOT NULL,
  Name Name,
  ADIF_Supported Boolean,
  
/* Primary key */
  Primary key(
        Contest_Id
      )

);
  
/*****************************************************************************/
/*
This is a Fixed list specified in ADIF-standard translations to actual used list
Cross mode QSOs currently not supported
*/
Create table OperatingMode (
  Mode Name NOT NULL,
  ADIF_Mode Name,
  ADIF_SubMode Name,
  
/* Primary key */
  Primary key(
        Mode
      )

);
  
/*****************************************************************************/
/*
The conditions for the held QSO.

I feel uncertain if this is an actual information carrying class or if is to be treated as a template just for collecting UsedCall/Operator/Equipment combos.
*/
Create table OperatingCondition (
  OperatingCondition_Id UniqueId NOT NULL,
  Name Name,
  REFCall Callsign NOT NULL,
  REFOpCall Callsign NOT NULL,
  REFUsedEq_Id UniqueId NOT NULL,
  REFUsedLocation_Id UniqueId NOT NULL,
  
/* Primary key */
  Primary key(
        OperatingCondition_Id
      )

);
  
/*****************************************************************************/
/*
There are online log services on the internet. Places where you can upload your log for different purposes.
ADIF supports three:
CLUBLOG, QRZCOM, HRDLOG, ...

But there is no reason to think these are the last or only.
*/
Create table OnlineLogservice (
  Logservice_Id UniqueId NOT NULL,
  Name Name,
  UploadDateField Name,
  UploadStatusField Name,
  
/* Primary key */
  Primary key(
        Logservice_Id
      )

);
  
/*****************************************************************************/
/*
A contacted station may operate with different additions to its callsign. For instance /P /M and so on. It is still the same station and person behind it.

In some countries callsigns are changed and the "old" is re-issued to someone else.
Thus the instance attribute which must be used together with basecall to be unique
*/
Create table ContactedStation (
  BaseCall Callsign NOT NULL,
  Instance Integer NOT NULL,
  Nick Name,
  Name Name,
  Manager Callsign,
  IsSilentKey Boolean,
  REFBaseCall Callsign,
  REFInstance Integer,
  REFConfirmationService_Id UniqueId,
  
/* Primary key */
  Primary key(
        BaseCall,
        Instance
      )

);
  
/*****************************************************************************/
/*
LOTW, EQSL.CC, Buro, Direct, ...
*/
Create table ConfirmationService (
  ConfirmationService_Id UniqueId NOT NULL,
  Name Name,
  QSLRDateField Name,
  QSLSDateField Name,
  QSLRCVDField Name,
  QSLSENTField Name,
  
/* Primary key */
  Primary key(
        ConfirmationService_Id
      )

);
  
/*****************************************************************************/
/*
For each QSL service interaction there can be a number of confirmations sent or received. Instances of this class indicate each of these. So when a card is received one of these are created. When a record is uploaded to LOTW another one is created.
The attributes add information about when the record was created and any specific message to be printed on the card/was printed on the card.
*/
Create table QSOConfirmation (
  Confirmation_Id UniqueId NOT NULL,
  Number Integer,
  QSLMessage Comment,
  RegisteredDate UTCDateTime,
  ASOConfirmationService_Id UniqueId NOT NULL,
  ASOQSO_Id UniqueId NOT NULL,
  
/* Primary key */
  Primary key(
        Confirmation_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this specialization of QSOConfirmation are created when a confirmation is received. That can be either from a online QSL service like Logbook of the world or a physical paper card.
There can be more than one instance for each QSO as there may be many cards received for the same QSO for a number of reasons.
*/
Create table ReceivedConfirmation (
  GENConfirmation_Id UniqueId NOT NULL,
  REFQSL_Rcvd Char NOT NULL,
  
/* Primary key */
  Primary key(
        GENConfirmation_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this specialization of QSOConfirmation are created when a confirmation is to be sent or have been sent. That can be either to an online QSL service like Logbook of the world or a physical paper card.
There can be more than one instance for each QSO as there may be many cards sent for the same QSO for a number of reasons.

Note that instances can be created without sending a confirmation. Just the intention to do so is enough.
*/
Create table SentConfirmation (
  GENConfirmation_Id UniqueId NOT NULL,
  StillValid Boolean,
  REFQSL_Sent Char NOT NULL,
  
/* Primary key */
  Primary key(
        GENConfirmation_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class are created for satellites actually used or where there is an intention or hope to work across them.

*/
Create table Satellite (
  Satellite_Id UniqueId NOT NULL,
  Name Name,
  Designator Name,
  OpStart UTCDateTime,
  OpEnd UTCDateTime,
  
/* Primary key */
  Primary key(
        Satellite_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class exist for each item included in an award. For example one row for each US state, one for each swedish län and one for each german DOK.
There will be a lot of instances in this table...
*/
Create table AwardItem (
  AwardItem_Id UniqueId NOT NULL,
  Name Name,
  REFAward_Id UniqueId NOT NULL,
  
/* Primary key */
  Primary key(
        AwardItem_Id
      )

);
  
/*****************************************************************************/
/*
For each award or diploma of interest there should be one instances of this class. For instance one for the german DOK award, one for the swedish Län award, one for VUCC and so on.
*/
Create table Award (
  Award_Id UniqueId NOT NULL,
  Name Name,
  ADIF_Field Name,
  DynamicItems Boolean,
  REFDXCC_Id UniqueId,
  
/* Primary key */
  Primary key(
        Award_Id
      )

);
  
/*****************************************************************************/
/*
For each meteor shower of interest there is one instance of this class. For instance Perseids, Leonids....
*/
Create table MeteorShower (
  MeteorShower_Id UniqueId NOT NULL,
  Name Name,
  StartTime UTCDateTime,
  EndTime UTCDateTime,
  
/* Primary key */
  Primary key(
        MeteorShower_Id
      )

);
  
/*****************************************************************************/
/*
There shall be an instance of every DXCC entity that has ever existed of this class. When a new entity is approved its valid from data should be added and when one is removed from the list its valid_To date should be set.
*/
Create table DXCC_Entity (
  DXCC_Id UniqueId NOT NULL,
  Name Name,
  Valid_From UTCDateTime,
  Valid_To UTCDateTime,
  SvNamn Name,
  ISO_A3 Name,
  
/* Primary key */
  Primary key(
        DXCC_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class document places from which contacts are made.
*/
Create table UsedLocation (
  UsedLocation_Id UniqueId NOT NULL,
  Name Name,
  Locator Locator,
  HeightAMSL Double Precision,
  REFDXCC_Id UniqueId,
  
/* Primary key */
  Primary key(
        UsedLocation_Id
      )

);
  
/*****************************************************************************/
/*
Lists different station setups available for use.
*/
Create table UsedEquipment (
  UsedEq_Id UniqueId NOT NULL,
  MY_ANT Comment,
  MY_RIG Comment,
  
/* Primary key */
  Primary key(
        UsedEq_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this class represent the different calls that can be used for contacts. For instance separate instances for SK5BN, 7S5LH, 8S5N and SE19O. Instances can be set up before beeing used.
*/
Create table UsedCall (
  Call Callsign NOT NULL,
  Name Name,
  
/* Primary key */
  Primary key(
        Call
      )

);
  
/*****************************************************************************/
/*
Instances of this class represents the different operators making contacts recorded in this logbook. 

In most cases there would probably only be one instance of this class but in cases where a person gets 
a new callsign or where the logbooks of two different persons are mainted in paralell there may be many instances.
*/
Create table Operator (
  OpCall Callsign NOT NULL,
  Name Name,
  Nick Name,
  
/* Primary key */
  Primary key(
        OpCall
      )

);
  
/*****************************************************************************/
/*
Different atmospheric conditions allow contacts to be made. Instances of this class shall exist at least for those used (when known). These are things like TEP, IONOSCATTER, AUE and LOS among other.
*/
Create table Propagation_Mode (
  PropagationName Name NOT NULL,
  Description Comment,
  
/* Primary key */
  Primary key(
        PropagationName
      )

);
  
/*****************************************************************************/
/*
There are four differrent statuses for sent confirmations defined by ADIF. One instance of this class for each.

*/
Create table QSL_Sent_Status (
  QSL_Sent Char NOT NULL,
  Name Name,
  
/* Primary key */
  Primary key(
        QSL_Sent
      )

);
  
/*****************************************************************************/
/*
There are four differrent statuses for received confirmations defined by ADIF. One instance of this class for each.
*/
Create table QSL_Rcvd_Status (
  QSL_Rcvd Char NOT NULL,
  Name Name,
  
/* Primary key */
  Primary key(
        QSL_Rcvd
      )

);
  
/*****************************************************************************/
/*
Confirmations, especially physical cards, are sent to somewhere for approval. This may be that. Have not yet decided if it is. Should it not be related to a "ReceivedConfirmation" then aswell?
*/
Create table Checker (
  Checker_Id UniqueId NOT NULL,
  Name Name,
  
/* Primary key */
  Primary key(
        Checker_Id
      )

);
  
/*****************************************************************************/
/*
The ADIF standard specifies four different status values possible for uploads to online QSL services. Instances of this class matches these status values.
*/
Create table UploadStatus (
  Indicator Char NOT NULL,
  Name Name,
  
/* Primary key */
  Primary key(
        Indicator
      )

);
  
/*****************************************************************************/
/*

    No comment written for class CallPrefix. Please do.
  
*/
Create table CallPrefix (
  Prefix Callsign NOT NULL,
  REFDXCC_Id UniqueId NOT NULL
);
  
/*****************************************************************************/
/*
Instances of this class represent a set of orbital parameters for a certain satellite.
More than one such set may exist for each satellite.
*/
Create table ElementSet (
  ElementSet_Id Name NOT NULL,
  ElementSet_Number Integer NOT NULL,
  Epoch_Year Integer,
  Epoch_Day Double Precision,
  RevNumAtEpoch Integer,
  MeanMotion Double Precision,
  MeanMotionPrim Double Precision,
  MeanMotionBis Double Precision,
  BStarDragTerm Double Precision,
  Eccentricity Double Precision,
  Inclination Double Precision,
  RAAN Double Precision,
  ArgumentOfPerigee Double Precision,
  MeanAnomaly Double Precision,
  REFSatellite_Id UniqueId NOT NULL,
  
/* Primary key */
  Primary key(
        ElementSet_Id,
        ElementSet_Number
      )

);
  
/*****************************************************************************/
/*
Instances of this AssociationClass specifies the relation between a contact and the different log services.
An instance here is not an indicator of that the QSO has been uploaded to the actual log service. Status may indicate that it is intended to happen later.

Delete of QSOs are not supported by this constuction as you can't have instances here without having a QSO...
*/
Create table OnlineLogInteraction (
  ASOLogservice_Id UniqueId NOT NULL,
  ASOQSO_Id UniqueId NOT NULL,
  UploadDate UTCDateTime,
  REFIndicator Char NOT NULL,
  
/* Primary key */
  Primary key(
        ASOLogservice_Id,
        ASOQSO_Id
      )

);
  
/*****************************************************************************/
/*
Instances of this AssociationClass specifies the relation between a contact and the different confirmation services.
An instance here is not an indicator of that the QSO has been uploaded to or confirmed by the actual confirmation service. Status may indicate that it is intended to happen later.

Delete of QSOs are not supported by this constuction as you can't have instances here without having a QSO...
*/
Create table ConfirmationServiceInteraction (
  ASOConfirmationService_Id UniqueId NOT NULL,
  ASOQSO_Id UniqueId NOT NULL,
  
/* Primary key */
  Primary key(
        ASOConfirmationService_Id,
        ASOQSO_Id
      )

);
  
/*****************************************************************************/
/*
Instances of the association class indicates that the qso is associated with a specific award item. For instance working station X gives you credit for a square of VUCC, an island for IOTA and perhaps a commune for WALA
*/
Create table ReceivedAwardCredit (
  ASOAwardItem_Id UniqueId NOT NULL,
  ASOQSO_Id UniqueId NOT NULL,
  Approved Boolean,
  GENConfirmation_Id UniqueId,
  REFChecker_Id UniqueId,
  
/* Primary key */
  Primary key(
        ASOAwardItem_Id,
        ASOQSO_Id
      )

);
  
/*****************************************************************************/
/*
For each used location there may be a number of location based award items granted to worked stations. And of cource the opposite is also true. 
A specific awardItem can be granted from a number of different used locations.

Instances of this association class connects these two classes in a many to many relationship.
*/
Create table LocationRelatedAwardCredit (
  ASOUsedLocation_Id UniqueId NOT NULL,
  ASOAwardItem_Id UniqueId NOT NULL,
  
/* Primary key */
  Primary key(
        ASOUsedLocation_Id,
        ASOAwardItem_Id
      )

);
  
alter table QSO
  add foreign key (
    REFMeteorShower_Id
  ) references MeteorShower(
    MeteorShower_Id
);
  
alter table QSO
  add foreign key (
    REFMode
  ) references OperatingMode(
    Mode
);
  
alter table QSO
  add foreign key (
    REFContest_Id
  ) references Contest(
    Contest_Id
);
  
alter table QSO
  add foreign key (
    REFSatellite_Id
  ) references Satellite(
    Satellite_Id
);
  
alter table QSO
  add foreign key (
    REFBaseCall,REFInstance
  ) references ContactedStation(
    BaseCall,Instance
);
  
alter table QSO
  add foreign key (
    REFDXCC_Id
  ) references DXCC_Entity(
    DXCC_Id
);
  
alter table QSO
  add foreign key (
    REFQSO_Id
  ) references QSO(
    QSO_Id
);
  
alter table QSO
  add foreign key (
    REFPropagationName
  ) references Propagation_Mode(
    PropagationName
);
  
alter table QSO
  add foreign key (
    REFUsedEq_Id
  ) references UsedEquipment(
    UsedEq_Id
);
  
alter table QSO
  add foreign key (
    REFOpCall
  ) references Operator(
    OpCall
);
  
alter table QSO
  add foreign key (
    REFCall
  ) references UsedCall(
    Call
);
  
alter table QSO
  add foreign key (
    REFUsedLocation_Id
  ) references UsedLocation(
    UsedLocation_Id
);
  
alter table OperatingCondition
  add foreign key (
    REFCall
  ) references UsedCall(
    Call
);
  
alter table OperatingCondition
  add foreign key (
    REFOpCall
  ) references Operator(
    OpCall
);
  
alter table OperatingCondition
  add foreign key (
    REFUsedEq_Id
  ) references UsedEquipment(
    UsedEq_Id
);
  
alter table OperatingCondition
  add foreign key (
    REFUsedLocation_Id
  ) references UsedLocation(
    UsedLocation_Id
);
  
alter table ContactedStation
  add foreign key (
    REFBaseCall,REFInstance
  ) references ContactedStation(
    BaseCall,Instance
);
  
alter table ContactedStation
  add foreign key (
    REFConfirmationService_Id
  ) references ConfirmationService(
    ConfirmationService_Id
);
  
alter table QSOConfirmation
  add foreign key (
    ASOConfirmationService_Id,ASOQSO_Id
  ) references ConfirmationServiceInteraction(
    ASOConfirmationService_Id,ASOQSO_Id
);
  
alter table ReceivedConfirmation
  add foreign key (
    REFQSL_Rcvd
  ) references QSL_Rcvd_Status(
    QSL_Rcvd
);
  
alter table ReceivedConfirmation
  add foreign key (
    GENConfirmation_Id
  ) references QSOConfirmation(
    Confirmation_Id
);
  
alter table SentConfirmation
  add foreign key (
    REFQSL_Sent
  ) references QSL_Sent_Status(
    QSL_Sent
);
  
alter table SentConfirmation
  add foreign key (
    GENConfirmation_Id
  ) references QSOConfirmation(
    Confirmation_Id
);
  
alter table AwardItem
  add foreign key (
    REFAward_Id
  ) references Award(
    Award_Id
);
  
alter table Award
  add foreign key (
    REFDXCC_Id
  ) references DXCC_Entity(
    DXCC_Id
);
  
alter table UsedLocation
  add foreign key (
    REFDXCC_Id
  ) references DXCC_Entity(
    DXCC_Id
);
  
alter table CallPrefix
  add foreign key (
    REFDXCC_Id
  ) references DXCC_Entity(
    DXCC_Id
);
  
alter table ElementSet
  add foreign key (
    REFSatellite_Id
  ) references Satellite(
    Satellite_Id
);
  
alter table OnlineLogInteraction
  add foreign key (
    REFIndicator
  ) references UploadStatus(
    Indicator
);
  
alter table OnlineLogInteraction
  add foreign key (
    ASOLogservice_Id
  ) references OnlineLogservice(
    Logservice_Id
);
  
alter table OnlineLogInteraction
  add foreign key (
    ASOQSO_Id
  ) references QSO(
    QSO_Id
);
  
alter table ConfirmationServiceInteraction
  add foreign key (
    ASOConfirmationService_Id
  ) references ConfirmationService(
    ConfirmationService_Id
);
  
alter table ConfirmationServiceInteraction
  add foreign key (
    ASOQSO_Id
  ) references QSO(
    QSO_Id
);
  
alter table ReceivedAwardCredit
  add foreign key (
    GENConfirmation_Id
  ) references ReceivedConfirmation(
    GENConfirmation_Id
);
  
alter table ReceivedAwardCredit
  add foreign key (
    REFChecker_Id
  ) references Checker(
    Checker_Id
);
  
alter table ReceivedAwardCredit
  add foreign key (
    ASOAwardItem_Id
  ) references AwardItem(
    AwardItem_Id
);
  
alter table ReceivedAwardCredit
  add foreign key (
    ASOQSO_Id
  ) references QSO(
    QSO_Id
);
  
alter table LocationRelatedAwardCredit
  add foreign key (
    ASOUsedLocation_Id
  ) references UsedLocation(
    UsedLocation_Id
);
  
alter table LocationRelatedAwardCredit
  add foreign key (
    ASOAwardItem_Id
  ) references AwardItem(
    AwardItem_Id
);
  
create index CallPrefix_ndx on CallPrefix
(
  Prefix
);
  