<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uml="http://www.eclipse.org/uml2/5.0.0/UML" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">
  <xsl:output method="xml" indent="yes"/>

<!--
Remaining things:

Major
=====
References to specializations: Attributes not found (affects receivedAwardCredit)
 (constraint implemented. Referential attribute not included)


Minor
=====
Relation Names could be set on foreign keys (yes it can, but how?)

-->

<!--  Template for root node -->
  <xsl:template match="/">
    <model>
      <xsl:apply-templates select="/uml:Model"/>
    </model>
  </xsl:template>


<!--========================================================================-->
<!--  Template for "Model" element -->
  <xsl:template match="uml:Model">
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Package']"/>
  </xsl:template>


<!--========================================================================-->
<!--  Template for "package" element -->
  <xsl:template match="packagedElement[@xmi:type='uml:Package']">
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Package']"/>
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:DataType']"/>
    <!-- create tables for each class and association class -->
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="ctable"/>
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:AssociationClass']" mode="ctable"/>
    <xsl:apply-templates select="packagedElement[@xmi:type='uml:Class']" mode="cindex"/>
  </xsl:template>


<!--========================================================================-->
<!--  Template for datatypes -->
  <xsl:template match="packagedElement[@xmi:type='uml:DataType']">
    <xsl:variable name="modeltype">
      <xsl:value-of select="@name"/>
    </xsl:variable>
    <xsl:variable name="domain">
      <xsl:call-template name="mapType2Domain">
        <xsl:with-param name="modeltype" select="$modeltype"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:if test="not($domain='**')">
      <datatype>
        <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
        <xsl:attribute name="domain"><xsl:value-of select="$domain"/></xsl:attribute>
      </datatype>
    </xsl:if>
  </xsl:template>


<!--========================================================================-->
<!--  Template for "class" and "association class" element -->
  <xsl:template match="packagedElement[@xmi:type='uml:Class'] | packagedElement[@xmi:type='uml:AssociationClass']" mode="ctable">
      <!-- class comment -->
    <class>
      <xsl:attribute name="name"><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:apply-templates select="ownedComment/body"/>
      <attributes>
        <!-- first take care of identifying attributes -->
        <!-- these can be from local class, a parent class or association ends if this is an association class-->

        <!-- local identifying attributes -->
	<xsl:apply-templates select="ownedAttribute[not(@association) and @isID='true']" mode="nonref">
          <xsl:with-param name="prefix"/>
          <xsl:with-param name="kind">local</xsl:with-param>
        </xsl:apply-templates>

        <!-- parent identifying attributes -->
        <xsl:if test="generalization">
          <xsl:variable name="genclass" select="generalization/@general"/>
          <xsl:apply-templates select="../packagedElement[@xmi:id=$genclass]/ownedAttribute[@isID='true']" mode="nonref">
            <xsl:with-param name="prefix">GEN</xsl:with-param>
            <xsl:with-param name="kind">inherited</xsl:with-param>
          </xsl:apply-templates>
        </xsl:if>

        <!-- AssociationClass ends -->
        <xsl:if test="@xmi:type='uml:AssociationClass'">
          <xsl:for-each select="ownedEnd">
            <xsl:variable name="asoclass" select="@type"/>
            <xsl:apply-templates select="../../packagedElement[@xmi:id=$asoclass]/ownedAttribute[@isID='true']" mode="nonref">
              <xsl:with-param name="prefix">ASO</xsl:with-param>
              <xsl:with-param name="kind">associated</xsl:with-param>
            </xsl:apply-templates>
          </xsl:for-each>
        </xsl:if>


        <!-- then take care of non-identifying attributes -->
        <!-- these are only local -->
	<xsl:apply-templates select="ownedAttribute[not(@association) and not(@isID)]" mode="nonref">
          <xsl:with-param name="prefix"/>
          <xsl:with-param name="kind">local</xsl:with-param>
        </xsl:apply-templates>


        <!-- last take care of referential attributes -->
        <!-- there is one ownedAttribute for each referred class but
             we need the identifying attributes in these classes -->
        <xsl:for-each select="ownedAttribute[@association]">
          <!-- loop over referred classes -->
          <xsl:variable name="refclass" select="@type"/>
          <!-- "lowerValue" element appears only if conditional -->
          <xsl:variable name="conditional">
            <xsl:choose>
              <xsl:when test="lowerValue">conditional-referential</xsl:when>
              <xsl:otherwise>unconditional-referential</xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <xsl:for-each select="../../packagedElement[@xmi:id=$refclass]">
            <xsl:choose>
              <xsl:when test="@xmi:type='uml:AssociationClass'">
                <!-- Reference to an association class -->
                <xsl:for-each select="ownedEnd">
                  <xsl:variable name="asoclass" select="@type"/>
                  <xsl:apply-templates select="../../packagedElement[@xmi:id=$asoclass]/ownedAttribute[@isID='true']" mode="ref">
                    <xsl:with-param name="prefix">ASO</xsl:with-param>
                    <xsl:with-param name="kind"><xsl:value-of select="$conditional"/></xsl:with-param>
                  </xsl:apply-templates>
                </xsl:for-each>
              </xsl:when>
              <xsl:when test="generalization">
                <xsl:variable name="genclass" select="generalization/@general"/>
                <xsl:apply-templates select="../packagedElement[@xmi:id=$genclass]/ownedAttribute[@isID='true']" mode="ref">
                  <xsl:with-param name="prefix">GEN</xsl:with-param>
                    <xsl:with-param name="kind"><xsl:value-of select="$conditional"/></xsl:with-param>
                </xsl:apply-templates>
              </xsl:when>
              <xsl:otherwise>
                <!-- Reference to an ordinary class -->
                <xsl:apply-templates select="ownedAttribute[@isID='true']" mode="ref">
                  <xsl:with-param name="prefix">REF</xsl:with-param>
                  <xsl:with-param name="kind"><xsl:value-of select="$conditional"/></xsl:with-param>
                </xsl:apply-templates>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>

        </xsl:for-each>
      </attributes>

        <!-- Now we should handle referential integrity -->
        <!-- We use the referential integrity attribute to create association
             information inside the class -->

        <xsl:for-each select="ownedAttribute[@association]">
          <xsl:variable name="assoc"><xsl:value-of select="@association"/></xsl:variable>
          <xsl:variable name="relation">
            <xsl:value-of select="../../packagedElement[@xmi:id=$assoc]/@name"/>
          </xsl:variable>

          <!-- loop over referred classes -->
          <xsl:variable name="refclass" select="@type"/>
          <xsl:for-each select="../../packagedElement[@xmi:id=$refclass]">
            <!-- Reference to an ordinary class -->
              <association>
                <xsl:attribute name="source"><xsl:value-of select="@name"/></xsl:attribute>
                <xsl:attribute name="relation"><xsl:value-of select="$relation"/></xsl:attribute>
                <xsl:choose>
                  <xsl:when test="@xmi:type='uml:AssociationClass'">
                    <xsl:for-each select="ownedEnd">
                      <xsl:variable name="asoclass" select="@type"/>
                      <xsl:apply-templates select="../../packagedElement[@xmi:id=$asoclass]/ownedAttribute[@isID='true']" mode="integrity">
                        <xsl:with-param name="prefix">ASO</xsl:with-param>
                        <xsl:with-param name="fprefix">ASO</xsl:with-param>
                      </xsl:apply-templates>
                    </xsl:for-each>
                  </xsl:when>
                  <xsl:when test="generalization">
                    <xsl:variable name="genclass" select="generalization/@general"/>
                    <xsl:apply-templates select="../packagedElement[@xmi:id=$genclass]/ownedAttribute[@isID='true']" mode="integrity">
                      <xsl:with-param name="prefix">GEN</xsl:with-param>
                      <xsl:with-param name="fprefix">GEN</xsl:with-param>
                    </xsl:apply-templates>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:apply-templates select="ownedAttribute[@isID='true']" mode="integrity">
                      <xsl:with-param name="prefix">REF</xsl:with-param>
                      <xsl:with-param name="fprefix"></xsl:with-param>
                    </xsl:apply-templates>
                  </xsl:otherwise>
                </xsl:choose>

            </association>
          </xsl:for-each>
        </xsl:for-each>

        <xsl:if test="@xmi:type='uml:AssociationClass'">
          <!-- Reference to an association class -->
          <xsl:for-each select="ownedEnd">
            <xsl:variable name="asoclass" select="@type"/>
            <association>
              <xsl:attribute name="source"><xsl:value-of select="../../packagedElement[@xmi:id=$asoclass]/@name"/></xsl:attribute>
              <xsl:apply-templates select="../../packagedElement[@xmi:id=$asoclass]/ownedAttribute[@isID='true']" mode="integrity">
                <xsl:with-param name="prefix">ASO</xsl:with-param>
                <xsl:with-param name="fprefix"></xsl:with-param>
              </xsl:apply-templates>
            </association>
          </xsl:for-each>
        </xsl:if>

        <xsl:if test="generalization">
          <xsl:variable name="genclass" select="generalization/@general"/>
          <association>
            <xsl:attribute name="source"><xsl:value-of select="../packagedElement[@xmi:id=$genclass]/@name"/></xsl:attribute>
            <xsl:apply-templates select="../packagedElement[@xmi:id=$genclass]/ownedAttribute[@isID='true']" mode="integrity">
              <xsl:with-param name="prefix">GEN</xsl:with-param>
              <xsl:with-param name="fprefix"></xsl:with-param>
            </xsl:apply-templates>
          </association>
        </xsl:if>


    </class>
  </xsl:template>



<!--========================================================================-->
<!--  Template for comment bodies -->
  <xsl:template match="body">
    <comment>
      <xsl:value-of select="text()"/>
    </comment>
  </xsl:template>



<!--========================================================================-->
<!--  Template for ordinary attributes -->
  <xsl:template match="ownedAttribute" mode="nonref">
    <xsl:param name="prefix"/>
    <xsl:param name="kind"/>
    <xsl:variable name="type">
      <xsl:call-template name="id2name">
        <xsl:with-param name="id" select="@type"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="domain">
      <xsl:call-template name="mapType2Domain">
        <xsl:with-param name="modeltype" select="$type"/>
      </xsl:call-template>
    </xsl:variable>

    <attribute>
      <xsl:attribute name="name"><xsl:value-of select="$prefix"/><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:attribute name="type"><xsl:value-of select="$type"/></xsl:attribute>
      <xsl:attribute name="isID"><xsl:value-of select="@isID"/></xsl:attribute>
      <xsl:attribute name="isOrdered"><xsl:value-of select="@isOrdered"/></xsl:attribute>
      <xsl:attribute name="kind"><xsl:value-of select="$kind"/></xsl:attribute>
    </attribute>
  </xsl:template>

<!--========================================================================-->
<!--  Template for referential attributes -->
  <xsl:template match="ownedAttribute" mode="ref">
    <xsl:param name="prefix"/>
    <xsl:param name="kind"/>
    <xsl:variable name="type">
      <xsl:call-template name="id2name">
        <xsl:with-param name="id" select="@type"/>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="domain">
      <xsl:call-template name="mapType2Domain">
        <xsl:with-param name="modeltype" select="$type"/>
      </xsl:call-template>
    </xsl:variable>

    <attribute>
      <xsl:attribute name="name"><xsl:value-of select="$prefix"/><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:attribute name="type"><xsl:value-of select="$type"/></xsl:attribute>
      <xsl:attribute name="isID"></xsl:attribute>
      <xsl:attribute name="kind"><xsl:value-of select="$kind"/></xsl:attribute>
    </attribute>
  </xsl:template>



<!--========================================================================-->
<!--  Template for attributes setting up integrity -->
  <xsl:template match="ownedAttribute" mode="integrity">
    <xsl:param name="prefix"/>
    <xsl:param name="fprefix"/>

    <attribute>
      <xsl:attribute name="name"><xsl:value-of select="$prefix"/><xsl:value-of select="@name"/></xsl:attribute>
      <xsl:attribute name="srcatt"><xsl:value-of select="$fprefix"/><xsl:value-of select="@name"/></xsl:attribute>
    </attribute>
  </xsl:template>


<!--========================================================================-->
<!--  Template for non-unique indexes -->
  <xsl:template match="packagedElement[@xmi:type='uml:Class']" mode="cindex">
    <xsl:if test="count(ownedAttribute[@isOrdered='true'])>0">
      <index>
        <xsl:attribute name="fortable"><xsl:value-of select="@name"/></xsl:attribute>
        <attributes>
          <xsl:for-each select="ownedAttribute[@isOrdered='true']">
            <attribute>
              <xsl:attribute name="name" select="@name"/>
	    </attribute>
          </xsl:for-each>
        </attributes>
      </index>
    </xsl:if>
  </xsl:template>

<!--========================================================================-->
<!--
    Given an id to a type, returns the name of that type
-->
  <xsl:template name="id2name">
    <xsl:param name="id"/>
    <xsl:variable name="type" select="//*[@xmi:id=$id]"/>
    <xsl:value-of select="$type/@name"/>
  </xsl:template>

<!--========================================================================-->
<!--
    Given a model-type, returns the domain to use
-->

<xsl:template name="mapType2Domain">
  <xsl:param name="modeltype"/>
  <xsl:variable name="ntype" select="normalize-space($modeltype)"/>
  <xsl:choose>
    <xsl:when test="$ntype = 'UniqueId'">INTEGER</xsl:when>
    <xsl:when test="$ntype = 'Name'">VARCHAR(32)</xsl:when>
    <xsl:when test="$ntype = 'Callsign'">VARCHAR(20)</xsl:when>
    <xsl:when test="$ntype = 'Comment'">VARCHAR(120)</xsl:when>
    <xsl:when test="$ntype = 'Locator'">VARCHAR(12)</xsl:when>
    <xsl:when test="$ntype = 'UTCDateTime'">TIMESTAMP</xsl:when>
    <xsl:when test="$ntype = 'Real'">**</xsl:when>
    <xsl:when test="$ntype = 'Double Precision'">**</xsl:when>
    <xsl:when test="$ntype = 'Integer'">**</xsl:when>
    <xsl:when test="$ntype = 'Boolean'">CHAR(1) CHECK (VALUE IN ('T','F'))</xsl:when>
    <xsl:when test="$ntype = 'ReportString'">VARCHAR(6)</xsl:when>
    <xsl:when test="$ntype = 'Char'">**</xsl:when>
    <xsl:when test="$ntype = 'SatMode'">VARCHAR(4)</xsl:when>
    <xsl:otherwise>--ERROR--</xsl:otherwise>
  </xsl:choose>
</xsl:template>


</xsl:stylesheet>


