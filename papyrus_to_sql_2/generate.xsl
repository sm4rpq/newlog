<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:uml="http://www.eclipse.org/uml2/5.0.0/UML" xmlns:xmi="http://www.omg.org/spec/XMI/20131001">
  <xsl:output method="text" indent="no"/>

<!--
TODO:

-->

<!--  Template for root node -->
  <xsl:template match="/">
    <xsl:apply-templates select="/model"/>
  </xsl:template>


<!--========================================================================-->
<!--  Template for "Model" element -->
  <xsl:template match="model">
    <xsl:apply-templates select="datatype"/>
    <xsl:apply-templates select="class"/>
    <xsl:apply-templates select="class/association"/>
    <xsl:apply-templates select="index"/>
  </xsl:template>


<!--========================================================================-->
<!--  Template for datatypes -->
  <xsl:template match="datatype">
create domain <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@domain"/>;
  </xsl:template>

<!--========================================================================-->
<!--  Template for "class" element -->
  <xsl:template match="class">
/*****************************************************************************/
/*
<xsl:choose>
  <xsl:when test="comment">
    <xsl:value-of select="comment"/>
  </xsl:when>
  <xsl:otherwise>
    No comment written for class <xsl:value-of select="@name"/>. Please do.
  </xsl:otherwise>
</xsl:choose>
*/
Create table <xsl:value-of select="@name"/> (
  <xsl:variable name="commalast">
    <xsl:choose>
      <xsl:when test="count(attributes/attribute[@isID='true'])>0">true</xsl:when>
      <xsl:otherwise>false</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>
  <xsl:apply-templates select="attributes/attribute">
    <xsl:with-param name="commalast" select="$commalast"/>
  </xsl:apply-templates>

<xsl:if test="count(attributes/attribute[@isID='true'])>0">
/* Primary key */
  Primary key(
        <xsl:for-each select="attributes/attribute[@isID='true']">
          <xsl:value-of select="@name"/><xsl:if test="position() != last()">,
        </xsl:if>

        </xsl:for-each>
      )
</xsl:if>
);
  </xsl:template>


<!--========================================================================-->
<!--  Template for "association" element -->
  <xsl:template match="association">
alter table <xsl:value-of select="../@name"/>
  add foreign key (
    <xsl:for-each select="attribute">
      <xsl:value-of select="@name"/><xsl:if test="position() != last()">,</xsl:if>
    </xsl:for-each>
  ) references <xsl:value-of select="@source"/>(
    <xsl:for-each select="attribute">
      <xsl:value-of select="@srcatt"/><xsl:if test="position() != last()">,</xsl:if>
    </xsl:for-each>
);
  </xsl:template>


<!--========================================================================-->
<!--  Template for ordinary attributes -->
  <xsl:template match="attribute">
    <xsl:param name="commalast"/>
    <xsl:value-of select="@name"/><xsl:text> </xsl:text><xsl:value-of select="@type"/>
    <xsl:if test="@isID='true' or @kind='unconditional-referential' or @isOrdered='true'"> NOT NULL</xsl:if><xsl:if test="(position() != last()) or $commalast='true'">,
  </xsl:if>
  </xsl:template>

<!--========================================================================-->
<!--  Template for "index" element -->
  <xsl:template match="index">
create index <xsl:value-of select="@fortable"/>_ndx on <xsl:value-of select="@fortable"/>
(
  <xsl:for-each select="attributes/attribute">
  <xsl:value-of select="@name"/><xsl:if test="position() != last()">,</xsl:if>
  </xsl:for-each>
);
  </xsl:template>

</xsl:stylesheet>

