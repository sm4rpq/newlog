unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Tacfile, ShellApi;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Button1: TButton;
    Panel2: TPanel;
    ListBox1: TListBox;
    ListBox2: TListBox;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    SaveDialog1: TSaveDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    Logfile : TLogfile;
    ADIF : TStringList;
    procedure ExportQSO;
    procedure AddDate(var Row : string;
                      Id : string;
                      Value : string);
    procedure AddTime(var Row : string;
                      Id : string;
                      Value : string);
    procedure AddString(var Row : string;
                        Id : string;
                        Value : string);
    procedure Traverse(Root : string);
  public
    { Public declarations }
    procedure DropFiles(var Msg : TWMDropFiles); message WM_DROPFILES;
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

uses
  FileCtrl;

procedure TForm1.AddDate(var Row : string;
                         Id : string;
                         Value : string);
begin
  Row:=Row+'<'+Id+':8:d>'+Value;
end;

procedure TForm1.AddTime(var Row : string;
                         Id : string;
                         Value : string);
begin
  Row:=Row+'<'+Id+':4:t>'+Value;
end;

procedure TForm1.AddString(var Row : string;
                           Id : string;
                           Value : string);
begin
  Value:=Trim(Value);
  Row:=Row+'<'+Id+':'+IntToStr(Length(Value))+'>'+Value;
end;

procedure TForm1.ExportQSO;

var
  ADIFRow : string;

begin
  ADIFRow:='';
  AddDate(ADIFRow,'qso_date',Logfile.QSODate);
  AddTime(ADIFRow,'time_on',Logfile.QSOTime);
  AddString(ADIFRow,'freq',IntToStr(Logfile.Freq));
  AddString(ADIFRow,'mode',Logfile.Mode);
  AddString(ADIFRow,'call',Logfile.Call);
  AddString(ADIFRow,'rst_sent',Logfile.rst_sent);
  AddString(ADIFRow,'rst_rcvd',Logfile.rst_rcvd);
  AddString(ADIFRow,'gridsquare',Logfile.Loc);
  AddString(ADIFRow,'operator',Logfile.Operator);
  ADIFRow:=ADIFRow+'<eor>';
  ADIF.Add(ADIFRow);
end;

procedure TForm1.Traverse(Root : string);

var
  Seek : TSearchRec;
  Attr : integer;
  Res : integer;

begin
  Attr:=faAnyFile;
  Res:=FindFirst(Root+'\*.*',Attr,Seek);
  while Res=0 do begin
    if (Seek.Name<>'.') and (Seek.Name<>'..') then begin
      if Seek.Attr=faDirectory then
        Traverse(Root+'\'+Seek.Name)
      else if Uppercase(ExtractFileExt(Seek.Name))='.DAT' then
        ListBox1.Items.Add(Root+'\'+Seek.Name);
    end;
    Res:=FindNext(Seek);
  end;
  FindClose(Seek);
end;

procedure TForm1.Button1Click(Sender: TObject);

var
  InputRoot : string;

begin
  InputRoot:='';
  if SelectDirectory(InputRoot,[],0) then begin
    Traverse(InputRoot);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);

var
  i : integer;
  index : integer;

begin
  for i:=0 to ListBox2.Count-1 do
    ListBox2.Items.Objects[i]:=TStringList.Create;

  for i:=0 to ListBox1.Count-1 do begin
    Logfile:=TLogfile.Construct(ListBox1.Items[i]);
    Index:=ListBox2.Items.IndexOf(Logfile.OwnLoc);
    ADIF:=TStringList(ListBox2.Items.Objects[Index]);
    while not Logfile.EOF do begin
      ExportQSO;
      Logfile.Next;
    end;
    Logfile.Free;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);

var
  i : integer;
  OutputRoot : string;

begin
  OutputRoot:='';
  if SelectDirectory(OutputRoot,[],0) then begin
    for i:=0 to ListBox2.Count-1 do
      TStringList(ListBox2.Items.Objects[i]).SaveToFile(OutputRoot+'\'+ListBox2.Items[i]+'.adif');
    //  if SaveDialog1.Execute then
    //    Memo1.Lines.SaveToFile(SaveDialog1.FileName);
  end;
end;

procedure TForm1.DropFiles(var Msg: TWMDropFiles);

var
  i : integer;
  n : integer;
  OneFile : string;
  cbOneFile : integer;

begin
  cbOneFile:=255;
  SetLength(OneFile,cbOneFile);
  n:=DragQueryFile(Msg.Drop,$FFFFFFFF,pChar(OneFile),cbOneFile);
  for i:=0 to n-1 do begin
    cbOneFile:=DragQueryFile(Msg.Drop,i,nil,0);
    cbOneFile:=cbOneFile+1;
    SetLength(OneFile,cbOneFile);
    cbOneFile:=DragQueryFile(Msg.Drop,i,pChar(OneFile),cbOneFile);
    OneFile:=Copy(OneFile,1,cbOneFile);
    ListBox1.Items.Add(OneFile);
  end;
  DragFinish(Msg.Drop);
end;

procedure TForm1.FormShow(Sender: TObject);
begin
  DragAcceptFiles(Handle,True);
end;

end.
