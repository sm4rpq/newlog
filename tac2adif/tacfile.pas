unit tacfile;

{
  Purpose:
    Parser for taclog .dat-files.
  Status:
    Potential bug re cross-mode qsos. Need to do actual tests in taclog to
    see which is which.
  Date:
    2021-01-30
}

interface

uses
  Classes,SysUtils;

type
  EVersionError = class(Exception);
  TLogfile = class(TObject)
  private
    FEOF : boolean;
    FQSOs : TStringList;
  public
    QSODate : string;
    QSOTime : string;
    Freq : integer;
    Mode : string;
    Call : string;
    rst_sent : string;
    no_sent : string;
    rst_rcvd : string;
    no_rcvd : string;
    Loc : string;
    OwnLoc : string;
    Operator : string;
    class function Construct(DatFile : String) : TLogfile;
    property EOF : boolean read FEOF;
    procedure Next; virtual; abstract;
  end;

  TTacV1 = class(TLogfile)
  public
    constructor Create(Datfile : string);
    destructor Destroy; override;
    procedure Next; override;
  end;

  TTacV3 = class(TLogfile)
  public
    constructor Create(Datfile : string);
    destructor Destroy; override;
    procedure Next; override;
  end;

implementation

uses
  Dialogs,Misc;

(*==========================================================*)

class function TLogfile.Construct(Datfile : String) : TLogfile;

var
  Content : TStringList;

begin
  Content:=TStringList.Create;
  try
    Content.LoadFromFile(Datfile);

    if Copy(Content.Strings[0],1,8)='TLDAT;1;' then
      Result:=TTacV1.Create(Datfile)
    else if Content.Strings[1]='Version=3' then
      Result:=TTacV3.Create(Datfile)
    else begin
      Result:=nil;
      raise EVersionError.Create('Unsupported log version');
    end;
  finally
    Content.Free;
  end;
end;

(*==========================================================*)

constructor TTacV1.Create(Datfile : string);
begin
  inherited Create;
  FQSOs:=TStringlist.Create;
  FQSOs.LoadFromFile(Datfile);
  Operator:=Item(FQSOs[0],';',2);
  OwnLoc:=Item(FQSOs[1],';',0);
  Datfile:=Uppercase(Datfile);
  if (Pos('_2\',Datfile)>0) or
     (Pos('KVARTAL\',Datfile)>0) or
     (Pos('JUL\',Datfile)>0) then
    Freq:=144
  else if Pos('_6\',Datfile)>0 then
    Freq:=50
  else if Pos('_70\',Datfile)>0 then
    Freq:=432
  else if Pos('_23\',Datfile)>0 then
    Freq:=1296
  else
    Freq:=0;
  FQSOs.Delete(0);
  FQSOs.Delete(0);
  Next;
end;

destructor TTacV1.Destroy;
begin
  FQSOs.Free;
  inherited Destroy;
end;

procedure TTacV1.Next;
begin
  while (FQSOs.Count>0) and (pos(';',FQSOs[0])=0) do
    FQSOs.Delete(0);
  if FQSOs.Count>0 then begin
    FEOF:=false;
    QSODate:=Item(FQSOs[0],';',0);
    QSOTime:=Item(FQSOs[0],';',1);
    Call:=UpperCase(Item(FQSOs[0],';',2));
    rst_sent:=Item(FQSOs[0],';',3);
    no_sent:=Item(FQSOs[0],';',4);
    rst_rcvd:=Item(FQSOs[0],';',5);
    no_rcvd:=Item(FQSOs[0],';',6);
    Loc:=Item(FQSOs[0],';',8);
    Mode:='SSB';
    FQSOs.Delete(0);
  end else begin
    FEOF:=true;
  end;
end;

(*==========================================================*)

constructor TTacV3.Create(Datfile : string);

const
  Bands : array['A'..'G'] of integer = (50,70,144,220,432,900,1296);

var
  Id : string;
  Band : char;

begin
  inherited Create;
  FQSOs:=TStringlist.Create;
  FQSOs.LoadFromFile(Datfile);
  while Id<>'[QSORecords]' do begin
    Id:=Item(FQSOs[0],'=',0);
    if Id='OwnCall' then
      Operator:=Item(FQSOs[0],'=',1)
    else if Id='OwnWWL' then
      OwnLoc:=Item(FQSOs[0],'=',1)
    else if Id='Band' then begin
      Band:=Item(FQSOs[0],'=',1)[1];
      Freq:=Bands[Band];
    end;
    FQSOs.Delete(0);
  end;
  Next;
end;

destructor TTacV3.Destroy;
begin
  FQSOs.Free;
  inherited Destroy;
end;

procedure TTacV3.Next;

var
  iMode : integer;

begin
  while (FQSOs.Count>0) and (pos(';',FQSOs[0])=0) do
    FQSOs.Delete(0);
  if FQSOs.Count>0 then begin
    FEOF:=false;
    QSODate:=Item(FQSOs[0],';',0);
    QSOTime:=Item(FQSOs[0],';',1);
    Call:=UpperCase(Item(FQSOs[0],';',2));

    iMode:=StrToInt(Item(FQSOs[0],';',3));
    case iMode of
      0 : Mode:='Other';
      1 : Mode:='SSB';
      2 : Mode:='CW';
      // 3 & 4 = Cross-mode SSB/CW
      3 : Mode:='SSBCW'; //TODO: Check
      4 : Mode:='CWSSB'; //TODO: Check
      5 : Mode:='AM';
      6 : Mode:='FM';
      7 : Mode:='RTTY';
      8 : Mode:='SSTV';
      9 : Mode:='ATV';
    else
      Mode:='';
    end;

    rst_sent:=Item(FQSOs[0],';',4);
    no_sent:=Item(FQSOs[0],';',5);
    rst_rcvd:=Item(FQSOs[0],';',6);
    no_rcvd:=Item(FQSOs[0],';',7);
    Loc:=Item(FQSOs[0],';',9);
    FQSOs.Delete(0);
  end else begin
    FEOF:=true;
  end;
end;

end.
