object Form1: TForm1
  Left = 2464
  Top = 386
  Width = 1010
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 1002
    Height = 412
    Align = alClient
    TabOrder = 1
    object ListBox1: TListBox
      Left = 8
      Top = 8
      Width = 281
      Height = 393
      ItemHeight = 13
      TabOrder = 0
    end
    object ListBox2: TListBox
      Left = 296
      Top = 8
      Width = 73
      Height = 393
      ItemHeight = 13
      Items.Strings = (
        'JO69OI'
        'JO79GH'
        'JO79GM'
        'JO79GN'
        'JO79HF'
        'JO79HH'
        'JO79HI'
        'JO79HL'
        'JO79HM'
        'JP73IF'
        'JP74AS'
        'JP74BT'
        'JP74BU'
        'JP75FC')
      TabOrder = 1
    end
    object Memo1: TMemo
      Left = 640
      Top = 8
      Width = 353
      Height = 393
      ScrollBars = ssBoth
      TabOrder = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1002
    Height = 41
    Align = alTop
    TabOrder = 0
    object Button1: TButton
      Left = 8
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Filer'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 304
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Pl'#246'j'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 640
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Spara'
      TabOrder = 2
      OnClick = Button3Click
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 720
    Top = 8
  end
end
